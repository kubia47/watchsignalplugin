<?php
ini_set('display_errors', 1);
/*
Plugin Name: WatchSignals Comparison
Description: All at Luxury Watch comparison.
Version: 1.0
Author: WatchSignals Team
Author URI: http://watchsignals.com
*/

/*Define Constants for this plugin*/
define( 'WATCHSIGNALS_SETUP_VERSION', '1.0' );
define( 'WATCHSIGNALS_DIR', plugin_dir_path( __FILE__ ) );
define( 'WATCHSIGNALS_URL', plugin_dir_url( __FILE__ ) );
define( 'WATCHSIGNALS_NOPHOTO_URL', plugin_dir_url( __FILE__ )."assets/img/no-photo.png" );
define( 'WATCHSIGNALS_SITE_URL', get_site_url() );

/*Now lets init the functionality of this plugin*/
require_once( WATCHSIGNALS_DIR . '/includes/init.php' );

//add some options and pages when plugin is active
function watchsignals_activation(){
	/*********CREATE SOME PAGES FOR OUR PLUGIN*****************/
	$pages_settings = array();
	register_setting( 'watchsignals_pages_settings', 'watchsignals_pages_settings' );	
	$pages_array = array(				
		array("Title" => 'Watch Signals - Home', "Slug" => "home","Template" => "home.php", "Shortcode" => '[WATCHSIGNALS_HOMEPAGE]'),
		array("Title" => 'Watch Signals - Search Results', "Slug" => "search","Template" => "visual_builder.php", "Shortcode" => '[WATCHSIGNALS_SEARCHRESULTS]'),
		array("Title" => 'Watch Signals - Product Details', "Slug" => "product","Template" => "visual_builder.php","Shortcode" => '[WATCHSIGNALS_PRODUCTDETAILS]'),
		array("Title" => 'Watch Signals - Compare Products', "Slug" => "compare-products","Template" => "visual_builder.php","Shortcode" => '[WATCHSIGNALS_COMPAREPRODUCTS]'),
		array("Title" => 'Watch Signals - Advanced Search', "Slug" => "advanced-search","Template" => "visual_builder.php","Shortcode" => '[WATCHSIGNALS_ADVANCEDSEARCH]'),
	);
	foreach($pages_array as $item){
		//create new page, this function in /includes/functions.php
		$newpageId=watchsignals_create_page($item["Title"],$item["Shortcode"],$item["Slug"],$item["Template"]);
		array_push($pages_settings, $newpageId);
	}		
	//update watchsignals_settings
	update_option( 'watchsignals_pages_settings', $pages_settings );
	/////////////////////////////////////////////////////////////
}
//run this functon when we active a plugin
register_activation_hook(__FILE__, 'watchsignals_activation');

//define variable, add some functions when plugin is loaded
function watchsignals_loaded(){
    // Create Global Class to use it everywhere in plugin
	global $WS_COUNTRIES;
	$WS_COUNTRIES = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
	global $WS_CLASS;
	$WS_CLASS = new WATCHSIGNALS();
	global $WS_CACHE;
	$WS_CACHE=new Cache(array(
			  'name'      => 'WS-CACHE-FILES',
			  'path'      => WATCHSIGNALS_DIR.'includes/cache/',
			  'extension' => '.cache'
			));
	$WS_CACHE->eraseExpired();
	
	if ( ! is_admin() ) {
		//add our script for searching in footer
		wp_enqueue_script( 'ws-json2', 
						   WATCHSIGNALS_URL.'assets/js/json2.js', 
						   '', 
						   rand(1, 100), 
						   true);
		wp_enqueue_script( 'ws-functions-js', 
						   WATCHSIGNALS_URL.'assets/js/ws-functions.js', 
						   '', 
						   rand(1, 100), 
						   true);
		//add our script for searching in footer
		wp_enqueue_script( 'ws-ajax-search-js', 
						   WATCHSIGNALS_URL.'assets/js/ws-ajax-search.js', 
						   '', 
						   rand(1, 100), 
						   true);
		
		include_once(WATCHSIGNALS_DIR . "/includes/js-variables.php");
	}
}
//run watchsignals_loaded function when plugin is loaded
add_action( 'wp_head', 'watchsignals_loaded' );

//run this function when deactive plugin
function watchsignals_deactivation() {	
	//delete all pages
    $page_ids = get_option('watchsignals_pages_settings');
	foreach($page_ids as $page_id){
		wp_delete_post($page_id);
	}    
	//delete options
	delete_option( 'watchsignals_pages_settings' );
}
register_deactivation_hook( __FILE__, 'watchsignals_deactivation' );

/* call our code on admin pages only, not on front end requests or during
 * AJAX calls.
 * Always wait for the last possible hook to start your code.
 */
add_action( 'admin_menu', 'WS_Coupon_Plugin' );

function WS_Coupon_Plugin()
{
    /**
     * Register the pages and the style and script loader callbacks.
     *
     * @wp-hook admin_menu
     * @return  void
     */
    add_menu_page(
        'WS Coupon',     // page title
        'WS Coupon',     // menu title
        'manage_options',   // capability
        'ws-coupon',     // menu slug
        'ws_coupon_render' // callback function
    );
}

function ws_coupon_render(){
    $file = plugin_dir_path( __FILE__ ) . "includes/ws_coupon.php";

    if ( file_exists( $file ) )
        require $file;
}
?>