var ws_chosen_brands=[];
var ws_ajax_cache = {

    data: {},

    remove: function (cache_id) {

        delete ws_ajax_cache.data[cache_id];

    },

    exist: function (cache_id) {

        return ws_ajax_cache.data.hasOwnProperty(cache_id) && ws_ajax_cache.data[cache_id] !== null;

    },

    get: function (cache_id) {

        return ws_ajax_cache.data[cache_id];

    },

    set: function (cache_id, cachedData) {

        ws_ajax_cache.remove(cache_id);

        ws_ajax_cache.data[cache_id] = cachedData;

    }
};
var ws_ajax_search = {
    // Some variables
    _current_selection_index:0,
    _last_request_results_count:0,
    _first_down_up:true,
    _is_search_open:false,

    init: function init() {        		
		jQuery(document).on('submit', ".search-form", function(e){
			e.preventDefault();
			var action="https://ws.euxira.net/search/";
			window.location=action+"?query="+ws_ajax_search.ws_create_search_string(jQuery(this).find("input[type='text']").val());
			return false;
		});
  
        jQuery(document).click( function(e){ 
            if( jQuery(e.target).closest(".head_search").length || jQuery(e.target).closest(".custom_search_box").length) 
                return;
            jQuery( '.head_search .re-aj-search-wrap' ).removeClass( 're-aj-search-open' ).empty();
            jQuery( '.custom_search_box .re-aj-search-wrap' ).removeClass( 're-aj-search-open' ).empty();
            e.stopPropagation();
            if (ws_ajax_search._is_search_open === true) {
                ws_ajax_search._is_search_open = false;
            }            
        });

        jQuery(document).click( function(e){ 
            if( jQuery(e.target).closest(".search-header-contents").length ) 
                return;
            jQuery( '.main-nav' ).removeClass( 'top-search-onclick-open' );
            e.stopPropagation();
            if (ws_ajax_search._is_search_open === true) {
                ws_ajax_search._is_search_open = false;
            }            
        });
		
		jQuery(".ws-search-btn").click(function(){
			var ajaxsearchitem = jQuery('.ws-ajax-serch');
			if(ajaxsearchitem.val() != "" && ajaxsearchitem.val().length >=2){
				var action=jQuery(this).parent().attr('action');
				window.location=action+"?query="+ws_ajax_search.ws_create_search_string(ajaxsearchitem.val());               
			}			
		});

        // keydown on the text box
        jQuery('.ws-ajax-serch').keydown(function(event) {
            var ajaxsearchitem = jQuery(this);			
            if (
                (event.which && event.which == 39)
                || (event.keyCode && event.keyCode == 39)
                || (event.which && event.which == 37)
                || (event.keyCode && event.keyCode == 37))
            {
                //do nothing on left and right arrows
                ws_ajax_search.ws_ajax_set_focus(ajaxsearchitem);
                return;
            }

            if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
                // on enter
                var ws_ajax_search_cur = jQuery(this).parent().parent().find('.re-sch-cur-element');
                if (ws_ajax_search_cur.length > 0) {
                    var re_searchopen_url = ws_ajax_search_cur.find('.re-search-result-title a').attr('href');
                    window.location = re_searchopen_url;
                } else {
					var action=jQuery(this).parent().attr('action');
					window.location=action+"?query="+ws_ajax_search.ws_create_search_string(ajaxsearchitem.val());
                }
                return false; //redirect for search on enter
            } else {

                if ((event.which && event.which == 40) || (event.keyCode && event.keyCode == 40)) {
                    // down
                    ws_ajax_search.ws_aj_search_move_key_down(ajaxsearchitem);
                    return false; //disable the envent

                } else if((event.which && event.which == 38) || (event.keyCode && event.keyCode == 38)) {
                    //up
                    ws_ajax_search.ws_aj_search_move_key_up(ajaxsearchitem);
                    return false; //disable the envent
                } else {

                    //for backspace we have to check if the search query is empty and if so, clear the list
                    if ((event.which && event.which == 8) || (event.keyCode && event.keyCode == 8)) {
                        //if we have just one character left, that means it will be deleted now and we also have to clear the search results list
                        var search_query = jQuery(this).val();
                        if (search_query.length == 1) {
                            jQuery(this).parent().parent().find('.re-aj-search-wrap').removeClass( 're-aj-search-open' ).empty();
                        }
                    }

                    //various keys
                    ws_ajax_search.ws_ajax_set_focus(ajaxsearchitem);
                    //jQuery('.re-aj-search-wrap').empty();
                    setTimeout(function(){
                        ws_ajax_search.do_ajax_call(ajaxsearchitem);
                    }, 100);
                }
                return true;
            }
        });

    },
	ws_create_search_string: function(string){
		return string.replace(/^\s+|\s+$/g, "").replace(/\s+/g, "+");
	},
    /**
     * moves the select up
     */
    ws_aj_search_move_key_up: function ws_aj_search_move_key_up(elem) {
        if (ws_ajax_search._first_down_up === true) {
            ws_ajax_search._first_down_up = false;
            if (ws_ajax_search._current_selection_index === 0) {
                ws_ajax_search._current_selection_index = ws_ajax_search._last_request_results_count - 1;
            } else {
                ws_ajax_search._current_selection_index--;
            }
        } else {
            if (ws_ajax_search._current_selection_index === 0) {
                ws_ajax_search._current_selection_index = ws_ajax_search._last_request_results_count;
            } else {
                ws_ajax_search._current_selection_index--;
            }
        }
        elem.parent().parent().find('.re-search-result-div').removeClass('re-sch-cur-element');
        if (ws_ajax_search._current_selection_index  > ws_ajax_search._last_request_results_count -1) {
            //the input is selected
            elem.closest('form').fadeTo(100, 1);
        } else {
            ws_ajax_search.ws_search_input_remove_focus(elem);
            elem.parent().parent().find('.re-search-result-div').eq(ws_ajax_search._current_selection_index).addClass('re-sch-cur-element');
        }
    },

    /**
     * moves the select prompt down
     */
    ws_aj_search_move_key_down: function ws_aj_search_move_key_down(elem) {
        if (ws_ajax_search._first_down_up === true) {
            ws_ajax_search._first_down_up = false;
        } else {
            if (ws_ajax_search._current_selection_index === ws_ajax_search._last_request_results_count) {
                ws_ajax_search._current_selection_index = 0;
            } else {
                ws_ajax_search._current_selection_index++;
            }
        }
        elem.parent().parent().find('.re-search-result-div').removeClass('re-sch-cur-element');
        if (ws_ajax_search._current_selection_index > ws_ajax_search._last_request_results_count - 1 ) {
            //the input is selected
            elem.closest('form').fadeTo(100, 1);
        } else {
            ws_ajax_search.ws_search_input_remove_focus(elem);
            elem.parent().parent().find('.re-search-result-div').eq(ws_ajax_search._current_selection_index).addClass('re-sch-cur-element');
        }
    },

    /**
     * puts the focus on the input box
     */
    ws_ajax_set_focus: function ws_ajax_set_focus(elem) {
        ws_ajax_search._current_selection_index = 0;
        ws_ajax_search._first_down_up = true;
        elem.closest('form').fadeTo(100, 1);
        elem.parent().parent().find('.re-search-result-div').removeClass('re-sch-cur-element');
    },

    /**
     * removes the focus from the input box
     */
    ws_search_input_remove_focus: function ws_search_input_remove_focus(elem) {
        if (ws_ajax_search._last_request_results_count !== 0) {
            elem.closest('form').css('opacity', 0.5);
        }
    },

    /**
     * AJAX: process the response from the server
     */
    process_ajax_response: function (data, callelem) {
        var current_query = callelem.val();

        //the search is empty - drop results
        if (current_query == '') {
            callelem.parent().parent().find('.re-aj-search-wrap').empty();
            return;
        }

        var td_data_object = jQuery.parseJSON(data); //get the data object
        //drop the result - it's from a old query
		//console.log(current_query);
        if (td_data_object.re_search_query !== current_query) {
            return;
        }

        //reset the current selection and total posts
        ws_ajax_search._current_selection_index = 0;
        ws_ajax_search._last_request_results_count = td_data_object.re_total_inlist;
        ws_ajax_search._first_down_up = true;


        //update the query
        callelem.parent().parent().find('.re-aj-search-wrap').addClass( 're-aj-search-open' ).html(td_data_object.re_data);
        var iconsearch = callelem.parent().find('.fa-sync'); 
        iconsearch.removeClass('fa-sync fa-spin').addClass('fa-search');
        callelem.removeClass('searching-now');
        var winheight = jQuery(window).height();
        if (winheight < 700){
          callelem.parent().parent().find('.re-aj-search-wrap').addClass( 're-aj-search-overflow' );
        }

    },

    /**
     * AJAX: do the ajax request
     */
    do_ajax_call: function do_ajax_call(elem) {
        var posttypes = elem.data('posttype');
        var enable_compare = elem.data('enable_compare');
        if(elem.prevObject == undefined){
            var catid = elem.data('catid');
        }else{
            var catid = elem.attr('data-catid');
        }
        var callelem = elem;
        if (elem.val() == '') {
            ws_ajax_search.ws_ajax_set_focus(callelem);
            return;
        }

        var search_query = elem.val();

        //do we have a cache hit
        if (ws_ajax_cache.exist(search_query)) {
            ws_ajax_search.process_ajax_response(ws_ajax_cache.get(search_query), callelem);
            return;
        }

        var iconsearch =  elem.parent().find('.fa-search');     
        iconsearch.removeClass('fa-search').addClass('fa-sync fa-spin');
        elem.addClass('searching-now');

        jQuery.ajax({
            type: 'POST',
            url: ws_ajaxSearch_url,			
            data: {
                action: 'ws_ajax_search',
                re_string: search_query                
            },
            success: function(data, textStatus, XMLHttpRequest){
				//console.log(data);
                //ws_ajax_cache.set(search_query, data);
                ws_ajax_search.process_ajax_response(data, callelem);
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                //console.log(errorThrown);
            }
        });
    }
};

jQuery(document).ready(function($) {
	// Search icon show/hide
	jQuery('.icon-search-onclick').unbind("click");
	jQuery('.icon-search-onclick').click(function(e) {
		e.stopPropagation();
		jQuery( '.main-nav' ).toggleClass( 'top-search-onclick-open' );
		if (ws_ajax_search._is_search_open === true) {
			ws_ajax_search._is_search_open = false;
		}
		else {
			ws_ajax_search._is_search_open = true;
			if (jQuery('html').hasClass('flash')) {
				setTimeout(function(){
					jQuery( '.main-nav .search-header-contents input[name="s"]' ).focus();
				}, 200);
			}                
		}
	});	

	jQuery(".ws-brand-select").change(function(){
		var chosenOne=jQuery(this).val();
		jQuery.blockUI(0);
		jQuery.ajax({
            type: 'POST',
            url: ws_ajaxSearch_url,			
            data: {
                action: 'ws_ajax_filter_brand',
                re_string: chosenOne              
            },
            success: function(data, textStatus, XMLHttpRequest){
				result=jQuery.parseJSON(data);				
				jQuery(".ws-brand-area .product").remove();
				jQuery(".ws-brand-area").append(result.re_data);
				jQuery.unblockUI();
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
	});
	
	jQuery(".ws-condition-select").change(function(){
		var chosenOne=jQuery(this).val();
		jQuery.blockUI(0);
		jQuery.ajax({
            type: 'POST',
            url: ws_ajaxSearch_url,			
            data: {
                action: 'ws_ajax_filter_condition',
                re_string: chosenOne              
            },
            success: function(data, textStatus, XMLHttpRequest){
				result=jQuery.parseJSON(data);				
				jQuery(".ws-new-preowned-area .product").remove();
				jQuery(".ws-new-preowned-area").append(result.re_data);
				jQuery.unblockUI();
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
	});
	
	jQuery(".ws-location-select").change(function(){
		var chosenOne=jQuery(this).val();
		jQuery.blockUI(0);
		jQuery.ajax({
            type: 'POST',
            url: ws_ajaxSearch_url,			
            data: {
                action: 'ws_ajax_filter_location',
                re_string: chosenOne              
            },
            success: function(data, textStatus, XMLHttpRequest){
				result=jQuery.parseJSON(data);				
				jQuery(".ws-location-area .product").remove();
				jQuery(".ws-location-area").append(result.re_data);
				jQuery.unblockUI();
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
	});
	
	jQuery(".ws-gender-select").change(function(){
		var chosenOne=jQuery(this).val();
		jQuery.blockUI(0);
		jQuery.ajax({
            type: 'POST',
            url: ws_ajaxSearch_url,			
            data: {
                action: 'ws_ajax_filter_gender',
                re_string: chosenOne              
            },
            success: function(data, textStatus, XMLHttpRequest){
				result=jQuery.parseJSON(data);				
				jQuery(".ws-gender-area .product").remove();
				jQuery(".ws-gender-area").append(result.re_data);
				jQuery.unblockUI();
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
	});
	
	if(jQuery("#ws-brand-dropdown").length){		
		jQuery("#ws-brand-dropdown").selectpicker({
			maxOptions: 3
		});
		jQuery('.selectpicker').change(function (e) {
			ws_chosen_brands = jQuery(e.target).val();
		});
		jQuery(".bootstrap-select").addClass("form-control");
		jQuery(".bootstrap-select button").addClass("form-control");
		jQuery(".bootstrap-select").show();
	}
	
	if(jQuery(".ws-adsearch-btn").length){
		jQuery(".ws-adsearch-btn").click(function(){
			var re_string=jQuery("#ws-additional-search").val();
			var case_material=jQuery("#ws-casematerial-dropdown").val();
			var gender=jQuery("#ws-gender-dropdown").val();
			var bracelet_material=jQuery("#ws-braceletmaterial-dropdown").val();
			if(re_string != ""){
				var searchLink=jQuery(".ws-search-link").val();
					searchLink+="?adsearch=true";
					searchLink+="&query="+ws_ajax_search.ws_create_search_string(re_string);
					if(ws_chosen_brands != null){
						for(var i=0;i<ws_chosen_brands.length;i++){
							searchLink+="&manufactures[]="+ws_chosen_brands[i];
						};					
					}
				var movementObj=jQuery(".ws-movement-container input:checked");
					if(movementObj.length!=0){
						movementObj.each(function(){
							searchLink+="&movement[]="+jQuery(this).val();
						});
					}
				var conditionObj=jQuery(".ws-condition-container input:checked");
					if(conditionObj.length!=0){
						conditionObj.each(function(){
							searchLink+="&conditions[]="+jQuery(this).val();
						});
					}
				var scopeofdeliveryObj=jQuery(".ws-scopeofdelivery-container input:checked");
					if(scopeofdeliveryObj.length!=0){
						scopeofdeliveryObj.each(function(){
							searchLink+="&scopeofdelivery[]="+jQuery(this).val();
						});
					}
					
					if(case_material != "---"){
						searchLink+="&caseMaterial="+case_material;
					}
					if(gender != "---"){
						searchLink+="&gender="+gender;
					}
					if(bracelet_material != "---"){
						searchLink+="&braceletMaterial="+bracelet_material;
					}
				var priceFrom=jQuery("#ws-pricefrom-search").val();
				var priceTo=jQuery("#ws-priceto-search").val();
				
				if(priceFrom != "" && priceTo != ""){
					searchLink+="&priceFrom="+priceFrom+"&priceTo="+priceTo;
				}
				
				var sortBy=jQuery("#ws-sortby-dropdown").val();
					searchLink+="&sortBy="+sortBy;
					
				var countryChosen=jQuery("#ws-location-dropdown").val();					
				if(countryChosen!=""){
					searchLink+="&country="+countryChosen;
				}
					
					//console.log(searchLink);
				window.location=searchLink;
			}
			
		});
	}
		
	setTimeout(function(){
		ws_ajax_search.init();
	}, 500);
});