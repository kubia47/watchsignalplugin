function noResultHTML(){
	var htmlTags='<div class="alert alert-warning" role="alert"><i class="fas fa-exclamation-circle"></i> Sorry, your search did not match any items</div>';
	return htmlTags;
}

function eventCheckBox(className){
	$("."+className+"-checkbox .custom-control-input").unbind("change");
	$("."+className+"-checkbox .custom-control-input").change(function(){
		updateFiltersInPopup();
	});
}

function add_brand_tag(where,label, value){
	if(where=="brand"){
		for(j=0;j<ws_brand_array.length;j++){
			if(ws_brand_array[j]["id"]==value){
				var html='<span class="label-tag"><span class="text">'+ws_brand_array[j]["name"]+'</span><span class="delete" data-type="manufactures[]" data-value="'+value+'">x</span></span>';
				break;
			}
		}
	}

	if(where=="caseMaterial"){
		for(j=0;j<ws_search_result_array.caseMaterialArray.length;j++){
			if(ws_search_result_array.caseMaterialArray[j]["id"]==value){
				var html='<span class="label-tag"><span class="text">'+ws_search_result_array.caseMaterialArray[j]["name"]+'</span><span class="delete" data-type="caseMaterials[]" data-value="'+value+'">x</span></span>';
				break;
			}
		}
	}

	if(where=="dealer"){
		for(j=0;j<ws_dealer_array.length;j++){
			if(ws_dealer_array[j]["id"]==value){
				var html='<span class="label-tag"><span class="text">'+ws_dealer_array[j]["name"]+'</span><span class="delete" data-type="dealers[]" data-value="'+value+'">x</span></span>';
				break;
			}
		}
	}

	$("."+where+"-chosen-items").append(html);
	$("."+where+"-chosen-items .label-tag").unbind("click");
	$("."+where+"-chosen-items .label-tag").click(function(){
		var value=$(this).find(".delete").attr("data-value");
		$("#"+where+"-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
	});
}

function remove_brand_tag(where,id){
	$("."+where+"-chosen-items").find(".delete[data-value='"+id+"']").parent().remove();
}

function set_filter_default(){
	/*reset brand filter tabs*/
	if(ws_brand_array.length > 0){
		$("#brand-dropdown").empty();
		$(".brand-chosen-items").empty();
		var brandHTML="";
		var chosenBrandHTML="";
		for(i=0;i<ws_brand_array.length;i++){
			brandHTML+="<option value='"+ws_brand_array[i].id+"'";
			if(Array.isArray(vars["manufactures"])){
				if(vars["manufactures"].indexOf(ws_brand_array[i].id) != -1){
					brandHTML+=" selected";
					chosenBrandHTML+='<span class="label-tag"><span class="text">'+ws_brand_array[i].name+'</span><span class="delete" data-type="manufactures[]" data-value="'+ws_brand_array[i].id+'">x</span></span>';
				}
			}
			brandHTML+=">"+ws_brand_array[i].name+" ("+ws_brand_array[i].total+")</option>";
		}
		$(".brand-chosen-items").append(chosenBrandHTML);
		$("#brand-dropdown").append(brandHTML);
		$('#brand-dropdown').multiselect( 'reload' );
		$(".brand-chosen-items .label-tag").unbind("click");
		$(".brand-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#brand-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}else{
		$(".brand-chosen-items").empty();
		$(".brand-dropdown-container").hide();
		$(".brand-no-results").show();
	}

	/*reset dealer filter tabs*/
	if(ws_search_result_array.caseMaterialArray.length!=0){
		$("#caseMaterial-dropdown").empty();
		$(".caseMaterial-chosen-items").empty();
		var caseMaterialHTML="";
		var chosencaseMaterialHTML="";
		for(i=0;i<ws_search_result_array.caseMaterialArray.length;i++){
			caseMaterialHTML+="<option value='"+ws_search_result_array.caseMaterialArray[i].id+"'";
			if(Array.isArray(vars["caseMaterials"])){
				if(vars["caseMaterials"].indexOf(ws_search_result_array.caseMaterialArray[i].id) != -1){
					caseMaterialHTML+=" selected";
					chosencaseMaterialHTML+='<span class="label-tag"><span class="text">'+ws_search_result_array.caseMaterialArray[i].name+'</span><span class="delete" data-type="caseMaterials[]" data-value="'+ws_search_result_array.caseMaterialArray[i].id+'">x</span></span>';
				}
			}
			caseMaterialHTML+=">"+ws_search_result_array.caseMaterialArray[i].name+" ("+ws_search_result_array.caseMaterialArray[i].total+")</option>";
		}
		$(".caseMaterial-chosen-items").append(chosencaseMaterialHTML);
		$("#caseMaterial-dropdown").append(caseMaterialHTML);
		$('#caseMaterial-dropdown').multiselect( 'reload' );
		$(".caseMaterial-chosen-items .label-tag").unbind("click");
		$(".caseMaterial-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#caseMaterial-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}else{
		$(".caseMaterial-chosen-items").empty();
		$(".case-dropdown-container").hide();
		$(".caseMaterial-no-results").show();
	}

	/*update movement checkbox*/
	if(ws_search_result_array.movementArray.length!=0){
		$(".filter-group-movement .filter-group-toggle").empty();
		var movementHTML="";
		for(i=0;i<ws_search_result_array.movementArray.length;i++){
			movementHTML+='<div class="custom-control custom-checkbox filter-checkbox movement-checkbox">';
			movementHTML+='<input type="checkbox" value="'+ws_search_result_array.movementArray[i].id+'" class="custom-control-input" id="movement-'+ws_search_result_array.movementArray[i].id+'"';
			if(Array.isArray(vars["movements"])){
				if(vars["movements"].indexOf(ws_search_result_array.movementArray[i].id) != -1){
					movementHTML+=" checked";
				}
			}
			movementHTML+='><label class="custom-control-label" for="movement-'+ws_search_result_array.movementArray[i].id+'">'+ws_search_result_array.movementArray[i].name+' ('+ws_search_result_array.movementArray[i].total+')</label></div>';
		}
		$(".filter-group-movement .filter-group-toggle").html(movementHTML);
		eventCheckBox("movement");
	}else{
		$(".filter-group-movement .filter-group-toggle").html(noResultHTML());
	}
	/*update bracelet material checkbox*/
	if(ws_search_result_array.braceletMaterialArray.length!=0){
		$(".filter-group-braceletMaterial .filter-group-toggle").empty();
		var braceletMaterialHTML="";
		for(i=0;i<ws_search_result_array.braceletMaterialArray.length;i++){
			braceletMaterialHTML+='<div class="custom-control custom-checkbox filter-checkbox braceletMaterial-checkbox">';
			braceletMaterialHTML+='<input type="checkbox" value="'+ws_search_result_array.braceletMaterialArray[i].id+'" class="custom-control-input" id="braceletMaterial-'+ws_search_result_array.braceletMaterialArray[i].id+'"';
			if(Array.isArray(vars["braceletMaterial"])){
				if(vars["braceletMaterial"].indexOf(ws_search_result_array.braceletMaterialArray[i].id) != -1){
					braceletMaterialHTML+=" checked";
				}
			}
			braceletMaterialHTML+='><label class="custom-control-label" for="braceletMaterial-'+ws_search_result_array.braceletMaterialArray[i].id+'">'+ws_search_result_array.braceletMaterialArray[i].name+' ('+ws_search_result_array.braceletMaterialArray[i].total+')</label></div>';
		}
		$(".filter-group-braceletMaterial .filter-group-toggle").html(braceletMaterialHTML);
		eventCheckBox("braceletMaterial");
	}else{
		$(".filter-group-braceletMaterial .filter-group-toggle").html(noResultHTML());
	}
	/*update bracelet color checkbox*/
	if(ws_search_result_array.braceletColorArray.length!=0){
		$(".filter-group-braceletColor .filter-group-toggle").empty();
		var braceletColorHTML="";
		for(i=0;i<ws_search_result_array.braceletColorArray.length;i++){
			braceletColorHTML+='<div class="custom-control custom-checkbox filter-checkbox braceletColor-checkbox">';
			braceletColorHTML+='<input type="checkbox" value="'+ws_search_result_array.braceletColorArray[i].id+'" class="custom-control-input" id="braceletColor-'+ws_search_result_array.braceletColorArray[i].id+'"';
			if(Array.isArray(vars["braceletColor"])){
				if(vars["braceletColor"].indexOf(ws_search_result_array.braceletColorArray[i].id) != -1){
					braceletColorHTML+=" checked";
				}
			}
			braceletColorHTML+='><label class="custom-control-label" for="braceletColor-'+ws_search_result_array.braceletColorArray[i].id+'">'+ws_search_result_array.braceletColorArray[i].name+' ('+ws_search_result_array.braceletColorArray[i].total+')</label></div>';
		}
		$(".filter-group-braceletColor .filter-group-toggle").html(braceletColorHTML);
		eventCheckBox("braceletColor");
	}else{
		$(".filter-group-braceletColor .filter-group-toggle").html(noResultHTML());
	}
	/*update dial color checkbox*/
	if(ws_search_result_array.dialColorArray.length!=0){
		var selectedDialColors=[];
		$(".dialColor-checkbox .custom-control-input:checked").each(function(){
			selectedDialColors.push($(this).val());
		});
		$(".filter-group-dialColor .filter-group-toggle").empty();
		var dialColorHTML="";
		for(i=0;i<ws_search_result_array.dialColorArray.length;i++){
			dialColorHTML+='<div class="custom-control custom-checkbox filter-checkbox dialColor-checkbox">';
			dialColorHTML+='<input type="checkbox" value="'+ws_search_result_array.dialColorArray[i].id+'" class="custom-control-input" id="dialColor-'+ws_search_result_array.dialColorArray[i].id+'"';
			if(Array.isArray(vars["dialColors"])){
				if(vars["dialColors"].indexOf(ws_search_result_array.dialColorArray[i].id) != -1){
					dialColorHTML+=" checked";
				}
			}
			dialColorHTML+='><label class="custom-control-label" for="dialColor-'+ws_search_result_array.dialColorArray[i].id+'">'+ws_search_result_array.dialColorArray[i].name+' ('+ws_search_result_array.dialColorArray[i].total+')</label></div>';
		}
		$(".filter-group-dialColor .filter-group-toggle").html(dialColorHTML);
		eventCheckBox("dialColor");
	}else{
		$(".filter-group-dialColor .filter-group-toggle").html(noResultHTML());
	}

	/*update gender checkbox*/
	if(ws_search_result_array.genderArray.length!=0){
		$(".filter-group-gender .filter-group-toggle").empty();
		var genderHTML="";
		for(i=0;i<ws_search_result_array.genderArray.length;i++){
			genderHTML+='<div class="custom-control custom-checkbox filter-checkbox gender-checkbox">';
			genderHTML+='<input type="checkbox" value="'+ws_search_result_array.genderArray[i].name+'" class="custom-control-input" id="gender-'+ws_search_result_array.genderArray[i].id+'"';
			if(vars["gender"] == ws_search_result_array.genderArray[i].name){
				genderHTML+=" checked";
			}
			genderHTML+='><label class="custom-control-label" for="gender-'+ws_search_result_array.genderArray[i].id+'">'+ws_search_result_array.genderArray[i].name+' ('+ws_search_result_array.genderArray[i].total+')</label></div>';
		}
		$(".filter-group-gender .filter-group-toggle").html(genderHTML);
		eventCheckBox("gender");
	}else{
		$(".filter-group-gender .filter-group-toggle").html(noResultHTML());
	}

	/*update glass checkbox*/
	if(ws_search_result_array.glassArray.length!=0){
		$(".filter-group-glass .filter-group-toggle").empty();
		var glassHTML="";
		for(i=0;i<ws_search_result_array.glassArray.length;i++){
			glassHTML+='<div class="custom-control custom-checkbox filter-checkbox glass-checkbox">';
			glassHTML+='<input type="checkbox" value="'+ws_search_result_array.glassArray[i].id+'" class="custom-control-input" id="glass-'+ws_search_result_array.glassArray[i].id+'"';
			if(Array.isArray(vars["glass"])){
				if(vars["glass"].indexOf(ws_search_result_array.glassArray[i].id) != -1){
					glassHTML+=" checked";
				}
			}
			glassHTML+='><label class="custom-control-label" for="glass-'+ws_search_result_array.glassArray[i].id+'">'+ws_search_result_array.glassArray[i].name+' ('+ws_search_result_array.glassArray[i].total+')</label></div>';
		}
		$(".filter-group-glass .filter-group-toggle").html(glassHTML);
		eventCheckBox("glass");
	}else{
		$(".filter-group-glass .filter-group-toggle").html(noResultHTML());
	}
	/*update numeral checkbox*/
	if(ws_search_result_array.numeralArray.length!=0){
		$(".filter-group-numeral .filter-group-toggle").empty();
		var numeralHTML="";
		for(i=0;i<ws_search_result_array.numeralArray.length;i++){
			numeralHTML+='<div class="custom-control custom-checkbox filter-checkbox numeral-checkbox">';
			numeralHTML+='<input type="checkbox" value="'+ws_search_result_array.numeralArray[i].id+'" class="custom-control-input" id="numeral-'+ws_search_result_array.numeralArray[i].id+'"';
			if(Array.isArray(vars["numeral"])){
				if(vars["numeral"].indexOf(ws_search_result_array.numeralArray[i].id) != -1){
					numeralHTML+=" checked";
				}
			}
			numeralHTML+='><label class="custom-control-label" for="numeral-'+ws_search_result_array.numeralArray[i].id+'">'+ws_search_result_array.numeralArray[i].name+' ('+ws_search_result_array.numeralArray[i].total+')</label></div>';
		}
		$(".filter-group-numeral .filter-group-toggle").html(numeralHTML);
		eventCheckBox("numeral");
	}else{
		$(".filter-group-numeral .filter-group-toggle").html(noResultHTML());
	}
	/*update buckle checkbox*/
	if(ws_search_result_array.buckleArray.length!=0){
		$(".filter-group-buckle .filter-group-toggle").empty();
		var buckleHTML="";
		for(i=0;i<ws_search_result_array.buckleArray.length;i++){
			buckleHTML+='<div class="custom-control custom-checkbox filter-checkbox buckle-checkbox">';
			buckleHTML+='<input type="checkbox" value="'+ws_search_result_array.buckleArray[i].id+'" class="custom-control-input" id="buckle-'+ws_search_result_array.buckleArray[i].id+'"';
			if(Array.isArray(vars["buckle"])){
				if(vars["buckle"].indexOf(ws_search_result_array.buckleArray[i].id) != -1){
					buckleHTML+=" checked";
				}
			}
			buckleHTML+='><label class="custom-control-label" for="buckle-'+ws_search_result_array.buckleArray[i].id+'">'+ws_search_result_array.buckleArray[i].name+' ('+ws_search_result_array.buckleArray[i].total+')</label></div>';
		}
		$(".filter-group-buckle .filter-group-toggle").html(buckleHTML);
		eventCheckBox("buckle");
	}else{
		$(".filter-group-buckle .filter-group-toggle").html(noResultHTML());
	}
	/*update buckleMaterial checkbox*/
	if(ws_search_result_array.buckleMaterialArray.length!=0){
		$(".filter-group-buckleMaterial .filter-group-toggle").empty();
		var buckleMaterialHTML="";
		for(i=0;i<ws_search_result_array.buckleMaterialArray.length;i++){
			buckleMaterialHTML+='<div class="custom-control custom-checkbox filter-checkbox buckleMaterial-checkbox">';
			buckleMaterialHTML+='<input type="checkbox" value="'+ws_search_result_array.buckleMaterialArray[i].id+'" class="custom-control-input" id="buckleMaterial-'+ws_search_result_array.buckleMaterialArray[i].id+'"';
			if(Array.isArray(vars["buckleMaterial"])){
				if(vars["buckleMaterial"].indexOf(ws_search_result_array.buckleMaterialArray[i].id) != -1){
					buckleMaterialHTML+=" checked";
				}
			}
			buckleMaterialHTML+='><label class="custom-control-label" for="buckleMaterial-'+ws_search_result_array.buckleMaterialArray[i].id+'">'+ws_search_result_array.buckleMaterialArray[i].name+' ('+ws_search_result_array.buckleMaterialArray[i].total+')</label></div>';
		}
		$(".filter-group-buckleMaterial .filter-group-toggle").html(buckleMaterialHTML);
		eventCheckBox("buckleMaterial");
	}else{
		$(".filter-group-buckleMaterial .filter-group-toggle").html(noResultHTML());
	}
}

function loadBrandMultiselect(){
	/*multiselect for brand filter in popup*/
	$('#brand-dropdown').multiselect({
		columns: 1,
		maxHeight: "73vh",
		placeholder: 'Select Brands',
		search: true,
		selectAll: false,
		maxSelected: 4,
		onLoad: function(element){
			$("#brand-dropdown").next().find("ul").wrap( "<div class='ms-options-container'></div>" );
		},
		onOptionClick: function( element, option ){
			var thisOpt = $(option);

			if($(".brand-chosen-items").find(".delete[data-value='"+thisOpt.val()+"']").length==0){
				add_brand_tag("brand",thisOpt.attr("title"), thisOpt.val());
			}else{
				remove_brand_tag("brand",thisOpt.val());
			}
			updateFiltersInPopup();
		}
	});
}

function loadCaseMaterialMultiselect(){
	/*multiselect for brand filter in popup*/
	$('#caseMaterial-dropdown').multiselect({
		columns: 1,
		maxHeight: "73vh",
		placeholder: 'Select Case Materials',
		search: true,
		selectAll: false,
		maxSelected: 4,
		onLoad: function(element){
			$("#caseMaterial-dropdown").next().find("ul").wrap( "<div class='ms-options-container'></div>" );
		},
		onOptionClick: function( element, option ){
			var thisOpt = $(option);
			if($(".caseMaterial-chosen-items").find(".delete[data-value='"+thisOpt.val()+"']").length==0){
				add_brand_tag("caseMaterial",thisOpt.attr("title"), thisOpt.val());
			}else{
				remove_brand_tag("caseMaterial",thisOpt.val());
			}
			updateFiltersInPopup();
		}
	});
}

function loadMultiSelectDropdown(){
	/*multiselect for dealer filter in popup*/
	$('#dealer-dropdown').multiselect({
		columns: 1,
		maxHeight: "73vh",
		placeholder: 'Select Dealers',
		search: true,
		selectAll: false,
		maxSelected: 4,
		onLoad: function(element){
			$("#dealer-dropdown").next().find("ul").wrap( "<div class='ms-options-container'></div>" );
			$("#dealer-dropdown").next().find("ul").children().each(function(){
				$(this).children().append(" <i class='fas fa-city' style='float:right'></i>");
			});
		},
		onOptionClick: function( element, option ){
			var thisOpt = $(option);

			if($(".dealer-chosen-items").find(".delete[data-value='"+thisOpt.val()+"']").length==0){
				add_brand_tag("dealer",thisOpt.attr("title"), thisOpt.val());
			}else{
				remove_brand_tag("dealer",thisOpt.val());
			}
			updateFiltersInPopup();
		}
	});
}
function updateFiltersInPopup(){
	var parameters={
		"action": "update_counter",
		"re_string": vars["re_string"],
		"manufactures": [],
		"caseMaterials":[],
		"priceFrom":0,
		"priceTo":0,
	};
	var _priceFrom=parseInt($(".price-range-from").val());
	var _priceTo=parseInt($(".price-range-to").val());

	if($(".brand-chosen-items .label-tag").length != 0){
		parameters.manufactures=[];
		$(".brand-chosen-items .label-tag").each(function(){
			parameters.manufactures.push($(this).find(".delete").attr("data-value"));
		});
		parameters.adsearch="true";
	}

	if($(".caseMaterial-chosen-items .label-tag").length != 0){
		parameters.caseMaterials=[];
		$(".caseMaterial-chosen-items .label-tag").each(function(){
			parameters.caseMaterials.push($(this).find(".delete").attr("data-value"));
		});
		parameters.adsearch="true";
	}

	if($(".movement-checkbox .custom-control-input:checked").length != 0){
		parameters.movement=[];
		$(".movement-checkbox .custom-control-input:checked").each(function(){
			parameters.movement.push($(this).val());
		});
		parameters.adsearch="true";
	}

	if($(".braceletMaterial-checkbox .custom-control-input:checked").length != 0){
		parameters.braceletMaterial=[];
		$(".braceletMaterial-checkbox .custom-control-input:checked").each(function(){
			parameters.braceletMaterial.push($(this).val());
		});
		parameters.adsearch="true";
	}

	if($(".braceletColor-checkbox .custom-control-input:checked").length != 0){
		parameters.braceletColor=[];
		$(".braceletColor-checkbox .custom-control-input:checked").each(function(){
			parameters.braceletColor.push($(this).val());
		});
		parameters.adsearch="true";
	}

	if($(".dialColor-checkbox .custom-control-input:checked").length != 0){
		parameters.dialColors=[];
		$(".dialColor-checkbox .custom-control-input:checked").each(function(){
			parameters.dialColors.push($(this).val());
		});
		parameters.adsearch="true";
	}

	if($(".gender-checkbox .custom-control-input:checked").length != 0){
		parameters.gender="";
		$(".gender-checkbox .custom-control-input:checked").each(function(){
			parameters.gender=$(this).val();
		});
		parameters.adsearch="true";
	}

	if($(".glass-checkbox .custom-control-input:checked").length != 0){
		parameters.glass=[];
		$(".glass-checkbox .custom-control-input:checked").each(function(){
			parameters.glass.push($(this).val());
		});
		parameters.adsearch="true";
	}
	if($(".numeral-checkbox .custom-control-input:checked").length != 0){
		parameters.numeral=[];
		$(".numeral-checkbox .custom-control-input:checked").each(function(){
			parameters.numeral.push($(this).val());
		});
		parameters.adsearch="true";
	}
	if($(".buckle-checkbox .custom-control-input:checked").length != 0){
		parameters.buckle=[];
		$(".buckle-checkbox .custom-control-input:checked").each(function(){
			parameters.buckle.push($(this).val());
		});
		parameters.adsearch="true";
	}
	if($(".buckleMaterial-checkbox .custom-control-input:checked").length != 0){
		parameters.buckleMaterial=[];
		$(".buckleMaterial-checkbox .custom-control-input:checked").each(function(){
			parameters.buckleMaterial.push($(this).val());
		});
		parameters.adsearch="true";
	}

	if(_priceFrom>=0&&_priceTo>=0){
		if(_priceFrom > _priceTo){
			_priceTo=_priceFrom;
		}
		parameters.priceFrom=_priceFrom;
		parameters.priceTo=_priceTo;
		parameters.adsearch="true";
	}

	$(".filters-blockUI").show();
	$(".search-filter-btn").html('<i class="fas fa-circle-notch fa-spin"></i> Search...');
	$.ajax({
	  url: ws_ajaxSearch_url,
	  type: 'post',
	  dataType: "json",
	  data: parameters,
	  success: function( data ) {
			updateFilters(data);
			$(".filters-blockUI").hide();
			$(".search-filter-btn").html("Show Results ("+data.totalResults+")");
	  }
	});
}
function updateFilters(data){
	var currentTab=$(".filter-header-chosen").attr("data-type");
	/*update brand dropdown*/
	if(data.brandArray.length!=0){
		var selectedBrands=[];
		$( "#brand-dropdown" ).next().find(".selected").each(function(){
			selectedBrands.push($(this).find("input").val());
		});
		$("#brand-dropdown").empty();
		$(".brand-chosen-items").empty();
		var brandHTML="";
		var chosenBrandHTML="";
		for(i=0;i<data.brandArray.length;i++){
			brandHTML+="<option value='"+data.brandArray[i].id+"'";
			if(selectedBrands.indexOf(data.brandArray[i].id) != -1){
				brandHTML+=" selected";
				chosenBrandHTML+='<span class="label-tag"><span class="text">'+data.brandArray[i].name+'</span><span class="delete" data-type="manufactures[]" data-value="'+data.brandArray[i].id+'">x</span></span>';
			}
			brandHTML+=">"+data.brandArray[i].name+" ("+data.brandArray[i].total+")</option>";
		}
		$(".brand-chosen-items").append(chosenBrandHTML);
		$("#brand-dropdown").append(brandHTML);
		$('#brand-dropdown').multiselect( 'reload' );
		$(".brand-chosen-items .label-tag").unbind("click");
		$(".brand-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#brand-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}else{
		$(".brand-chosen-items").empty();
		$(".brand-dropdown-container").hide();
		$(".brand-no-results").show();
	}
	/*update case material dropdown*/
	if(data.caseMaterialArray.length!=0){
		var selectedCaseMaterials=[];
		$( "#caseMaterial-dropdown" ).next().find(".selected").each(function(){
			selectedCaseMaterials.push($(this).find("input").val());
		});
		$("#caseMaterial-dropdown").empty();
		$(".caseMaterial-chosen-items").empty();
		var caseMaterialHTML="";
		var chosencaseMaterialHTML="";
		for(i=0;i<data.caseMaterialArray.length;i++){
			caseMaterialHTML+="<option value='"+data.caseMaterialArray[i].id+"'";
			if(selectedCaseMaterials.indexOf(data.caseMaterialArray[i].id) != -1){
				caseMaterialHTML+=" selected";
				chosencaseMaterialHTML+='<span class="label-tag"><span class="text">'+data.caseMaterialArray[i].name+'</span><span class="delete" data-type="caseMaterials[]" data-value="'+data.caseMaterialArray[i].id+'">x</span></span>';
			}
			caseMaterialHTML+=">"+data.caseMaterialArray[i].name+" ("+data.caseMaterialArray[i].total+")</option>";
		}
		$(".caseMaterial-chosen-items").append(chosencaseMaterialHTML);
		$("#caseMaterial-dropdown").append(caseMaterialHTML);
		$('#caseMaterial-dropdown').multiselect( 'reload' );
		$(".caseMaterial-chosen-items .label-tag").unbind("click");
		$(".caseMaterial-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#caseMaterial-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}else{
		$(".caseMaterial-chosen-items").empty();
		$(".case-dropdown-container").hide();
		$(".caseMaterial-no-results").show();
	}
	/*update movement checkbox*/
	if(data.movementArray.length!=0){
		var selectedMovements=[];
		$(".movement-checkbox .custom-control-input:checked").each(function(){
			selectedMovements.push($(this).val());
		});
		$(".filter-group-movement .filter-group-toggle").empty();
		var movementHTML="";
		for(i=0;i<data.movementArray.length;i++){
			movementHTML+='<div class="custom-control custom-checkbox filter-checkbox movement-checkbox">';
			movementHTML+='<input type="checkbox" value="'+data.movementArray[i].id+'" class="custom-control-input" id="movement-'+data.movementArray[i].id+'"';
			if(selectedMovements.indexOf(data.movementArray[i].id) != -1){
				movementHTML+=" checked";
			}
			movementHTML+='><label class="custom-control-label" for="movement-'+data.movementArray[i].id+'">'+data.movementArray[i].name+' ('+data.movementArray[i].total+')</label></div>';
		}
		$(".filter-group-movement .filter-group-toggle").html(movementHTML);
		eventCheckBox("movement");
	}else{
		$(".filter-group-movement .filter-group-toggle").html(noResultHTML());
	}
	/*update bracelet material checkbox*/
	if(data.braceletMaterialArray.length!=0){
		var selectedbraceletMaterials=[];
		$(".braceletMaterial-checkbox .custom-control-input:checked").each(function(){
			selectedbraceletMaterials.push($(this).val());
		});
		$(".filter-group-braceletMaterial .filter-group-toggle").empty();
		var braceletMaterialHTML="";
		for(i=0;i<data.braceletMaterialArray.length;i++){
			braceletMaterialHTML+='<div class="custom-control custom-checkbox filter-checkbox braceletMaterial-checkbox">';
			braceletMaterialHTML+='<input type="checkbox" value="'+data.braceletMaterialArray[i].id+'" class="custom-control-input" id="braceletMaterial-'+data.braceletMaterialArray[i].id+'"';
			if(selectedbraceletMaterials.indexOf(data.braceletMaterialArray[i].id) != -1){
				braceletMaterialHTML+=" checked";
			}
			braceletMaterialHTML+='><label class="custom-control-label" for="braceletMaterial-'+data.braceletMaterialArray[i].id+'">'+data.braceletMaterialArray[i].name+' ('+data.braceletMaterialArray[i].total+')</label></div>';
		}
		$(".filter-group-braceletMaterial .filter-group-toggle").html(braceletMaterialHTML);
		eventCheckBox("braceletMaterial");
	}else{
		$(".filter-group-braceletMaterial .filter-group-toggle").html(noResultHTML());
	}
	/*update bracelet color checkbox*/
	if(data.braceletColorArray.length!=0){
		var selectedbraceletColors=[];
		$(".braceletColor-checkbox .custom-control-input:checked").each(function(){
			selectedbraceletColors.push($(this).val());
		});
		$(".filter-group-braceletColor .filter-group-toggle").empty();
		var braceletColorHTML="";
		for(i=0;i<data.braceletColorArray.length;i++){
			braceletColorHTML+='<div class="custom-control custom-checkbox filter-checkbox braceletColor-checkbox">';
			braceletColorHTML+='<input type="checkbox" value="'+data.braceletColorArray[i].id+'" class="custom-control-input" id="braceletColor-'+data.braceletColorArray[i].id+'"';
			if(selectedbraceletColors.indexOf(data.braceletColorArray[i].id) != -1){
				braceletColorHTML+=" checked";
			}
			braceletColorHTML+='><label class="custom-control-label" for="braceletColor-'+data.braceletColorArray[i].id+'">'+data.braceletColorArray[i].name+' ('+data.braceletColorArray[i].total+')</label></div>';
		}
		$(".filter-group-braceletColor .filter-group-toggle").html(braceletColorHTML);
		eventCheckBox("braceletColor");
	}else{
		$(".filter-group-braceletColor .filter-group-toggle").html(noResultHTML());
	}
	/*update dial color checkbox*/
	if(data.dialColorArray.length!=0){
		var selectedDialColors=[];
		$(".dialColor-checkbox .custom-control-input:checked").each(function(){
			selectedDialColors.push($(this).val());
		});
		$(".filter-group-dialColor .filter-group-toggle").empty();
		var dialColorHTML="";
		for(i=0;i<data.dialColorArray.length;i++){
			dialColorHTML+='<div class="custom-control custom-checkbox filter-checkbox dialColor-checkbox">';
			dialColorHTML+='<input type="checkbox" value="'+data.dialColorArray[i].id+'" class="custom-control-input" id="dialColor-'+data.dialColorArray[i].id+'"';
			if(selectedDialColors.indexOf(data.dialColorArray[i].id) != -1){
				dialColorHTML+=" checked";
			}
			dialColorHTML+='><label class="custom-control-label" for="dialColor-'+data.dialColorArray[i].id+'">'+data.dialColorArray[i].name+' ('+data.dialColorArray[i].total+')</label></div>';
		}
		$(".filter-group-dialColor .filter-group-toggle").html(dialColorHTML);
		eventCheckBox("dialColor");
	}else{
		$(".filter-group-dialColor .filter-group-toggle").html(noResultHTML());
	}

	/*update gender checkbox*/
	if(data.genderArray.length!=0){
		var selectedgenders;
		$(".gender-checkbox .custom-control-input:checked").each(function(){
			selectedgenders=$(this).val();
		});
		$(".filter-group-gender .filter-group-toggle").empty();
		var genderHTML="";
		for(i=0;i<data.genderArray.length;i++){
			genderHTML+='<div class="custom-control custom-checkbox filter-checkbox gender-checkbox">';
			genderHTML+='<input type="checkbox" value="'+data.genderArray[i].name+'" class="custom-control-input" id="gender-'+data.genderArray[i].id+'"';
			if(selectedgenders == data.genderArray[i].name){
				genderHTML+=" checked";
			}
			genderHTML+='><label class="custom-control-label" for="gender-'+data.genderArray[i].id+'">'+data.genderArray[i].name+' ('+data.genderArray[i].total+')</label></div>';
		}
		$(".filter-group-gender .filter-group-toggle").html(genderHTML);
		eventCheckBox("gender");
	}else{
		$(".filter-group-gender .filter-group-toggle").html(noResultHTML());
	}

	/*update glass checkbox*/
	if(data.glassArray.length!=0){
		var selectedglass=[];
		$(".glass-checkbox .custom-control-input:checked").each(function(){
			selectedglass=$(this).val();
		});
		$(".filter-group-glass .filter-group-toggle").empty();
		var glassHTML="";
		for(i=0;i<data.glassArray.length;i++){
			glassHTML+='<div class="custom-control custom-checkbox filter-checkbox glass-checkbox">';
			glassHTML+='<input type="checkbox" value="'+data.glassArray[i].id+'" class="custom-control-input" id="glass-'+data.glassArray[i].id+'"';
			if(selectedglass.indexOf(data.glassArray[i].id) != -1){
				glassHTML+=" checked";
			}
			glassHTML+='><label class="custom-control-label" for="glass-'+data.glassArray[i].id+'">'+data.glassArray[i].name+' ('+data.glassArray[i].total+')</label></div>';
		}
		$(".filter-group-glass .filter-group-toggle").html(glassHTML);
		eventCheckBox("glass");
	}else{
		$(".filter-group-glass .filter-group-toggle").html(noResultHTML());
	}
	/*update numeral checkbox*/
	if(data.numeralArray.length!=0){
		var selectedNumeral=[];
		$(".numeral-checkbox .custom-control-input:checked").each(function(){
			selectedNumeral=$(this).val();
		});
		$(".filter-group-numeral .filter-group-toggle").empty();
		var numeralHTML="";
		for(i=0;i<data.numeralArray.length;i++){
			numeralHTML+='<div class="custom-control custom-checkbox filter-checkbox numeral-checkbox">';
			numeralHTML+='<input type="checkbox" value="'+data.numeralArray[i].id+'" class="custom-control-input" id="numeral-'+data.numeralArray[i].id+'"';
			if(selectedNumeral.indexOf(data.numeralArray[i].id) != -1){
				numeralHTML+=" checked";
			}
			numeralHTML+='><label class="custom-control-label" for="numeral-'+data.numeralArray[i].id+'">'+data.numeralArray[i].name+' ('+data.numeralArray[i].total+')</label></div>';
		}
		$(".filter-group-numeral .filter-group-toggle").html(numeralHTML);
		eventCheckBox("numeral");
	}else{
		$(".filter-group-numeral .filter-group-toggle").html(noResultHTML());
	}
	/*update buckle checkbox*/
	if(data.buckleArray.length!=0){
		var selectedbuckle=[];
		$(".buckle-checkbox .custom-control-input:checked").each(function(){
			selectedbuckle=$(this).val();
		});
		$(".filter-group-buckle .filter-group-toggle").empty();
		var buckleHTML="";
		for(i=0;i<data.buckleArray.length;i++){
			buckleHTML+='<div class="custom-control custom-checkbox filter-checkbox buckle-checkbox">';
			buckleHTML+='<input type="checkbox" value="'+data.buckleArray[i].id+'" class="custom-control-input" id="buckle-'+data.buckleArray[i].id+'"';
			if(selectedbuckle.indexOf(data.buckleArray[i].id) != -1){
				buckleHTML+=" checked";
			}
			buckleHTML+='><label class="custom-control-label" for="buckle-'+data.buckleArray[i].id+'">'+data.buckleArray[i].name+' ('+data.buckleArray[i].total+')</label></div>';
		}
		$(".filter-group-buckle .filter-group-toggle").html(buckleHTML);
		eventCheckBox("buckle");
	}else{
		$(".filter-group-buckle .filter-group-toggle").html(noResultHTML());
	}

	if(data.buckleMaterialArray.length!=0){
		var selectedbuckleMaterial=[];
		$(".buckleMaterial-checkbox .custom-control-input:checked").each(function(){
			selectedbuckleMaterial=$(this).val();
		});
		$(".filter-group-buckleMaterial .filter-group-toggle").empty();
		var buckleMaterialHTML="";
		for(i=0;i<data.buckleMaterialArray.length;i++){
			buckleMaterialHTML+='<div class="custom-control custom-checkbox filter-checkbox buckleMaterial-checkbox">';
			buckleMaterialHTML+='<input type="checkbox" value="'+data.buckleMaterialArray[i].id+'" class="custom-control-input" id="buckleMaterial-'+data.buckleMaterialArray[i].id+'"';
			if(selectedbuckleMaterial.indexOf(data.buckleMaterialArray[i].id) != -1){
				buckleMaterialHTML+=" checked";
			}
			buckleMaterialHTML+='><label class="custom-control-label" for="buckleMaterial-'+data.buckleMaterialArray[i].id+'">'+data.buckleMaterialArray[i].name+' ('+data.buckleMaterialArray[i].total+')</label></div>';
		}
		$(".filter-group-buckleMaterial .filter-group-toggle").html(buckleMaterialHTML);
		eventCheckBox("buckleMaterial");
	}else{
		$(".filter-group-buckleMaterial .filter-group-toggle").html(noResultHTML());
	}
}
var $ = jQuery.noConflict();
$(function(){
	$("#ws-search-pagination").pagination({
        pages: ws_pagination_pages,
        cssStyle: 'light-theme',
		displayedPages: 3,
		currentPage: ws_pagination_currentPage,
		hrefTextPrefix: ws_hrefTextPrefix,
		onInit: function(){
			$("#ws-search-pagination ul").addClass("pagination justify-content-center");
		},
		onPageClick : function(){
			$("#ws-search-pagination ul").addClass("pagination justify-content-center");
		}
    });

	$('.price-range-slider').jRange({
		from: 0,
		to: 300000,
		step: 1,
		format: '%s $',
		width: '90%',
		showLabels: false,
		isRange : true,
		ondragend: function(){
			updateFiltersInPopup();
		}
	});

	$(".filter-group").click(function(){
		if($(this).hasClass("filter-closed")){
			$(this).removeClass("filter-closed");
			$(this).addClass("filter-opened");
			$(this).find(".filter-group-toggle").slideDown("slow");
		}else{
			$(this).removeClass("filter-opened");
			$(this).addClass("filter-closed");
			$(this).find(".filter-group-toggle").slideUp("slow");
		}
	});

	$(".price-range-slider").change(function(){
		var rangeValue=$(this).val().split(",");
		$(".price-range-from").val(rangeValue[0]);
		if(parseInt(rangeValue[1]) != 300000)
			$(".price-range-to").val(rangeValue[1]);
		else
			$(".price-range-to").val("> 300000");
	});

	loadBrandMultiselect();
	loadCaseMaterialMultiselect();
	eventCheckBox("movement");
	eventCheckBox("braceletMaterial");
	eventCheckBox("gender");
	eventCheckBox("braceletColor");
	eventCheckBox("dialColor");
	eventCheckBox("glass");
	eventCheckBox("numeral");
	eventCheckBox("buckle");
	eventCheckBox("buckleMaterial");

	$(".view-item").click(function(){
		var data=$(this).attr("data-value");
		var link=window.location.href.replace("&resultview=grid","").replace("&resultview=list","");
		window.location.href=link+"&resultview="+data;
	});

	$("#search-sort-by").change(function(){
		var data=$(this).val();
		var link=window.location.href.replace("&sortBy=Relevance","").replace("&sortBy=Newest","").replace("&sortBy=PriceLowToHigh","").replace("&sortBy=PriceHighToLow","");
		window.location.href=link+"&sortBy="+data;
	});

	//event for filter tab on header
	$(".filter-header").click(function(){
		if($(".filter-popup").is(":hidden")){
			//find brand in search string
			var matched_array=[];
			for(j=0;j<ws_brand_array.length;j++){
				var regex = new RegExp( ws_brand_array[j]["name"], 'gi' );
				var matched=vars["re_string"].match(regex);
				if(matched != null){
					if($(".brand-chosen-items").find(".delete[data-value='"+ws_brand_array[j]["id"]+"']").length==0){
						$("#brand-dropdown").next().find("input[value='"+ws_brand_array[j]["id"]+"']").click();
					}
				}
			}

			$("#filters-container").addClass("filter-fixed");
			$(".filter-header-container").css("z-index","1050");
			$(".filter-overlay").show();
		}
			$(".filter-header").removeClass("filter-header-chosen");
			$(this).addClass("filter-header-chosen");
		var type=$(this).attr("data-type");
			$(".filter-content").hide();
			$("#filter-"+type).show();
			$(".filter-popup").show();

		if(type=="Price"){
			if(vars["conditions"] != "all"){
				for(i=0;i<vars["conditions"].length;i++){
					$("#condition-"+vars["conditions"][i]).click();
				}
			}

			if($(".price-range-from").val()=="" && $(".price-range-to").val()==""){
				if(ws_search_result_array.MinPrice != null && ws_search_result_array.MaxPrice != null)
					$('.price-range-slider').jRange('setValue', parseInt(ws_search_result_array.MinPrice)+','+parseInt(ws_search_result_array.MaxPrice));
				else if(vars["priceFrom"] != 0 && vars["priceTo"] != 0)
					$('.price-range-slider').jRange('setValue', parseInt(vars["priceFrom"])+','+parseInt(vars["priceTo"]));
			}
		}

	});

	//add function for delete filter tags
	$(".filters-tag .label-tag").click(function(){
		var type=$(this).find(".delete").attr("data-type");
		var data=$(this).find(".delete").attr("data-value");
		var replaceText="&"+type+"="+data;
		if(type=="price-range"){
			replaceText="&priceFrom="+data.split("-")[0]+"&priceTo="+data.split("-")[1];
		}
		var link=window.location.href.replace(replaceText,"");
		if($(".filters-tag").find(".label-tag").length == 1){
			link=link.replace("adsearch=true","");
		}
		window.location.href=link;
	});

	$(".popular-brand li").click(function(){
		var value=$(this).attr("data-value");

		if($(".brand-chosen-items").find(".delete[data-value='"+value+"']").length==0){
			$("#brand-dropdown").next().find("input[value='"+value+"']").click();
		}
	});

	$(".cancel-filter-btn").click(function(){
		$(".brand-chosen-items").empty();
		$(".dealer-chosen-items").empty();
		$(".caseMaterial-chosen-items").empty();
		set_filter_default();
		$(".filter-header-container").css("z-index","");
		$(".filter-overlay").hide();
		$(".filter-popup").hide();
		$("#filters-container").removeClass("filter-fixed");
		$(".filter-header").removeClass("filter-header-chosen");
	});

	if($(".brand-chosen-items .label-tag").length != 0){
		$(".brand-chosen-items .label-tag").unbind("click");
		$(".brand-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#brand-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}

	if($(".caseMaterial-chosen-items .label-tag").length != 0){
		$(".caseMaterial-chosen-items .label-tag").unbind("click");
		$(".caseMaterial-chosen-items .label-tag").click(function(){
			var value=$(this).find(".delete").attr("data-value");
			$("#caseMaterial-dropdown").next().find(".selected").find("input[value='"+value+"']").click();
		});
	}

	/*search button in filter popup*/
	$(".search-filter-btn").click(function(){
		//get selected brands
		var isAdvanceSearch=false;
		var newSearchLink=ws_homeUrl+"/search/?query="+vars["re_string"];
		var _priceFrom=parseInt($(".price-range-from").val());
		var _priceTo=parseInt($(".price-range-to").val());

		if($(".brand-chosen-items .label-tag").length != 0){
			$(".brand-chosen-items .label-tag").each(function(){
				newSearchLink+="&manufactures[]="+$(this).find(".delete").attr("data-value");
			});
			isAdvanceSearch=true;
		}

		if($(".caseMaterial-chosen-items .label-tag").length != 0){
			$(".caseMaterial-chosen-items .label-tag").each(function(){
				newSearchLink+="&caseMaterials[]="+$(this).find(".delete").attr("data-value");
			});
			isAdvanceSearch=true;
		}

		if($(".gender-checkbox .custom-control-input:checked").length != 0){
			$(".gender-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&gender="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".movement-checkbox .custom-control-input:checked").length != 0){
			$(".movement-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&movement[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".braceletMaterial-checkbox .custom-control-input:checked").length != 0){
			$(".braceletMaterial-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&braceletMaterial[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".dialColor-checkbox .custom-control-input:checked").length != 0){
			$(".dialColor-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&dialColors[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".glass-checkbox .custom-control-input:checked").length != 0){
			$(".glass-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&glass[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".buckle-checkbox .custom-control-input:checked").length != 0){
			$(".buckle-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&buckle[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".buckleMaterial-checkbox .custom-control-input:checked").length != 0){
			$(".buckleMaterial-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&buckleMaterial[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		if($(".numeral-checkbox .custom-control-input:checked").length != 0){
			$(".numeral-checkbox .custom-control-input:checked").each(function(){
				newSearchLink+="&numeral[]="+$(this).val();
			});
			isAdvanceSearch=true;
		}

		/*if($(".condition-checkbox").prev().is(":checked")){
			$(".condition-checkbox").each(function(){
				if($(this).prev().is(":checked")){
					newSearchLink+="&conditions[]="+$(this).prev(":checked").val();
				}
			});
		}*/

		if(_priceFrom>=0&&_priceTo>=0){
			if(_priceFrom > _priceTo){
				_priceTo=_priceFrom;
				$(".price-range-to").val(_priceTo);
			}

			newSearchLink+="&priceFrom="+_priceFrom+"&priceTo="+_priceTo;
			isAdvanceSearch=true;
		}
		//console.log(newSearchLink);

		if(isAdvanceSearch==true)
			window.location.href=newSearchLink+"&adsearch=true";
		else
			window.location.href=newSearchLink;
	});
});
