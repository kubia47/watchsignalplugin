var $ = jQuery.noConflict();
$(document).ready(function($) {
	$('.srch-field').keydown(function (event) {
    var ajaxsearchitem = $(this);
    if (
        (event.which && event.which == 39)
        || (event.keyCode && event.keyCode == 39)
        || (event.which && event.which == 37)
        || (event.keyCode && event.keyCode == 37))
    {
        //do nothing on left and right arrows
        console.log('do nothing on left and right arrows');
        return;
    }

    if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
        // on enter
        console.log('enter');
				window.location.href=ws_homeUrl+"/search/?query="+ajaxsearchitem.val();
        return false; //redirect for search on enter
    } else {

        if ((event.which && event.which == 40) || (event.keyCode && event.keyCode == 40)) {
            // down
            console.log(ajaxsearchitem.val());
            return false; //disable the envent

        } else if((event.which && event.which == 38) || (event.keyCode && event.keyCode == 38)) {
            // up
            console.log(ajaxsearchitem.val());
            return false; // disable the envent
        } else {

            // for backspace we have to check if the search query is empty and if so, clear the list
            if ((event.which && event.which == 8) || (event.keyCode && event.keyCode == 8)) {
                //if we have just one character left, that means it will be deleted now and we also have to clear the search results list
								var search_query = jQuery(this).val();
								if (search_query.length == 1) {
										//clear all results in dropdown
								}
            }

            setTimeout(function(){
							if (ajaxsearchitem.val() !== "" && ajaxsearchitem.val().length >= 3) {
								$(".fa-search-icon").addClass("fa-spinner fa-spin");

								$.ajax ({
				          url: ws_ajaxSearch_url,
						  		type: 'post',
				          dataType: "json",
				          data: {
										action: 'ws_ajax_search',
				            re_string: ajaxsearchitem.val()
				          },
				          success: function (data) {
				            $(".fa-search-icon").removeClass("fa-spinner fa-spin");
										$(".search-result-wrap").empty();
										data.forEach (function(currentValue){
											var bolText = currentValue.label.replace(new RegExp("("+ajaxsearchitem.val()+")","gi"), "<span class='result-suggestion-bold-color'>$1</span>");
											var itemHTML = "<div class='col-md-12 search-item' data-key='"+currentValue.value+"'>"+bolText+"</div>";
											$(".search-result-wrap").append(itemHTML);
										});

										$(".search-dropdown-wrap").show();

										$(".search-item").click(function(){
							        window.location.href=ws_homeUrl+"/"+$(this).attr('data-key');
										});
				          },
									error: function (MLHttpRequest, textStatus, errorThrown) {
			              console.log(errorThrown);
			            }
				        });
							} else {
								$(".fa-search-icon").removeClass("fa-spinner fa-spin");
							}
            }, 100)
        }
        return true
    }
  });

	$( ".header-search-area input" ).autocomplete({
		source: function( request, response ) {
			$.ajax( {
				url: ws_ajaxSearch_url,
				type: 'post',
				dataType: "json",
				data: {
					action: 'ws_ajax_search',
					re_string: request.term
				},
				success: function( data ) {
					response( data );
				}
			} );
		},
		position: { my: "left top-15", at: "left bottom"},
		minLength: 2,
		select: function( event, ui ) {
			$( ".header-search-area input" ).val(ui.item.label);
        var pro_slug = convertToSlug(ui.item.label) + '_' + ui.item.value;
        window.location.href=ws_homeUrl+"/"+pro_slug;
        return false;
			},
		focus: function(event, ui) {
			$( ".header-search-area input" ).val(ui.item.label);
		},
		open: function (e, ui) {
	 		var position = $("#ui-id-1").position(),
	 		left = position.left, top = position.top;
	 		$("#ui-id-1").css({
				width: $(".header-search-area input").width()+12+"px",
				top: top+14 });
			$(".header-search-area input").removeClass("ui-autocomplete-loading");
			var acData = $(this).data('ui-autocomplete');
					acData
					.menu
					.element
					.find('li')
					.each(function () {
						var me = $(this);
						var keywords = acData.term.split(' ').join('|');
						me.html(me.text().replace(new RegExp("(" + keywords + ")", "gi"), "<span class='result-suggestion-bold-color'>$1</span>"));
					});
				}
			});

	$(".search-btn-wrap").click(function(){
		if($(".srch-field").val().length != 0){
			window.location=ws_homeUrl+"/search/?query="+convertToSlug($(".srch-field").val());
		}
	});

	$("#subscribe-form").submit(function(e){
      e.preventDefault();
  });

	$(".frm-footer").submit(function(e){
      e.preventDefault();
  });

	$(document).click( function(e){
		if($(e.target).closest(".search-input-wrap").length==0){
			$(".search-dropdown-wrap").hide();
		}
		if($(e.target).closest(".tab-wrapper").length==0){
				$("#ui-id-1").hide();
				$(".tab-wrapper").hide();
		}
	});
});

function convertToSlug(Text){
    return Text
        .toLowerCase()
        .replace(/ /g,'_')
        .replace(/[^\w-]+/g,'')
        ;
}
