var $ = jQuery.noConflict();

$(document).ready(function () {
  //your code here
  wsSelectRating();

$('.nav-tabs a').click(function(){
    $('.nav-tabs li').removeClass('active');
    $(this).parent().addClass('active');
});

$('.compare-block, .compare-price-item').click(function(){
    var _href = $(this).attr('datalink');
    window.open(_href, '_blank');
});

//coupon
$(".coupon-block").click(function(event) {
    event.stopPropagation();
});

$(".coupon-block .set-coupon").click(function(event) {
    setCoupon($(this), 'SET');
});

$(".coupon-block .update-coupon").click(function(event) {
    setCoupon($(this), 'UPDATE');
});

$(".coupon-block .remove-coupon").click(function(event) {
    setCoupon($(this), 'REMOVE');
});

var setCoupon = function(_this, type){
    //parent
    $_parent = _this.parent();

    //get current dynamic id
    var staticId = _this.attr('static-id');
    var websourceId = _this.attr('websource-id');
    var couponCode = $_parent.children('.coupon-input').val();

    if(couponCode != ''){
        //hide button
        _this.hide();

        var _action = (type == 'SET') ? 'setcoupon' : ((type == 'UPDATE') ? 'updatecoupon' : 'removecoupon');

        //do ajax
        var data = {
            'action': _action,
            'staticId': staticId,
            'websourceId': websourceId,
            'couponCode': couponCode
        }
        $.post(ws_plugin_url+"includes/coupon-ajax.php", data, function(response) {
            var result = JSON.parse(response);

            if(result.status && _action != 'removecoupon'){
                //show edit button
                $_parent.children('.coupon-input').hide();
                $_parent.children('.remove-coupon').show();
                $_parent.children('.edit-coupon').show();
                $_parent.children('.flip-button').show();
                $_parent.children('.flip-button').children().children('span').text(couponCode);
            }

            if(result.status && _action == 'removecoupon'){
                //show edit button
                $_parent.children('.coupon-input').show();
                $_parent.children('.coupon-input').val('');
                $_parent.children('.edit-coupon').hide();
                $_parent.children('.set-coupon').show();
                $_parent.children('.update-coupon').hide();
                $_parent.children('.flip-button').hide();
            }
        });
    }
}

$(".coupon-block .edit-coupon").click(function(event) {
    $parent = $('.action-'+$(this).attr('parent-c'));
    $parent.children('.coupon-input').show();
    $parent.children('.update-coupon').show();
    $parent.children('.flip-button').hide();
    $(this).hide();

});

$(".coupon-block .flip-button").click(function(){
    var copyText = $(this).children('.code').val();
    var dealerLink = $(this).children('.dealer-link').val();
    ClipboardHelper.copyText(copyText);

    //show notification
    $.growl.notice({ message: "Coupon Code Copied!" });

    //open coupon code
    $(this).hide();
    $(this).next('.coupon-shown').show();

    setTimeout(function(){
        //get current link
        var win = window.open(dealerLink, '_blank');

        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
    }, 1000);
});

//post review action
$('#post-review').click(function(){
        //clear error
        $('.rating-error').hide();

        //post review
        wsPostReview();
  });
});


var wsSelectRating = function(){
    $('.stars a').click(function(){
        $('.stars a').removeClass('active');
        $(this).addClass('active');

        //remove error text
        $('.rating-error.rating-star').hide();
    });
}

var wsPostReview = function(){
    var starSelect = parseInt($('.stars a.active').html());
    var comment = $('#comment').val();
    var author = $('#author').val();
    var email = $('#email').val();

    if(isNaN(starSelect)){
        $('.rating-error.rating-star').show();
        return;
    }

    if(comment == ''){
        $('.rating-error.rating-comment').show();
        return;
    }

    if(author == ''){
        //alert('Type your name please');
        //return;
    }

    if(email == ''){
        $('.rating-error.rating-email').show();
        return;
    }

    //append to form data
    var formData = new FormData();
    $(".review-photo").find('input[type=file]').each(function(index, file){
        if(file.files[0] != undefined){
            formData.append('file[]', file.files[0]);
        }
    });
    formData.append('uploadfile', 'uploadfile');

    $.ajax({
        url: ws_plugin_url+"includes/reviews.php", //submit to contest submit action
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            if(res.status){
                var photo = '';
                if(res.el == 'success'){
                    photo = res.fileuploaded[0];
                }

                var data = {
                        'action': 'ws_add_review',
                        'watchId': GlobalWatchId,
                        'rate': starSelect,
                        'comment': comment.replace(/\n\r?/g, '<br />'),
                        'username': author,
                        'useremail': email,
                        'userip': '',
                        'photo': photo
                    };

                $.post(ws_plugin_url+"includes/reviews.php", data, function(response) {
                    var result = JSON.parse(response);

                    if(result.status){
                        console.log(result.data);

                        //clear the form
                        $('.stars a').removeClass('active');
                        $('#comment').val('');
                        $('#author').val('');
                        $('#email').val('');

                        $('.review-photo img').attr('src', $('.review-photo img').attr('no-photo')).width(50);
                        $('#review-photo').val('');

                        //success alert
                        $('.thankyou-content h4').text('Thank you for your review!');
                        $('#alert-price-success-modal').modal('show');

                    }else{
                        if(result.el == 'email'){
                            $('.rating-error.rating-email').show();
                        }
                    }
                });
            }else{
                $('.rating-error.rating-photo').text(res.message);
                $('.rating-error.rating-photo').show();
            }
        }
    });
}

//new upload
var readURL = function(input){
    $('.rating-error.rating-photo').hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //console.log(e.target.result);
            $('.review-photo img').attr('src', e.target.result)
                .width(90);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showPopupChart(){
    drawChart('mb_curve_chart');
}

var ClipboardHelper = {

    copyElement: function ($element)
    {
        this.copyText($element.text())
    },
    copyText:function(text) // Linebreaks with \n
    {
        var $tempInput =  $("<textarea>");
        $("body").append($tempInput);
        $tempInput.val(text).select();
        document.execCommand("copy");
        $tempInput.remove();
    }
};