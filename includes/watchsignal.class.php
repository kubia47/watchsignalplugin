<?php

if ( ! class_exists( 'WATCHSIGNALS' ) ) :

/**
 * Main plugin class
 *
 * @class WATCHSIGNALS
 */
class WATCHSIGNALS {
	protected $hostname 	= "35.202.46.142"; //mysql 35.240.224.175
	protected $username 	= "roots"; //mysql evowatchuser
	protected $password 	= "1010"; //mysql Sjum29RNRKPf3he
	protected $db					= "watchsignals"; //mysql watchsignals
	protected $port				= "5432";
	protected $result;
	protected $connection;
	protected $flagConnect;

	public function __construct() {
			$connection = pg_connect("host=$this->hostname port=$this->port dbname=$this->db user=$this->username password=$this->password");

			if (!$connection) {
    		die("Error in connection: " . pg_last_error());
     	} else {
				$this->connection = $connection;
				$this->flagConnect = true;
			}
	}

	public function base58_encode_url($input, $pro_name){
		//checksum input
	    $checksum = substr(array_sum(str_split($input)), -1);

	    $alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	    $base_count = strval(strlen($alphabet));
	    $encoded = '';
	    while (floatval($input) >= floatval($base_count))
	    {
	        $div = bcdiv($input, $base_count);
	        $mod = bcmod($input, $base_count);
	        $encoded = substr($alphabet, intval($mod), 1) . $encoded;
	        $input = $div;
	    }
	    if (floatval($input) > 0)
	    {
	        $encoded = substr($alphabet, intval($input), 1) . $encoded;
	    }

	    $delimiter = '_';
	    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $pro_name))))), $delimiter));

	    return ($slug.'_'.$encoded.$checksum);
	}

	public function base58_decode($input){
		$last_digit = substr($input, -1);
		$input = substr($input, 0, -1);

	    $alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	    $base_count = strval(strlen($alphabet));
	    $decoded = strval(0);
	    $multi = strval(1);
	    while (strlen($input) > 0)
	    {
	        $digit = substr($input, strlen($input) - 1);
	        $decoded = bcadd($decoded, bcmul($multi, strval(strpos($alphabet, $digit))));
	        $multi = bcmul($multi, $base_count);
	        $input = substr($input, 0, strlen($input) - 1);
	    }

	    //check if checksum is right
	    //echo $decoded;
	    $checksum = substr(array_sum(str_split($decoded)), -1);

	    if($checksum == $last_digit){
	    	return($decoded);
	    }else{
	    	return false;
	    }
	}

	public function getPriceRange($product_id){
        $watchProposals = $this->getDynamicProposalById($product_id);
        if(count($watchProposals) > 0){
            $arrProposals = [];
            foreach($watchProposals as $key =>$watchProposal){
                //$countOffers++;
                if($watchProposal['Currency'] != 'USD'){
                    $watchProposal['Price'] = $this->currencyExchange($watchProposal['Price'], $watchProposal['Currency']);
                    $watchProposal['Currency'] = 'USD';
                }

                //group by websource
                if($watchProposal['EndDate'] == null){
                    array_push($arrProposals, $watchProposal);
                }
            }

            //start sort by price
            usort($arrProposals, function ($item1, $item2) {
                return $item1['Price'] > $item2['Price'];
            });

            $lowest_price = $arrProposals[0]['Price'];
            $highest_price = $arrProposals[count($arrProposals) - 1]['Price'];
            $price_currency = 'USD';
            return $price_range = ($lowest_price != $highest_price) ? $this->showProductPrice($lowest_price, $price_currency).' - '.$this->showProductPrice($highest_price, $price_currency) : 'From '.$this->showProductPrice($lowest_price, $price_currency);

        }else{
            return false;
        }
    }

	public function scoring($score){
        $score_arr = [];

        switch(true){
            case $score > 1.25:
                $score_arr['class'] = 'bad one-bar';
                $score_arr['label'] = 'Expensive Offer';
                break;
            case $score > 1.1:
                $score_arr['class'] = 'bad two-bars';
                $score_arr['label'] = 'Average Offer';
                break;
            case $score > 0.85:
                $score_arr['class'] = 'ok three-bars';
                $score_arr['label'] = 'Fair Offer';
                break;
            case $score > 0.67:
                $score_arr['class'] = 'good four-bars';
                $score_arr['label'] = 'Good Offer';
                break;
            case $score > 0.44:
                $score_arr['class'] = 'good five-bars';
                $score_arr['label'] = 'Awesome Offer';
                break;
            default :
                $score_arr['class'] = 'bad none';
                $score_arr['label'] = 'Score unavailable';
                break;
        }

        return $score_arr;
    }

    public function currencyExchange($price, $currency){
        //convert all to USD
        //get exchange rate
        $access_key = '6c54bd5aeea93c373d2a373caf7e67c0';
        $url = 'http://apilayer.net/api/live?access_key='.$access_key;

        $result = file_get_contents($url);
        $result = json_decode($result);

        $exchangeRate = 1;
        $exchangeValue = $price;
        if ($result->success) {
            $fromto = 'USD'.$currency;
            $exchangeRate = $result->quotes->$fromto;
            $exchangeValue = $price / $exchangeRate;
        }
        return $exchangeValue;
    }

    public function showProductPrice($price, $currency){
        setlocale(LC_MONETARY,"en_US");
        return money_format('%.2n',$price);
    }

	public function getLocationFromDB(){
		$sql="SELECT distinct Location FROM main.dynamicproposals
			WHERE Location is not null
			order by Location";
		$locationItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$tmp=explode(",",$row["Location"]);
			$locationItems[] = $tmp[0];
		}
		return array_unique($locationItems);
	}

	public function filterWatchByGender($gender,$limit=10,$offset=0){
		$re_string=explode("/",$gender);
		if(count($re_string)==2){
			$sql='SELECT * FROM main.staticmodels WHERE `Gender` REGEXP "[[:<:]]'.$re_string[0].'[[:>:]]|[[:<:]]'.$re_string[1].'[[:>:]]" limit '.$limit.' offset '.$offset;
		}else{
			$sql='SELECT * FROM main.staticmodels WHERE `Gender` REGEXP "[[:<:]]'.$gender.'[[:>:]]" limit '.$limit.' offset '.$offset;
		}
		//echo $sql;
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function filterWatchByLocation($location,$limit=10,$offset=0){
		$sql='SELECT * FROM staticmodelsChrono24 WHERE `Location` REGEXP "[[:<:]]'.$location.'[[:>:]]" limit '.$limit.' offset '.$offset;
		//echo $sql;
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function filterWatchesByCondition($conditionArray,$limit=10,$offset=0){
		$re_string=explode("/",$conditionArray);
		$combinedTbl="(SELECT main.staticmodels.*,main.dynamicproposals.id as DynamicID,dBrand.name as BrandName,main.dynamicproposals.Description,main.dynamicproposals.Conditions,main.dynamicproposals.startdate,main.dynamicproposals.EndDate,main.dynamicproposals.price
FROM main.staticmodels,main.dynamicproposals,dBrand
WHERE main.staticmodels.id=main.dynamicproposals.staticid AND main.staticmodels.brandid=dBrand.id order by main.staticmodels.id) as combinedTbl";

		$sql='SELECT * FROM '.$combinedTbl.' WHERE combinedTbl.Conditions REGEXP "[[:<:]]'.$re_string[0].'[[:>:]]|[[:<:]]'.$re_string[1].'[[:>:]]" limit '.$limit.' offset '.$offset;
		//echo $sql;
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function filterWatchesByBrand($brand,$limit=10,$offset=0){
		$sql="SELECT main.staticmodels.*,dBrand.name as BrandName FROM main.staticmodels,dBrand where main.staticmodels.brandid=dBrand.id AND BrandID=$brand limit $limit offset $offset";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getFeaturedProduct($brandID, $limit){
		$staticidList = "(SELECT staticid
				FROM main.dynamicproposals
				WHERE staticid is not null
				GROUP BY staticid
				HAVING COUNT(Distinct StartDate) > 1 and COUNT(distinct Price) > 1)";

		$sql="SELECT main.staticmodels.*, pricerange.minprice, pricerange.maxprice FROM main.staticmodels
			LEFT JOIN main.prices as pricerange ON main.staticmodels.id=pricerange.staticid
			WHERE pricerange.minprice >= 1000 AND main.staticmodels.brandid=$brandID AND main.staticmodels.id in $staticidList
			ORDER BY main.staticmodels.id DESC
			LIMIT $limit";

		//echo $sql."<hr>";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getWatchWithMaxPrice($watchname){
		$staticidList = "(SELECT staticid
				FROM main.dynamicproposals
				WHERE staticid is not null
				GROUP BY staticid
				HAVING COUNT(Distinct StartDate) > 1 and COUNT(distinct Price) > 1)";

		$sql="SELECT main.staticmodels.*, pricerange.minprice, pricerange.maxprice FROM main.staticmodels
			LEFT JOIN main.prices as pricerange ON main.staticmodels.id=pricerange.staticid
			WHERE pricerange.minprice >= 1000 AND main.staticmodels.watchname ILIKE '%$watchname%' AND main.staticmodels.id in $staticidList
			ORDER BY pricerange.maxprice DESC
			LIMIT 1 OFFSET 0";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getBrandCategories($re_string,$option="all"){
		if($option=="showSuggestion"){
			$re_string=str_replace(" ","%",$re_string);
			$staticidList = "(SELECT staticid
				FROM main.dynamicproposals
				WHERE staticid is not null
				GROUP BY staticid
				HAVING COUNT(Distinct StartDate) > 1 and COUNT(distinct Price) > 1)";

			$combinedTbl="(SELECT distinct main.staticmodels.id FROM main.dynamicproposals, main.staticmodels WHERE main.dynamicproposals.staticid=main.staticmodels.id)";

			$sql="SELECT dim.brand.*,
				(SELECT COUNT(*) FROM (SELECT * FROM main.staticmodels as st WHERE (st.watchname ILIKE '%$re_string%' OR st.watchmodel ILIKE '%$re_string%' OR st.referencenumber ILIKE '%$re_string%') AND st.id in $staticidList AND st.id in $combinedTbl) as st WHERE st.brandid = dim.brand.id) AS Total
				FROM dim.brand";
		}else if($option=="adsearch"){
			$sql="SELECT dim.brand.*, (SELECT COUNT(*) FROM main.staticmodels WHERE main.staticmodels.brandid = dim.brand.id) AS Total FROM dim.brand";
		}else if($option=="all"){
			$sql="SELECT dim.brand.*, (SELECT COUNT(*) FROM main.staticmodels WHERE main.staticmodels.brandid = dim.brand.id) AS Total FROM dim.brand";
		}
		echo "<div style='display:none'>";
		echo $sql;
		echo "</div>";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getWebSource(){
		$sql="SELECT dim.websource.*,
			(SELECT COUNT(distinct dy.staticid ) FROM main.dynamicproposals as dy WHERE dy.websourceid = dim.websource.id AND dy.EndDate is null) AS Total
			FROM dim.websource";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

    public function getBrand(){
        $sql="SELECT * FROM dim.brand";
        $watchItems = array();
        $result = $this->execute($sql);
        while($row = pg_fetch_assoc($result)){
            $watchItems[] = $row;
        }
        return $watchItems;
    }

    public function getModels(){
        $sql="SELECT WatchModel FROM main.staticmodels GROUP BY WatchModel";
        $watchItems = array();
        $result = $this->execute($sql);
        while($row = pg_fetch_assoc($result)){
            $watchItems[] = $row;
        }
        return $watchItems;
    }

    public function getModelsByBrand($brandId){
        $sql="SELECT WatchModel FROM main.staticmodels WHERE BrandID=".$brandId." GROUP BY WatchModel";
        $watchItems = array();
        $result = $this->execute($sql);
        if($result){
            while($row = pg_fetch_assoc($result)){
                $watchItems[] = $row;
            }
            return $watchItems;
        }else{
            return false;
        }
    }

    public function getGenders(){
        $sql="SELECT * FROM dim.gender";
        $watchItems = array();
        $result = $this->execute($sql);
        while($row = pg_fetch_assoc($result)){
            $watchItems[] = $row;
        }
        return $watchItems;
    }

	public function getMovements(){
		$sql="SELECT * FROM dim.movement";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getConditions(){
		$sql="SELECT * FROM dim.WatchCondition";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getMaterials(){
		$sql="SELECT * FROM dim.material";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getMinPrice(){
		$sql="SELECT Price FROM (SELECT main.staticmodels.*,main.dynamicproposals.id as DynamicID,dBrand.name as BrandName,main.dynamicproposals.Description,main.dynamicproposals.Conditions,main.dynamicproposals.startdate,main.dynamicproposals.EndDate,main.dynamicproposals.price
		FROM main.staticmodels,main.dynamicproposals,dBrand
		WHERE main.staticmodels.id=main.dynamicproposals.staticid AND main.staticmodels.brandid=dBrand.id AND main.dynamicproposals.EndDate is null order by main.staticmodels.id) as combinedTbl
		WHERE Price is not null
		ORDER BY Price
		LIMIT 1
		offset 0";

		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getMaxPrice(){
		$sql="SELECT Price FROM (SELECT main.staticmodels.*,main.dynamicproposals.id as DynamicID,dBrand.name as BrandName,main.dynamicproposals.Description,main.dynamicproposals.Conditions,main.dynamicproposals.startdate,main.dynamicproposals.EndDate,main.dynamicproposals.price
		FROM main.staticmodels,main.dynamicproposals,dBrand
		WHERE main.staticmodels.id=main.dynamicproposals.staticid AND main.staticmodels.brandid=dBrand.id AND main.dynamicproposals.EndDate is null order by main.staticmodels.id) as combinedTbl
		WHERE Price is not null
		ORDER BY Price desc
		LIMIT 1
		offset 0";

		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getBraceletMaterial(){
		$sql="SELECT distinct BraceletMaterial FROM staticmodelsChrono24 where BraceletMaterial != '' order by BraceletMaterial";
		$watchItems = array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function advancedSearch($vars=array(),$sortBy,$limit=10,$offset=0){
		$isFirstQuery=true;
		$onlyBrandUsed=true;
		$onlyCaseMaterialUsed=true;

		if($vars["manufactures"]=="all"){
			$brandQuery="";
		}else{
			$isFirstQuery=false;
			$onlyCaseMaterialUsed=false;
			$brandQuery="NewTbl12.brandid in (";
			foreach($vars["manufactures"] as $brand){
				$brandQuery.=$brand.",";
			}
			$brandQuery=substr($brandQuery, 0, -1);
			$brandQuery.=")";
		}

		if($vars["movements"]=="all"){
			$movementQuery="";
		}else{
			if($isFirstQuery==true)
				$movementQuery="(";
			else
				$movementQuery="AND (";

			$isFirstQuery=false;
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			foreach($vars["movements"] as $k=>$movement){
				if($k<count($vars["movements"])-1)
					$movementQuery.="NewTbl12.movementid=$movement OR ";
				else
					$movementQuery.="NewTbl12.movementid=$movement";
			}
			$movementQuery.=")";
		}

		if($vars["caseMaterials"]=="all")
			$caseMaterialQuery="";
		else{
			if($isFirstQuery==true)
				$caseMaterialQuery="(";
			else
				$caseMaterialQuery="AND (";
			$onlyBrandUsed=false;
			$isFirstQuery=false;
			foreach($vars["caseMaterials"] as $k=>$caseMaterial){
				if($k<count($vars["caseMaterials"])-1)
					$caseMaterialQuery.="NewTbl12.casematerialid=$caseMaterial OR ";
				else
					$caseMaterialQuery.="NewTbl12.casematerialid=$caseMaterial";
			}
			$caseMaterialQuery.=")";
		}

		if($vars["gender"]=="all")
			$genderQuery="";
		else{
			if($isFirstQuery==true)
				$genderQuery="NewTbl12.GenderName ILIKE '%".$vars["gender"]."%'";
			else
				$genderQuery="AND NewTbl12.GenderName ILIKE '%".$vars["gender"]."%'";
			$isFirstQuery=false;
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
		}

		if($vars["braceletMaterial"]=="all")
			$braceletMaterialQuery="";
		else{
			if($isFirstQuery==true)
				$braceletMaterialQuery="(";
			else
				$braceletMaterialQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["braceletMaterial"] as $k=>$braceletMaterial){
				if($k<count($vars["braceletMaterial"])-1)
					$braceletMaterialQuery.="NewTbl12.braceletmaterialid=$braceletMaterial OR ";
				else
					$braceletMaterialQuery.="NewTbl12.braceletmaterialid=$braceletMaterial";
			}
			$braceletMaterialQuery.=")";
		}

		if($vars["braceletColor"]=="all")
			$braceletColorQuery="";
		else{
			if($isFirstQuery==true)
				$braceletColorQuery="(";
			else
				$braceletColorQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["braceletColor"] as $k=>$braceletColor){
				if($k<count($vars["braceletColor"])-1)
					$braceletColorQuery.="NewTbl12.braceletcolorid=$braceletColor OR ";
				else
					$braceletColorQuery.="NewTbl12.braceletcolorid=$braceletColor";
			}
			$braceletColorQuery.=")";
		}

		if($vars["dialColors"]=="all")
			$dialColorsQuery="";
		else{
			if($isFirstQuery==true)
				$dialColorsQuery="(";
			else
				$dialColorsQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["dialColors"] as $k=>$dialColor){
				if($k<count($vars["dialColors"])-1)
					$dialColorsQuery.="NewTbl12.dialcolorid=$dialColor OR ";
				else
					$dialColorsQuery.="NewTbl12.dialcolorid=$dialColor";
			}
			$dialColorsQuery.=")";
		}

		if($vars["glass"]=="all")
			$glassQuery="";
		else{
			if($isFirstQuery==true)
				$glassQuery="(";
			else
				$glassQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["glass"] as $k=>$glass){
				if($k<count($vars["glass"])-1)
					$glassQuery.="NewTbl12.glassid=$glass OR ";
				else
					$glassQuery.="NewTbl12.glassid=$glass";
			}
			$glassQuery.=")";
		}

		if($vars["numeral"]=="all")
			$numeralQuery="";
		else{
			if($isFirstQuery==true)
				$numeralQuery="(";
			else
				$numeralQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["numeral"] as $k=>$numeral){
				if($k<count($vars["numeral"])-1)
					$numeralQuery.="NewTbl12.numeralid=$numeral OR ";
				else
					$numeralQuery.="NewTbl12.numeralid=$numeral";
			}
			$numeralQuery.=")";
		}

		if($vars["buckle"]=="all")
			$buckleQuery="";
		else{
			if($isFirstQuery==true)
				$buckleQuery="(";
			else
				$buckleQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["buckle"] as $k=>$buckle){
				if($k<count($vars["buckle"])-1)
					$buckleQuery.="NewTbl12.buckleid=$buckle OR ";
				else
					$buckleQuery.="NewTbl12.buckleid=$buckle";
			}
			$buckleQuery.=")";
		}

		if($vars["buckleMaterial"]=="all")
			$buckleMaterialQuery="";
		else{
			if($isFirstQuery==true)
				$buckleMaterialQuery="(";
			else
				$buckleMaterialQuery="AND (";
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
			$isFirstQuery=false;
			foreach($vars["buckleMaterial"] as $k=>$buckleMaterial){
				if($k<count($vars["buckleMaterial"])-1)
					$buckleMaterialQuery.="NewTbl12.bucklematerialid=$buckleMaterial OR ";
				else
					$buckleMaterialQuery.="NewTbl12.bucklematerialid=$buckleMaterial";
			}
			$buckleMaterialQuery.=")";
		}

		if($vars["priceFrom"]==0 && $vars["priceTo"]==0)
			$priceQuery="";
		else{
			if($isFirstQuery==true)
				$priceQuery="NewTbl12.minprice >= ".$vars["priceFrom"]." AND NewTbl12.minprice <= ".$vars["priceTo"];
			else
				$priceQuery="AND NewTbl12.minprice >= ".$vars["priceFrom"]." AND NewTbl12.minprice <= ".$vars["priceTo"];
			$isFirstQuery=false;
			$onlyBrandUsed=false;
			$onlyCaseMaterialUsed=false;
		}

		if($vars["conditions"]!="all"){
			$conditionFilter="Select distinct d.staticid from main.dynamicproposals as d
				 inner join (select ID from main.staticmodels as st) as Temp
				On Temp.id=d.staticid
				 Where";
			foreach($vars["conditions"] as $i=>$condition){
				if($i==0)
					$conditionFilter.=" d.WatchConditionID=$condition";
				else
					$conditionFilter.=" OR d.WatchConditionID=$condition";
			}
		}

		if($sortBy=="Relevance"){
			$order_by="";
		}else if($sortBy=="Newest"){
			$order_by="ORDER BY NewTbl12.id desc";
		}else if($sortBy=="PriceLowToHigh"){
			$order_by="ORDER BY NewTbl12.minprice";
		}else{
			$order_by="ORDER BY NewTbl12.minprice desc";
		}

		$re_string=str_replace(" ","%",$vars["re_string"]);

		$from="SELECT st.* FROM main.staticmodels as st
			WHERE (st.watchname ILIKE '%$re_string%' OR st.watchmodel ILIKE '%$re_string%' OR st.referencenumber ILIKE '%$re_string%')";

		if($vars["conditions"]!="all"){
			$from.=" AND st.id in ($conditionFilter)";
		}

		$select="from (select NewTbl11.*, co1.name as DialColorName
		from (select NewTbl10.*, movement.name as MovementName
		from (select NewTbl9.*, pricerange.minprice, pricerange.maxprice
		from (select NewTbl8.*,num.name as NumeralName
		from (select NewTbl7.*,co.name as BraceletColorName
		from (select NewTbl6.*,brma.name as BraceletMaterialName
		from (select NewTbl5.*,ma.name as CaseMaterialName
		from (select NewTbl4.*,ge.name as GenderName
		from (select NewTbl3.*,bk.name as BuckleName
		from (select NewTbl2.*,gl.name as GlassName
		from (select NewTbl1.*,br.name as BrandName
		from ($from) as NewTbl1 LEFT JOIN dim.brand as br
		ON NewTbl1.brandid=br.id) as NewTbl2 LEFT JOIN dim.glass as gl
		ON NewTbl2.glassid=gl.id) as NewTbl3 LEFT JOIN dim.buckle as bk
		ON NewTbl3.buckleid=bk.id) as NewTbl4 LEFT JOIN dim.gender as ge
		ON NewTbl4.genderid=ge.id) as NewTbl5 LEFT JOIN dim.material as ma
		ON NewTbl5.casematerialid=ma.id) as NewTbl6 LEFT JOIN dim.material as brma
		ON NewTbl6.braceletmaterialid=brma.id) as NewTbl7 LEFT JOIN dim.color as co
		ON NewTbl7.braceletcolorid=co.id) as NewTbl8 LEFT JOIN dim.numeral as num
		ON NewTbl8.numeralid=num.id) as NewTbl9 INNER JOIN main.prices as pricerange
		ON NewTbl9.id=pricerange.staticid) as NewTbl10 LEFT JOIN dim.movement as movement
		ON NewTbl10.movementid=movement.id) as NewTbl11 LEFT JOIN dim.color as co1
		ON NewTbl11.dialcolorid=co1.id) as NewTbl12  LEFT JOIN dim.material as bucma
		ON NewTbl12.bucklematerialid=bucma.id";

		$sql="SELECT NewTbl12.*, bucma.name as BucketName $select WHERE $brandQuery $movementQuery $caseMaterialQuery $genderQuery $braceletMaterialQuery $braceletColorQuery $dialColorsQuery $glassQuery $numeralQuery $buckleQuery $buckleMaterialQuery $priceQuery $order_by limit $limit offset $offset";
		/*echo "<div style='display:none'>";
		echo $sql;
		echo "</div>";*/
		$watchItems = array("Response" => array());
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["Response"][] = $row;
		}

		//get min price
		$sql="SELECT NewTbl12.*, bucma.name as BucketName $select WHERE $brandQuery $movementQuery $caseMaterialQuery $genderQuery $braceletMaterialQuery $braceletColorQuery $dialColorsQuery $glassQuery $numeralQuery $buckleQuery $buckleMaterialQuery $priceQuery ORDER BY NewTbl12.minprice LIMIT 1 OFFSET 0";
		$result = $this->execute($sql);
		$row = pg_fetch_assoc($result);
		$watchItems["MinPrice"] = $row["minprice"];
		//get max price
		$sql="SELECT NewTbl12.*, bucma.name as BucketName $select WHERE $brandQuery $movementQuery $caseMaterialQuery $genderQuery $braceletMaterialQuery $braceletColorQuery $dialColorsQuery $glassQuery $numeralQuery $buckleQuery $buckleMaterialQuery $priceQuery ORDER BY NewTbl12.minprice DESC LIMIT 1 OFFSET 0";
		$result = $this->execute($sql);
		$row = pg_fetch_assoc($result);
		$watchItems["MaxPrice"] = $row["minprice"];

		$sql="select count(*) as total $select
		WHERE $brandQuery $movementQuery $caseMaterialQuery $genderQuery $braceletMaterialQuery $braceletColorQuery $dialColorsQuery $glassQuery $numeralQuery $buckleQuery $buckleMaterialQuery $priceQuery";
		$result = $this->execute($sql);
		$row = pg_fetch_assoc($result);
		$watchItems["TotalResults"] = $row["total"];

		//-------------------------------
		$fromCombinedForFilter="SELECT NewTbl12.*, bucma.name as BucketName $select WHERE $brandQuery $movementQuery $caseMaterialQuery $genderQuery $braceletMaterialQuery $braceletColorQuery $dialColorsQuery $glassQuery $numeralQuery $buckleQuery $buckleMaterialQuery $priceQuery $order_by";
		if($onlyBrandUsed==true){
			//get Brand Category
			$sql="select dim.brand.id, dim.brand.name,total
					from (select brandid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by brandid) as filtered inner join dim.brand on filtered.brandid=dim.brand.id";
		}else{
			//get Brand Category
			$sql="select * from (SELECT dim.brand.*,
				(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.brandid = dim.brand.id) AS Total
				FROM dim.brand
				GROUP BY dim.brand.id) as filtered where total > 0";
		}
		$watchItems["brandArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["brandArray"][] = $row;
		}
		//get Movement Category
		$sql="select * from (SELECT dim.movement.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.movementid = dim.movement.id) AS total
			FROM dim.movement
			GROUP BY dim.movement.id) as filtered where total > 0";
		$watchItems["movementArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["movementArray"][] = $row;
		}
		//get Gender Category
		$sql="select * from (SELECT dim.gender.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.genderid = dim.gender.id) AS Total
			FROM dim.gender
			GROUP BY dim.gender.id) as filtered where total > 0";
		$watchItems["genderArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["genderArray"][] = $row;
		}

		if ($onlyCaseMaterialUsed==true) {
			//get Case Material Category
			$sql="select dim.material.id, dim.material.name,total
					from (select casematerialid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by casematerialid) as filtered inner join dim.material on filtered.casematerialid=dim.material.id";
		} else {
			//get Case Material Category
			$sql="select * from (SELECT dim.material.*,
				(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.casematerialid = dim.material.id) AS Total
				FROM dim.material
				GROUP BY dim.material.id) as filtered where total > 0";
		}

		$watchItems["caseMaterialArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["caseMaterialArray"][] = $row;
		}
		//get Bracelet Material Category
		$sql="select * from (SELECT dim.material.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.braceletmaterialid = dim.material.id) AS Total
			FROM dim.material
			GROUP BY dim.material.id) as filtered where total > 0";
		$watchItems["braceletMaterialArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["braceletMaterialArray"][] = $row;
		}
		//get Buckle Material Category
		$sql="select * from (SELECT dim.material.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.bucklematerialid = dim.material.id) AS Total
			FROM dim.material
			GROUP BY dim.material.id) as filtered where total > 0";
		$watchItems["buckleMaterialArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["buckleMaterialArray"][] = $row;
		}
		//get Glass Category
		$sql="select * from (SELECT dim.glass.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.glassid = dim.glass.id) AS Total
			FROM dim.glass
			GROUP BY dim.glass.id) as filtered where total > 0";
		$watchItems["glassArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["glassArray"][] = $row;
		}
		//get DialColor Category
		$sql="select * from (SELECT dim.color.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.dialcolorid = dim.color.id) AS Total
			FROM dim.color
			GROUP BY dim.color.id) as filtered where total > 0";
		$watchItems["dialColorArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["dialColorArray"][] = $row;
		}
		//get BraceletColorID Category
		$sql="select * from (SELECT dim.color.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.braceletcolorid = dim.color.id) AS Total
			FROM dim.color
			GROUP BY dim.color.id) as filtered where total > 0";
		$watchItems["braceletColorArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["braceletColorArray"][] = $row;
		}
		//get Numeral Category
		$sql="select * from (SELECT dim.numeral.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.numeralid = dim.numeral.id) AS Total
			FROM dim.numeral
			GROUP BY dim.numeral.id) as filtered where total > 0";
		$watchItems["numeralArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["numeralArray"][] = $row;
		}
		//get Buckle Category
		$sql="select * from (SELECT dim.buckle.*,
			(SELECT COUNT(*) FROM ($fromCombinedForFilter) as AAA WHERE AAA.buckleid = dim.buckle.id) AS Total
			FROM dim.buckle
			GROUP BY dim.buckle.id) as filtered where total > 0";
		$watchItems["buckleArray"]=array();
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["buckleArray"][] = $row;
		}
		return $watchItems;
	}

	public function showSuggestion($re_string,$page,$sortBy="all",$limit=10,$offset=0){
		if($sortBy=="Relevance"){
			$order_by="";
		}else if($sortBy=="Newest"){
			$order_by="ORDER BY NewTbl10.id desc";
		}else if($sortBy=="PriceLowToHigh"){
			$order_by="ORDER BY NewTbl10.minprice";
		}else{
			$order_by="ORDER BY NewTbl10.minprice desc";
		}

		$re_string=str_replace(" ","%",$re_string);

		$from="SELECT st.* FROM main.staticmodels as st
				WHERE (st.watchname ILIKE '%$re_string%' OR st.watchmodel ILIKE '%$re_string%' OR st.referencenumber ILIKE '%$re_string%')";

		$select="NewTbl10.*, movement.name as MovementName
		from (select NewTbl9.*, pricerange.minprice, pricerange.maxprice
		from (select NewTbl8.*,num.name as NumeralName
		from (select NewTbl7.*,co.name as BraceletColorName
		from (select NewTbl6.*,brma.name as BraceletMaterialName
		from (select NewTbl5.*,ma.name as CaseMaterialName
		from (select NewTbl4.*,ge.name as GenderName
		from (select NewTbl3.*,bk.name as BuckleName
		from (select NewTbl2.*,gl.name as GlassName
		from (select NewTbl1.*,br.name as BrandName
		from ($from) as NewTbl1 LEFT JOIN dim.brand as br
		ON NewTbl1.brandid=br.id) as NewTbl2 LEFT JOIN dim.glass as gl
		ON NewTbl2.glassid=gl.id) as NewTbl3 LEFT JOIN dim.buckle as bk
		ON NewTbl3.buckleid=bk.id) as NewTbl4 LEFT JOIN dim.gender as ge
		ON NewTbl4.genderid=ge.id) as NewTbl5 LEFT JOIN dim.material as ma
		ON NewTbl5.casematerialid=ma.id) as NewTbl6 LEFT JOIN dim.material as brma
		ON NewTbl6.braceletmaterialid=brma.id) as NewTbl7 LEFT JOIN dim.color as co
		ON NewTbl7.braceletcolorid=co.id) as NewTbl8 LEFT JOIN dim.numeral as num
		ON NewTbl8.numeralid=num.id) as NewTbl9 INNER JOIN main.prices as pricerange
		ON NewTbl9.id=pricerange.staticid) as NewTbl10 LEFT JOIN dim.movement as movement
		ON NewTbl10.movementid=movement.id";

		$sql="SELECT $select $order_by LIMIT $limit OFFSET $offset";
		/* echo "<div style='display:none'>";
		echo $sql;
		echo "</div>"; */
		$watchItems = array("Response" => array());
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems["Response"][] = $row;
		}

		if($page == "search-page"){
			//get min price
			$sql="SELECT $select ORDER BY NewTbl10.minprice LIMIT 1 OFFSET 0";
			$result = $this->execute($sql);
			$row = pg_fetch_assoc($result);
			$watchItems["MinPrice"] = $row["minprice"];
			//get max price
			$sql="SELECT $select ORDER BY NewTbl10.minprice DESC LIMIT 1 OFFSET 0";
			$result = $this->execute($sql);
			$row = pg_fetch_assoc($result);
			$watchItems["MaxPrice"] = $row["minprice"];

			$sql="SELECT count(*) as Total FROM ($from) as st";
			$result = $this->execute($sql);
			$row = pg_fetch_assoc($result);
			$watchItems["TotalResults"] = $row["total"];

			//get Brand Category
			$sql="select dim.brand.id, dim.brand.name,total
					from (select brandid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by brandid) as filtered inner join dim.brand on filtered.brandid=dim.brand.id";
			$watchItems["brandArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["brandArray"][] = $row;
			}

			//get Movement Category
			$sql="select dim.movement.id, dim.movement.name,total
					from (select movementid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by movementid) as filtered inner join dim.movement on filtered.movementid=dim.movement.id";
			$watchItems["movementArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["movementArray"][] = $row;
			}
			//get Gender Category
			$sql="select dim.gender.id, dim.gender.name,total
					from (select genderid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by genderid) as filtered inner join dim.gender on filtered.genderid=dim.gender.id";
			$watchItems["genderArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["genderArray"][] = $row;
			}
			//get Case Material Category
			$sql="select dim.material.id, dim.material.name,total
					from (select casematerialid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by casematerialid) as filtered inner join dim.material on filtered.casematerialid=dim.material.id";
			$watchItems["caseMaterialArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["caseMaterialArray"][] = $row;
			}
			//get Bracelet Material Category
			$sql="select dim.material.id, dim.material.name,total
					from (select braceletmaterialid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by braceletmaterialid) as filtered inner join dim.material on filtered.braceletmaterialid=dim.material.id";
			$watchItems["braceletMaterialArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["braceletMaterialArray"][] = $row;
			}
			//get Glass Category
			$sql="select dim.glass.id, dim.glass.name,total
					from (select glassid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by glassid) as filtered inner join dim.glass on filtered.glassid=dim.glass.id";
			$watchItems["glassArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["glassArray"][] = $row;
			}
			//get DialColor Category
			$sql="select dim.color.id, dim.color.name, total
					from (select dialcolorid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by dialcolorid) as filtered inner join dim.color on filtered.dialcolorid=dim.color.id";
			$watchItems["dialColorArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["dialColorArray"][] = $row;
			}
			//get BraceletColorID Category
			$sql="select dim.color.id, dim.color.name, total
				from (select braceletcolorid, count(*) as total
				from (select dp.watchlink, st_req.*
				from main.dynamicproposals as dp
				inner join ($from) as st_req
				on dp.staticid=st_req.id) temp
				group by braceletcolorid) as filtered inner join dim.color on filtered.braceletcolorid=dim.color.id";
			$watchItems["braceletColorArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["braceletColorArray"][] = $row;
			}
			//get Numeral Category
			$sql="select dim.numeral.id, dim.numeral.name, total
					from (select numeralid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by numeralid) as filtered inner join dim.numeral on filtered.numeralid=dim.numeral.id";
			$watchItems["numeralArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["numeralArray"][] = $row;
			}

			//get Buckle Category
			$sql="select dim.buckle.id, dim.buckle.name, total
					from (select buckleid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by buckleid) as filtered inner join dim.buckle on filtered.buckleid=dim.buckle.id";
			$watchItems["buckleArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["buckleArray"][] = $row;
			}

			//get Buckle Material Category
			$sql="select dim.material.id, dim.material.name, total
					from (select bucklematerialid, count(*) as total
					from (select dp.watchlink, st_req.*
					from main.dynamicproposals as dp
					inner join ($from) as st_req
					on dp.staticid=st_req.id) temp
					group by bucklematerialid) as filtered inner join dim.material on filtered.bucklematerialid=dim.material.id";
			$watchItems["buckleMaterialArray"]=array();

			$result = $this->execute($sql);
			while($row = pg_fetch_assoc($result)){
				$watchItems["buckleMaterialArray"][] = $row;
			}
		}

		return $watchItems;
	}

	public function getTotalRecords(){
		$watchItems = array();
		$sql="SELECT count(*) as Total FROM main.staticmodels";
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getWatchById($id){
		$watchItems = array();
		$from="SELECT M.*, B.name as BrandName FROM main.staticmodels M, dim.brand B WHERE M.brandid = B.id AND M.id=".$id;
		$sql="select NewTbl8.*,num.name as NumeralName
		from (select NewTbl7.*,co.name as BraceletColorName
		from (select NewTbl6.*,brma.name as BraceletMaterialName
		from (select NewTbl5.*,ma.name as CaseMaterialName
		from (select NewTbl4.*,ge.name as GenderName
		from (select NewTbl3.*,bk.name as BuckleName
		from (select NewTbl2.*,gl.name as GlassName
		from (select NewTbl1.*,move.name as MovementName
		from ($from) as NewTbl1 LEFT JOIN dim.movement as move
		ON NewTbl1.movementid=move.id) as NewTbl2 LEFT JOIN dim.glass as gl
		ON NewTbl2.glassid=gl.id) as NewTbl3 LEFT JOIN dim.buckle as bk
		ON NewTbl3.buckleid=bk.id) as NewTbl4 LEFT JOIN dim.gender as ge
		ON NewTbl4.genderid=ge.id) as NewTbl5 LEFT JOIN dim.material as ma
		ON NewTbl5.casematerialid=ma.id) as NewTbl6 LEFT JOIN dim.material as brma
		ON NewTbl6.braceletmaterialid=brma.id) as NewTbl7 LEFT JOIN dim.color as co
		ON NewTbl7.braceletcolorid=co.id) as NewTbl8 LEFT JOIN dim.numeral as num
		ON NewTbl8.numeralid=num.id";
		/*echo "<div style='display:none'>";
		echo $sql;
		echo "</div>";*/
		$result = $this->execute($sql);
		if($result){
			while($row = pg_fetch_assoc($result)){
				$watchItems[] = $row;
			}
		}
		return $watchItems;
	}

	public function getCurrentLowestPrice($id){
        $sql = 'select D.price, D.currency from main.staticmodels S, main.dynamicproposals D where S.id = D.staticid AND S.id='.$id.' AND D.enddate IS NULL ORDER BY D.price ASC LIMIT 1';
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			return $row;
		}
		return false;
	}

	public function getDynamicProposalById($id){
		$watchItems = array();
		$sql = "select D.*, W.name as WebSource, W.Country as WebSourceCountryCode, P.IsVisa, P.IsMastercard, P.IsAmericanExpress, P.IsJCB, P.IsUnionPay, P.IsPaypal, P.IsAmazonPay, P.IsBankTransfer, P.IsBitcoin, S.brandid, S.watchmodel, S.genderid FROM main.dynamicproposals D, main.staticmodels as S, dim.websource as W, dim.websourcepaymentmethod as P WHERE D.staticid=$id AND D.staticid = S.id AND D.websourceid = W.id AND D.websourceid = P.websourceid ORDER BY W.name, D.price ASC";
		$result = $this->execute($sql);
		while($row = pg_fetch_assoc($result)){
			$watchItems[] = $row;
		}
		return $watchItems;
	}

	public function getDynamicProposalForHomePage(){
        $watchItems = array();
	    //$sql = 'SELECT * FROM main.dynamicproposals WHERE staticid=14080';
        $sql = 'select D.*, W.name as WebSource, P.IsVisa, P.IsMastercard, P.IsAmericanExpress, P.IsJCB, P.IsUnionPay, P.IsPaypal, P.IsAmazonPay, P.IsBankTransfer, P.IsBitcoin FROM main.dynamicproposals D, dim.websource W, dim.websourcepaymentmethod as P WHERE D.staticid=14080 AND D.websourceid = W.id AND D.websourceid = P.websourceid ORDER BY W.name, D.price ASC';
        $result = $this->execute($sql);
        while($row = pg_fetch_assoc($result)){
            $watchItems[] = $row;
        }
        return $watchItems;
    }

	public function getRelatedProduct($brandId, $model){
		$num_products = 8;
		$count = 0;
		$watchItems = array();

		$sql = 'select S.*, D.price, D.currency FROM main.staticmodels S, main.dynamicproposals D WHERE S.id = D.staticid AND S.brandid='.$brandId.' AND S.watchmodel="'.$model.'" LIMIT '.$num_products;
		$result = $this->execute($sql);
		if($result){
			while($row = pg_fetch_assoc($result)){
				$watchItems[] = $row;
				$count++;
			}
		}

		if($count < $num_products){
			$limit = $num_products - $count;
			$sql = 'select S.*, D.price, D.currency FROM main.staticmodels S, main.dynamicproposals D WHERE S.id = D.staticid AND S.brandid='.$brandId.' AND S.watchmodel='.$model.' LIMIT '.$limit;
			$result = $this->execute($sql);
			if($result){
				while($row = pg_fetch_assoc($result)){
					$watchItems[] = $row;
				}
			}
		}
		return $watchItems;
	}

	/*Reviews*/
	public function getWatchReviews($watchId){
		$sql = 'SELECT * FROM staticmodelsWatchReviews WHERE WatchId='.$watchId;
		$reviews = array();

		$result = $this->execute($sql);
		if($result){
			while($row = pg_fetch_assoc($result)){
				$reviews[] = $row;
			}
		}
		return $reviews;
	}

	public function getAvarageRating($watchId){
		$sql = 'SELECT AVG(NumRate) as AvarageRating FROM staticmodelsWatchReviews WHERE WatchID = '.$watchId;
		$result = $this->execute($sql);
		if($result){
			while($row = pg_fetch_assoc($result)){
				return $row['AvarageRating'];
			}
		}
	}

	public function postReview($watchId, $rate, $comment, $author='', $email='', $userip, $photo){
		$sql = 'INSERT INTO staticmodelsWatchReviews (WatchID, UserId, UserIp, UserName, UserEmail, NumRate, Comment, Photo) VALUES ('.$watchId.',NULL ,"'.$userip.'","'.$author.'","'.$email.'",'.$rate.',"'.$comment.'","'.$photo.'");';

		$result = $this->execute($sql);

		if($result){
			$review = array();

			$lastId = $this->connection->insert_id;

			$sql3 = 'SELECT * FROM staticmodelsWatchReviews WHERE Id='.$lastId;
			$result3 = $this->execute($sql3);
			if($result3){
				while($row = pg_fetch_assoc($result3)){
					$review['Review'] = $row;
				}
			}


			$review['AvarageRating'] = $this->getAvarageRating($watchId);
			return $review;
		}

	}

	/*Coupon*/
    public function getCouponDynamicSourceId($staticId, $websourceId){
        $sql = 'SELECT C.Code FROM staticmodelsSourceCoupon C WHERE staticid='.$staticId.' AND WebSourceID='.$websourceId;

        $result = $this->execute($sql);
        if($result){
            while($row = pg_fetch_assoc($result)){
                return $row['Code'];
            }
        }
    }

    public function getAllSourceCouponFilter(){
        $sql = 'SELECT S.Code, S.Model, W.name as "WebSourceName", B.name as "BrandName", G.name as "GenderName" FROM staticmodelsSourceCouponFilter S, dim.websource W, dim.brand B, dim.gender G WHERE S.WebSourceId=W.id AND S.BrandId = B.id AND S.GenderId = G.id';

        $items = array();

        $result = $this->execute($sql);
        if($result){
            while($row = pg_fetch_assoc($result)){
                $items[] = $row;
            }
        }
        return $items;
    }

    public function setSourceCouponFilter($source, $brand, $model, $gender, $code){
        //UPDATE staticmodelsSourceCoupon SET  = `new_value' [WHERE condition];
        $sql = 'INSERT INTO staticmodelsSourceCouponFilter (WebSourceId, BrandId, Model, GenderId, Code) VALUES ('.$source.','.$brand.',"'.$model.'",'.$gender.',"'.$code.'");';

        $result = $this->execute($sql);

        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function setCouponDynamicSourceId($staticId, $websourceId, $code){
        //UPDATE staticmodelsSourceCoupon SET  = `new_value' [WHERE condition];
        $sql = 'INSERT INTO staticmodelsSourceCoupon (staticid, WebSourceID, Code) VALUES ('.$staticId.','.$websourceId.',"'.$code.'");';

        $result = $this->execute($sql);

        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function updateCouponDynamicSourceId($staticId, $websourceId, $code){
        //UPDATE staticmodelsSourceCoupon SET  = `new_value' [WHERE condition];
        $sql = 'UPDATE staticmodelsSourceCoupon SET Code="'.$code.'" WHERE staticid='.$staticId.' AND WebSourceID='.$websourceId;
        //echo $sql;
        $result = $this->execute($sql);

        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function removeCouponDynamicSourceId($staticId, $websourceId){
        //UPDATE staticmodelsSourceCoupon SET  = `new_value' [WHERE condition];
        $sql = 'DELETE FROM staticmodelsSourceCoupon WHERE staticid='.$staticId.' AND WebSourceID='.$websourceId;
        //echo $sql;
        $result = $this->execute($sql);

        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function getSourceCoupons($websourceId){
        $sql = 'SELECT * FROM staticmodelsSourceCouponFilter WHERE WebSourceId='.$websourceId;

        $items = array();

        $result = $this->execute($sql);
        if($result){
            while($row = pg_fetch_assoc($result)){
                $items[] = $row;
            }
        }
        return $items;
    }

	public function execute($sql){
		if ($this->flagConnect == true) {
			$this->result = pg_query($sql);
			return $this->result;
		}
	}

	function __destruct() {

		if ($this->flagConnect == true) {
			@pg_free_result($this->result);
			@pg_close ( $this->connection );
		}

	}
}

endif;
?>
