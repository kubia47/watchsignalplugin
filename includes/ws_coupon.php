<?php include_once('watchsignal.class.php');  $watchSignalObject = new WATCHSIGNALS();?>

<h1>Watch Signals Coupon</h1>

<label>List Sources</label>
<select id="list-sources">
    <?php
        //get list source from db
        $websources = $watchSignalObject->getWebSource();
        foreach($websources as $k => $websource){
            echo '<option value="'.$websource['ID'].'">'.$websource['Name'].'</option>';
        }
    ?>
</select><br/>

<label>Brands</label>
<select id="list-brands">
    <option value="0">All Brands</option>
    <?php
    //get list source from db
    $brands = $watchSignalObject->getBrand();
    foreach($brands as $k => $brand){
        echo '<option value="'.$brand['ID'].'">'.$brand['Name'].'</option>';
    }
    ?>
</select><br/>

<label>Models</label>
<select id="list-models">
    <option value="0">All Models</option>
    <?php
    //get list source from db
    /*$models = $watchSignalObject->getModels();
    foreach($models as $k => $model){
        if($model['WatchModel'] != ''){
            echo '<option value="'.$model['WatchModel'].'">'.$model['WatchModel'].'</option>';
        }
    }*/
    ?>
</select><br/>

<label>Genders</label>
<select id="list-genders">
    <option value="0">All Genders</option>
    <?php
    //get list source from db
    $genders = $watchSignalObject->getGenders();
    foreach($genders as $k => $gender){
        echo '<option value="'.$gender['ID'].'">'.$gender['Name'].'</option>';
    }
    ?>
</select><br/>

<label>Coupon Code</label>
<input id="coupon-code" type="text" value="" placeholder=""/>

<button id="add-coupon">Add Coupon</button>

<br/><br/>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Web Source</th>
        <th>Brand</th>
        <th>Model</th>
        <th>Gender</th>
        <th>Code</th>
    </tr>
    </thead>
    <tbody>
        <?php
            $source_coupons = $watchSignalObject->getAllSourceCouponFilter();

            if($source_coupons){
                foreach($source_coupons as $k => $source_coupon): //print_r($source_coupon);
                    ?>
                        <tr>
                            <td><?php echo $k+1; ?></td>
                            <td><?php echo $source_coupon['WebSourceName']; ?></td>
                            <td><?php echo $source_coupon['BrandName']; ?></td>
                            <td><?php echo ($source_coupon['Model'] == '0') ? 'All Models' : $source_coupon['Model']; ?></td>
                            <td><?php echo $source_coupon['GenderName']; ?></td>
                            <td><?php echo $source_coupon['Code']; ?></td>
                        </tr>
                    <?php
                endforeach;
            }
        ?>
    </tbody>
</table>

<script>
    var ws_plugin_url = '<?php echo get_site_url().'/wp-content/plugins/watchsignalsplugin/'  ; ?>';
    /*Brand select change*/
    jQuery('#list-brands').change(function(){
        var _this = jQuery(this);
        var select_brand = _this.val();
        if(select_brand != 0){
            //get all models of this brand
            //do ajax
            var data = {
                'action': 'GET_BRAND_MODELS',
                'brandId': select_brand
            }
            jQuery.post(ws_plugin_url + "includes/coupon-ajax.php", data, function(response) {
                var res = JSON.parse(response);
                if(res.status){
                    console.log(res.data);
                    for(var i = 0; i < res.data.length; i++){
                        if(res.data[i].WatchModel != ''){
                            jQuery('#list-models').append('<option value="'+res.data[i].WatchModel+'">'+res.data[i].WatchModel+'</option>');
                        }
                    }
                }else{
                    jQuery('#list-models').html('<option value="0">All Models</option>');
                }
            });
        }
    });

    jQuery('#add-coupon').click(function(){
        console.log('Add Coupon');

        var coupon_code = jQuery('#coupon-code').val();
        if(coupon_code != ''){
            var _source = jQuery('#list-sources').val();
            var _brand = jQuery('#list-brands').val();
            var _model = jQuery('#list-models').val();
            var _gender = jQuery('#list-genders').val();

            //send to ajax
            var data = {
                'action': 'ADD_SOURCE_COUPON',
                'Source': _source,
                'Brand': _brand,
                'Model': _model,
                'Gender': _gender,
                'Code': coupon_code
            }
            jQuery.post(ws_plugin_url + "includes/coupon-ajax.php", data, function(response) {
                var res = JSON.parse(response);
                window.location.reload();
            });
        }
    });
</script>