<?php
	/**/
	$params = $_POST;

	if(isset($params['action'])){
		if($params['action'] == 'alertprice'){
			if($params['email']=='' || !filter_var($params['email'], FILTER_VALIDATE_EMAIL)){
                echo json_encode(array('status'=>false, 'el'=>'name', 'message'=>"Please enter your valid email"));
                return;
            }

            if($params['dprice']==''){
                echo json_encode(array('status'=>false, 'el'=>'name', 'message'=>"Please enter your desired price"));
                return;
            }

            //start send infos to mailchimp using api
            // MailChimp API credentials
	        $apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';
	        $listID = 'f11ce874c7';

	        //
	        $email = $params['email'];
	        $watchname = $params['watchname'];
	        $oprice = $params['oprice'];
	        $dprice = $params['dprice'];
	        $currency = $params['currency'];

	        // MailChimp API URL
	        $memberID = md5(strtolower($email));
	        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
	        
	        // member information
	        $json = json_encode([
	            'email_address' => $email,
	            'status'        => 'subscribed',
	            'merge_fields'  => [
	                'WNAME'     => $watchname,
	                'OPRICE'     => $oprice,
	                'DPRICE'     => $dprice,
	                'CUR'		=> $currency
	            ]
	        ]);

	        // send a HTTP POST request with curl
	        $ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	        $result = curl_exec($ch);
	        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	        curl_close($ch);

	        // store the status message based on response code
	        if ($httpCode == 200) {
	            echo json_encode(array('status'=>true, 'message'=>"Send alert price successfully"));
	        }else{
	        	echo json_encode(array('status'=>false, 'message'=>"Send alert price failed"));
	        }

	        //if newsletter subscribed is checked
	        if(isset($params['newsletter'])){
	        	ws_subscribe_newsletter($email);
	        }

	        return;
		}

		if($params['action'] == 'subscribe'){
			$email = $params['sub-email'];
			if($email=='' || !filter_var($email, FILTER_VALIDATE_EMAIL)){
                echo json_encode(array('status'=>false, 'el'=>'name', 'message'=>"Please enter your valid email"));
                return;
            }

            if(!check_email_subscribed($email,'30de7cc15b')){
            	$status = ws_subscribe_newsletter($email);

            	if($status == 200){
	            	echo json_encode(array('status'=>true, 'message'=>"Send subscribe email successfully"));
	            }else{
	            	echo json_encode(array('status'=>false, 'message'=>"Send alert price failed"));
	            }
            }else{
            	echo json_encode(array('status'=>false, 'message'=>"You are already subscribed."));
            }
		}
		
		if($params['action'] == 'partnerNetwork'){
			if($params['partnerName']=='' || !filter_var($params['partnerName'], FILTER_VALIDATE_EMAIL)){
                echo json_encode(array('status'=>false, 'el'=>'partnerName', 'message'=>"Please enter your valid email"));
                return;
            }

            if($params['partnerWebsite']=='' || !filter_var($params['partnerWebsite'], FILTER_VALIDATE_URL)){
                echo json_encode(array('status'=>false, 'el'=>'partnerWebsite', 'message'=>"Please enter your website url"));
                return;
            }
			
			$email = $params['partnerName'];
			$website = $params['partnerWebsite'];
			$yourName = isset($params['partnerYName']) ? $params['partnerYName'] : "";
			$yourBName = isset($params['partnerBName']) ? $params['partnerBName'] : "";
			$yourIAm = isset($params['partnerIAm']) ? $params['partnerIAm'] : "";
			$yourIAmOther = isset($params['partnerIAmOther']) ? $params['partnerIAmOther'] : "";
			$yourWin = isset($params['partnerWin']) ? $params['partnerWin'] : "";
			$yourWinOther = isset($params['partnerWinOther']) ? $params['partnerWinOther'] : "";
			
			if($yourIAm == "Other" && $yourIAmOther == ""){
				echo json_encode(array('status'=>false, 'el'=>'partnerIAm', 'message'=>"Please enter other field"));
                return;
			}
			
			if($yourWin == 5 && $yourWinOther == ""){
				echo json_encode(array('status'=>false, 'el'=>'partnerWin', 'message'=>"Please enter other field"));
                return;
			}
			
			switch ($yourWin) {
				case 1:
					$yourWin="I have inventory of watches and want to be featured on Watchsignals search results";
					break;
				case 2:
					$yourWin="I have affiliate program/referral program for Watchsignals to promote my products";
					break;
				case 3:
					$yourWin="I want to place my advertisements on Watchsignals";
					break;
				case 4:
					$yourWin="I want real-time market reports, market intelligence for luxury watch market";
					break;
				case 5:
					$yourWin=$yourWinOther;
					break;
			}
			
			switch ($yourIAm) {
				case "WatchDealer":
					$yourIAm="Watch dealer/Independent watch shop";
					break;
				case "Marketplace":
					$yourIAm="Marketplace/Portal";
					break;
				case "WatchAuthorizationService":
					$yourIAm="Watch authorization service";
					break;
				case "Other":
					$yourIAm=$yourIAmOther;
					break;
			}
			
			$listID = '79deb282f3'; 
			
			if(!check_email_subscribed($email,$listID)){
				//check_website_subcribed($website);
				//start send infos to mailchimp using api
				// MailChimp API credentials
				$apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';

				// MailChimp API URL
				$memberID = md5(strtolower($email));
				$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
				$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

				// member information
				$json = json_encode([
					'email_address' => $email,
					'status'        => 'subscribed',
					'merge_fields'  => [						
						'WEBSITE'     => $website,
						'YNAME'	=> $yourName,
						'YBNAME'	=> $yourBName,
						'IAMA'	=> $yourIAm,
						'WINWIN'	=> $yourWin
					]
				]);

				// send a HTTP POST request with curl
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				$result = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

				// store the status message based on response code
				if ($httpCode == 200) {
					echo json_encode(array('status'=>true, 'message'=>"Send message successfully"));
				}else{
					echo json_encode(array('status'=>false, 'message'=>"Send message failed"));
				}
			}else{
            	echo json_encode(array('status'=>false, 'el'=>'partnerName','message'=>"You are already joined."));
            }            
		}

        if($params['action'] == 'contact'){
            if($params['email']=='' || !filter_var($params['email'], FILTER_VALIDATE_EMAIL)){
                echo json_encode(array('status'=>false, 'el'=>'email', 'message'=>"Please enter your valid email"));
                return;
            }

            if($params['message']==''){
                echo json_encode(array('status'=>false, 'el'=>'message', 'message'=>"Please enter your message"));
                return;
            }

            //start send infos to mailchimp using api
            // MailChimp API credentials
            $apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';
            $listID = '066a415c58';

            //
            $email = $params['email'];
            $message = $params['message'];
            $name = $params['name'];
            $phone = $params['phone'];

            // MailChimp API URL
            $memberID = md5(strtolower($email));
            $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

            // member information
            $json = json_encode([
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => [
                    'FNAME'     => $name,
                    'PHONE'     => $phone,
                    'MESSAGES'     => $message
                ]
            ]);

            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // store the status message based on response code
            if ($httpCode == 200) {
                echo json_encode(array('status'=>true, 'message'=>"Send message successfully"));
            }else{
                echo json_encode(array('status'=>false, 'message'=>"Send message failed"));
            }

            //send message to our email here


            return;
        }
	}
	
	function ws_subscribe_newsletter($email){
		$apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';
	    $listID = '30de7cc15b';

	    // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        
        // member information
        $json = json_encode([
            'email_address' => $email,
            'status'        => 'subscribed'
        ]);

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $httpCode;
	}
	
	function check_website_subcribed($website){
		$apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';
	    $listID = '79deb282f3';

	    // MailChimp API URL        
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        //$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/'.$listID.'/merge-fields/5';
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/search-members?query='.$website.'&list_id='.$listID;

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $res = json_decode($result);
        echo "<pre>";
		print_r($res);
        echo "</pre>";
		if($res->status == 404){
        	return false;
        }else{
        	return true;
        }
	}

	function check_email_subscribed($email,$listID){
		$apiKey = '7ed6236eaa9504634a3b71a57d3ee1ae-us7';
	    //$listID = '30de7cc15b';

	    // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $res = json_decode($result);
        

        if($res->status == 404){
        	return false;
        }else{
        	return true;
        }
	}
?>