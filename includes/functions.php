<?php
if(!function_exists("money_format")){
	function money_format($format, $number)
	{
		$regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
				  '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
		if (setlocale(LC_MONETARY, 0) == 'C') {
			setlocale(LC_MONETARY, '');
		}
		$locale = localeconv();
		preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
		foreach ($matches as $fmatch) {
			$value = floatval($number);
			$flags = array(
				'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
							   $match[1] : ' ',
				'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
				'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
							   $match[0] : '+',
				'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
				'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
			);
			$width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
			$left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
			$right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
			$conversion = $fmatch[5];

			$positive = true;
			if ($value < 0) {
				$positive = false;
				$value  *= -1;
			}
			$letter = $positive ? 'p' : 'n';

			$prefix = $suffix = $cprefix = $csuffix = $signal = '';

			$signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
			switch (true) {
				case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
					$prefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
					$suffix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
					$cprefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
					$csuffix = $signal;
					break;
				case $flags['usesignal'] == '(':
				case $locale["{$letter}_sign_posn"] == 0:
					$prefix = '(';
					$suffix = ')';
					break;
			}
			if (!$flags['nosimbol']) {
				$currency = $cprefix .
							($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
							$csuffix;
			} else {
				$currency = '';
			}
			$space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

			$value = number_format($value, $right, $locale['mon_decimal_point'],
					 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
			$value = @explode($locale['mon_decimal_point'], $value);

			$n = strlen($prefix) + strlen($currency) + strlen($value[0]);
			if ($left > 0 && $left > $n) {
				$value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
			}
			$value = implode($locale['mon_decimal_point'], $value);
			if ($locale["{$letter}_cs_precedes"]) {
				$value = $prefix . $currency . $space . $value . $suffix;
			} else {
				$value = $prefix . $value . $space . $currency . $suffix;
			}
			if ($width > 0) {
				$value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
						 STR_PAD_RIGHT : STR_PAD_LEFT);
			}

			$format = str_replace($fmatch[0], $value, $format);
		}
		return $format;
	}
}

if(!function_exists("watchsignals_dealer_network")){
	function watchsignals_dealer_network(){
		global $WS_CLASS;
		wp_enqueue_style( 'ws-dealer-network-css', WATCHSIGNALS_URL . 'assets/css/dealerNetwork.css',array(),'1.1','all');
		//show data by templates
		require_once( WATCHSIGNALS_DIR . 'includes/templates/dealerNetwork.php' );
	}
	//add stylesheet
	add_shortcode ("WATCHSIGNALS_DEALERNETWORK", 'watchsignals_dealer_network');
}
/*
* Show data from GCP
*/
if(!function_exists("watchsignals_show_mainpage")){
	function watchsignals_show_mainpage(){
		global $WS_CLASS;
		global $WS_CACHE;
		/*$cacheDuration=86400;
		$rolex_featured_idcache="Rolex-Featured-Homepage";
		$audemarsPiguet_featured_idcache="AudemarsPiguet-Featured-Homepage";
		$patek_featured_idcache="Patek-Featured-Homepage";
		$omega_featured_idcache="Omega-Featured-Homepage";
		$breitling_featured_idcache="Breitling-Featured-Homepage";
		$iwc_featured_idcache="Iwc-Featured-Homepage";
		$Panerai_featured_idcache="Panerai-Featured-Homepage";
		//get product from brands
		$WS_CACHE->setCache($rolex_featured_idcache);
		if($WS_CACHE->isCached($rolex_featured_idcache)){
			$rolex=$WS_CACHE->retrieve($rolex_featured_idcache);
		}else{
			$rolex=$WS_CLASS->getFeaturedProduct(382,3);
			$WS_CACHE->store($rolex_featured_idcache,$rolex,$cacheDuration);
		}
		$WS_CACHE->setCache($audemarsPiguet_featured_idcache);
		if($WS_CACHE->isCached($audemarsPiguet_featured_idcache)){
			$audemarsPiguet=$WS_CACHE->retrieve($audemarsPiguet_featured_idcache);
		}else{
			$audemarsPiguet=$WS_CLASS->getFeaturedProduct(31,3);
			$WS_CACHE->store($audemarsPiguet_featured_idcache,$audemarsPiguet,$cacheDuration);
		}
		$WS_CACHE->setCache($patek_featured_idcache);
		if($WS_CACHE->isCached($patek_featured_idcache)){
			$patek=$WS_CACHE->retrieve($patek_featured_idcache);
		}else{
			$patek=$WS_CLASS->getFeaturedProduct(342,3);
			$WS_CACHE->store($patek_featured_idcache,$patek,$cacheDuration);
		}
		$WS_CACHE->setCache($omega_featured_idcache);
		if($WS_CACHE->isCached($omega_featured_idcache)){
			$omega=$WS_CACHE->retrieve($omega_featured_idcache);
		}else{
			$omega=$WS_CLASS->getFeaturedProduct(334,3);
			$WS_CACHE->store($omega_featured_idcache,$omega,$cacheDuration);
		}
		$WS_CACHE->setCache($breitling_featured_idcache);
		if($WS_CACHE->isCached($breitling_featured_idcache)){
			$breitling=$WS_CACHE->retrieve($breitling_featured_idcache);
		}else{
			$breitling=$WS_CLASS->getFeaturedProduct(62,3);
			$WS_CACHE->store($breitling_featured_idcache,$breitling,$cacheDuration);
		}
		$WS_CACHE->setCache($iwc_featured_idcache);
		if($WS_CACHE->isCached($iwc_featured_idcache)){
			$iwc=$WS_CACHE->retrieve($iwc_featured_idcache);
		}else{
			$iwc=$WS_CLASS->getFeaturedProduct(226,3);
			$WS_CACHE->store($iwc_featured_idcache,$iwc,$cacheDuration);
		}
		$WS_CACHE->setCache($Panerai_featured_idcache);
		if($WS_CACHE->isCached($Panerai_featured_idcache)){
			$Panerai=$WS_CACHE->retrieve($Panerai_featured_idcache);
		}else{
			$Panerai=$WS_CLASS->getFeaturedProduct(340,3);
			$WS_CACHE->store($Panerai_featured_idcache,$Panerai,$cacheDuration);
		}

		$locations=$WS_CLASS->getLocationFromDB();
		$totalRecords=$WS_CLASS->getTotalRecords();

		$query_result=array_merge($rolex,$audemarsPiguet,$patek,$omega,$breitling,$iwc,$Panerai);
		*/
		//show data by templates
		require_once( WATCHSIGNALS_DIR . 'includes/templates/home.php' );

		wp_enqueue_script( 'ws-mainpage',
                       WATCHSIGNALS_URL.'assets/js/main.js',
                       '',
                       rand(1, 100),
                       true);
	}
	//add stylesheet
	add_shortcode ("WATCHSIGNALS_HOMEPAGE", 'watchsignals_show_mainpage');
}
function watchsignals_css_advancedsearch(){
	wp_enqueue_style( 'watchsignal-bootstrap-select', WATCHSIGNALS_URL . 'assets/css/bootstrap-select.min.css',array(),'1.1','all');
	wp_enqueue_script( 'ws-bootstrap-select',
                       WATCHSIGNALS_URL.'assets/js/bootstrap-select.min.js',
                       '',
                       rand(1, 100),
                       true);
}
if(!function_exists("watchsignals_show_advancedsearch")){
	function watchsignals_show_advancedsearch(){
		global $WS_CLASS;
		global $WS_COUNTRIES;
		$brandArray=$WS_CLASS->getBrandCategories();
		$ClaspArray=$WS_CLASS->getClaspMaterial();
		$BraceletArray=$WS_CLASS->getBraceletMaterial();
		watchsignals_css_advancedsearch();
		//show data by templates
		require_once( WATCHSIGNALS_DIR . 'includes/templates/advanced-search.php' );
	}
	add_shortcode ("WATCHSIGNALS_ADVANCEDSEARCH", 'watchsignals_show_advancedsearch');
}
if(!function_exists("watchsignals_show_productdetails")){
	function ws_getproduct_permalink($productId = ''){
		return WATCHSIGNALS_SITE_URL.'/product?id='.$productId;
	}

	function ws_css_productdetails(){
		wp_enqueue_script( 'ws-product-js',
                       WATCHSIGNALS_URL.'assets/js/product.js',
                       '',
                       rand(1, 100),
                       true);
	}

	function watchsignals_show_productdetails(){
		require_once( WATCHSIGNALS_DIR . 'includes/reviews.php' );
		ws_css_productdetails();
		//show data by templates
		require_once( WATCHSIGNALS_DIR . 'includes/templates/product-details.php' );
	}
	add_shortcode ("WATCHSIGNALS_PRODUCTDETAILS", 'watchsignals_show_productdetails');
}
if(!function_exists("watchsignals_show_compareproducts")){
	function watchsignals_show_compareproducts(){
		//show data by templates
		require_once( WATCHSIGNALS_DIR . 'includes/templates/compare-products.php' );
	}
	add_shortcode ("WATCHSIGNALS_COMPAREPRODUCTS", 'watchsignals_show_compareproducts');
}

function watchsignals_css_searchresults(){
	wp_enqueue_style( 'watchsignal_simplePagination', WATCHSIGNALS_URL . 'assets/css/simplePagination.css',array(),'1.1','all');
	wp_enqueue_style( 'jquery_multiselect', WATCHSIGNALS_URL . 'assets/css/jquery.multiselect.css',array(),'1.1','all');
	wp_enqueue_style( 'prism', WATCHSIGNALS_URL . 'assets/css/prism.css',array(),'1.1','all');
	wp_enqueue_style( 'jquery_range', WATCHSIGNALS_URL . 'assets/css/jquery.range.css',array(),'1.1','all');
	wp_enqueue_style( 'watchsignal_searchpage', WATCHSIGNALS_URL . 'assets/css/searchpage.css',array(),'1.1','all');
	wp_enqueue_script( 'ws-search-results',
                       WATCHSIGNALS_URL.'assets/js/search-results.js',
                       '',
                       rand(1, 100),
                       true);

}

if(!function_exists("watchsignals_show_searchresults")){
	function watchsignals_show_searchresults(){
		global $WS_CLASS;
		$view = WATCHSIGNALS_DIR . 'includes/templates/search-results.php';
		$searchable=true;
		if(!isset($_GET["query"]))
			$searchable=false;
		else{
			$items_per_page=10;
			$offset=0;
			$current_page=1;
			$re_string = trim(str_replace("+"," ",$_GET["query"]));

			if(isset($_GET["page-number"])){
				$current_page=$_GET["page-number"];
				if($current_page==1){
					$offset=0;
				}else{
					$offset=(($current_page-1)*$items_per_page)+1;
				}
			}

			$baseUrl = preg_replace('/&page-number=([0-9]*)/', '', basename($_SERVER['REQUEST_URI']));
			$vars=array();
			$vars["re_string"]=$re_string;
			$vars["manufactures"]=isset($_GET["manufactures"]) ? $_GET["manufactures"] : "all";
			$vars["movements"]=isset($_GET["movement"]) ? $_GET["movement"] : "all";
			$vars["caseMaterials"]=isset($_GET["caseMaterials"]) ? $_GET["caseMaterials"] : "all";
			$vars["gender"]=isset($_GET["gender"]) ? $_GET["gender"] : "all";
			$vars["braceletMaterial"]=isset($_GET["braceletMaterial"]) ? $_GET["braceletMaterial"] : "all";
			$vars["braceletColor"]=isset($_GET["braceletColor"]) ? $_GET["braceletColor"] : "all";
			$vars["priceFrom"]=isset($_GET["priceFrom"]) ? $_GET["priceFrom"] : 0;
			$vars["priceTo"]=isset($_GET["priceTo"]) ? $_GET["priceTo"] : 0;
			$sortBy=isset($_GET["sortBy"]) ? $_GET["sortBy"] : "all";
			$vars["conditions"]=isset($_GET["conditions"]) ? $_GET["conditions"] : "all";
			$vars["dealers"]=isset($_GET["dealers"]) ? $_GET["dealers"] : "all";
			$vars["dialColors"]=isset($_GET["dialColors"]) ? $_GET["dialColors"] : "all";
			$vars["glass"]=isset($_GET["glass"]) ? $_GET["glass"] : "all";
			$vars["numeral"]=isset($_GET["numeral"]) ? $_GET["numeral"] : "all";
			$vars["buckle"]=isset($_GET["buckle"]) ? $_GET["buckle"] : "all";
			$vars["buckleMaterial"]=isset($_GET["buckleMaterial"]) ? $_GET["buckleMaterial"] : "all";
			//$countryChosen=isset($_GET["country"]) ? $_GET["country"] : "all";
			//$scopeofdelivery=isset($_GET["scopeofdelivery"]) ? $_GET["scopeofdelivery"] : "all";
			$resultview=isset($_GET["resultview"]) ? $_GET["resultview"] : "grid";

			if(isset($_GET["adsearch"])){
				$query_result=$WS_CLASS->advancedSearch($vars,$sortBy,$items_per_page,$offset);
				$total_result=$query_result["TotalResults"];
			}else{
				$query_result=$WS_CLASS->showSuggestion($re_string,"search-page",$sortBy,$items_per_page,$offset);				
				$total_result=$query_result["TotalResults"];
			}
			$materialArray=$WS_CLASS->getMaterials();
			$brandArray=$query_result["brandArray"];
			$caseMaterialArray=$query_result["caseMaterialArray"];
			$braceletMaterialArray=$query_result["braceletMaterialArray"];
			$braceletColorArray=$query_result["braceletColorArray"];
			$movementArray=$query_result["movementArray"];
			$genderArray=$query_result["genderArray"];
			$dialColorArray=$query_result["dialColorArray"];
			$glassArray=$query_result["glassArray"];
			$numeralArray=$query_result["numeralArray"];
			$buckleArray=$query_result["buckleArray"];
			$buckleMaterialArray=$query_result["buckleMaterialArray"];

			$page_number=round($total_result/$items_per_page,0);
		}
		watchsignals_css_searchresults();
		//show data by templates
		require_once( $view );
	}
	add_shortcode ("WATCHSIGNALS_SEARCHRESULTS", 'watchsignals_show_searchresults');
}
//create page
if(!function_exists("watchsignals_create_page")){
	function watchsignals_create_page($title,$content,$slug,$template=""){
		// Create post object, you should add all pages first
		$my_post = array(
		  'post_title'    => wp_strip_all_tags( $title ),
		  'post_content'  => $content,
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'     => 'page',
		  'post_name'     => $slug,
		  'page_template'     => $template
		);
		// Insert the post into the database
		$newpageId = wp_insert_post( $my_post );
		return $newpageId;
	}
}
if(!function_exists("watchsignals_load_css")){
	function watchsignals_load_css() {
		wp_enqueue_style( 'watchsignal_slider', WATCHSIGNALS_URL . 'assets/css/mainpage.css',array(),'1.1','all');
	}
}
add_action( 'wp_print_styles', 'watchsignals_load_css', 2 );

?>
