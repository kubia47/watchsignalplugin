<?php
include_once("watchsignal.class.php");
$WS_CLASS = new WATCHSIGNALS();
$PROTOCOL=(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
$WATCHSIGNALS_NOPHOTO_URL = $PROTOCOL.'://watchsignals.com/wp-content/plugins/watch-signals/assets/img/no-photo.png';
if(isset($_POST["action"])){
	$action=$_POST["action"];
	if($action=="ws_ajax_search"){
		$re_string=$_POST["re_string"];
		$query_result=$WS_CLASS->showSuggestion($re_string,"search-input");
		$result=array();
		foreach($query_result["Response"] as $item){
			$value_encoding = $WS_CLASS->base58_encode_url($item["id"], $item["watchname"]);
			$result[] = array("value"=>$value_encoding,"label"=>$item['watchname']);
		}
	}else if($action=="update_counter"){
		$vars=array();
		$vars["re_string"]=$_POST["re_string"];
		$vars["manufactures"]=isset($_POST["manufactures"]) ? $_POST["manufactures"] : "all";
		$vars["movements"]=isset($_POST["movement"]) ? $_POST["movement"] : "all";
		$vars["caseMaterials"]=isset($_POST["caseMaterials"]) ? $_POST["caseMaterials"] : "all";
		$vars["gender"]=isset($_POST["gender"]) ? $_POST["gender"] : "all";
		$vars["braceletMaterial"]=isset($_POST["braceletMaterial"]) ? $_POST["braceletMaterial"] : "all";
		$vars["braceletColor"]=isset($_POST["braceletColor"]) ? $_POST["braceletColor"] : "all";
		$vars["dialColors"]=isset($_POST["dialColors"]) ? $_POST["dialColors"] : "all";
		$vars["glass"]=isset($_POST["glass"]) ? $_POST["glass"] : "all";
		$vars["priceFrom"]=isset($_POST["priceFrom"]) ? $_POST["priceFrom"] : 0;
		$vars["priceTo"]=isset($_POST["priceTo"]) ? $_POST["priceTo"] : 0;
		$vars["conditions"]=isset($_POST["conditions"]) ? $_POST["conditions"] : "all";
		$vars["dealers"]=isset($_POST["dealers"]) ? $_POST["dealers"] : "all";
		$vars["numeral"]=isset($_POST["numeral"]) ? $_POST["numeral"] : "all";
		$vars["buckle"]=isset($_POST["buckle"]) ? $_POST["buckle"] : "all";
		$vars["buckleMaterial"]=isset($_POST["buckleMaterial"]) ? $_POST["buckleMaterial"] : "all";

		if(isset($_POST["adsearch"])){
			$result=$WS_CLASS->advancedSearch($vars,"Relevance",1,0);
		}else{
			$result=$WS_CLASS->showSuggestion($vars["re_string"],"Relevance",1,0);
		}
	}

	echo json_encode($result);
}
?>
