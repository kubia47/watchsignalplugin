<?php
include_once('watchsignal.class.php');

	/**/
	$params = $_POST;

	if(isset($params['action'])){
		if($params['action'] == 'setcoupon'){
            $staticId = $params['staticId'];
            $websourceId = $params['websourceId'];
            $couponCode = $params['couponCode'];

            //save coupon
            $watchSignalObject = new WATCHSIGNALS();
            if($watchSignalObject->setCouponDynamicSourceId($staticId, $websourceId, $couponCode)){
                echo json_encode(array('status'=>true, 'message'=>'save coupon successfully!'));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
		}

        if($params['action'] == 'updatecoupon'){
            $staticId = $params['staticId'];
            $websourceId = $params['websourceId'];
            $couponCode = $params['couponCode'];

            //save coupon
            $watchSignalObject = new WATCHSIGNALS();
            if($watchSignalObject->updateCouponDynamicSourceId($staticId, $websourceId, $couponCode)){
                echo json_encode(array('status'=>true, 'message'=>'update coupon successfully!'));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
        }

        if($params['action'] == 'removecoupon'){
            $staticId = $params['staticId'];
            $websourceId = $params['websourceId'];

            //save coupon
            $watchSignalObject = new WATCHSIGNALS();
            if($watchSignalObject->removeCouponDynamicSourceId($staticId, $websourceId)){
                echo json_encode(array('status'=>true, 'message'=>'remove coupon successfully!'));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
        }

        if($params['action'] == 'GET_BRAND_MODELS'){
            $brandId = $params['brandId'];

            $watchSignalObject = new WATCHSIGNALS();
            $models = $watchSignalObject->getModelsByBrand($brandId);

            if($models){
                echo json_encode(array('status'=>true, 'message'=>'get models successfully!', 'data'=> $models));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
        }

        if($params['action'] == 'ADD_SOURCE_COUPON'){
            $Source = $params['Source'];
            $Brand = $params['Brand'];
            $Model = $params['Model'];
            $Gender = $params['Gender'];
            $Code = $params['Code'];

            //check if had coupon code
            $watchSignalObject = new WATCHSIGNALS();

            //add source coupon
            $save = $watchSignalObject->setSourceCouponFilter($Source, $Brand, $Model, $Gender, $Code);

            if($save){
                echo json_encode(array('status'=>true, 'message'=>'save source coupon filter successfully!'));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
        }

        if($params['action'] == 'GET_SOURCE_COUPON_FILTER'){
            //get source coupon filter
            $watchSignalObject = new WATCHSIGNALS();
            $source_coupons = $watchSignalObject->getAllSourceCouponFilter();

            if($source_coupons){
                echo json_encode(array('status'=>true, 'message'=>'get source coupon filter successfully!', 'data'=> $source_coupons));
            }else{
                echo json_encode(array('status'=>false, 'message'=>'error!'));
            }
        }
	}
?>