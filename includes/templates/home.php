<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/slick.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/slick-theme.css" type="text/css" media="all" />
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>

<div class="notification text-center">
    <span>15% SALES UNTIL END OF MARCH (HERE WILL GIVE THE PROMOTION OF WATCHSIGNAL</span>
    <a href="#" class="read-more">read more</a>
</div>
<div class="page-header">
    <div class="row">
        <div class="col-md-2">
            <div class="logo">
                <img src="<?php echo get_template_directory_uri(); ?>/images/img/logo.png" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="search-box">
                <input type="text" placeholder="Type the watch you want to compare here" />
                <div class="search-btn">
                    <input class="button" type="button" value="search" />
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="my-notification">
                <i class="fas fa-bell"></i> <span>My notification</span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="login">
                <i class="fas fa-user"></i> <span>Login</span>
            </div>
        </div>
    </div>
</div>
<div class="menu row">
    <div class="col-md-2"></div>
    <div class="nav-item col-md-10">
        <ul>
            <li><a href="#">nav1</a></li>
            <li><a href="#">nav2</a></li>
            <li><a href="#">nav3</a></li>
            <li><a href="#">nav4</a></li>
            <li><a href="#">nav5</a></li>
            <li><a href="#">nav6</a></li>
        </ul>
    </div>
</div>
<div class="slider">
    <div class="banner-slick">
        <div class="banner"><span>PROMOTION BANNER HEADLINE HERE THIS MAINLY IS FOR DEALERS AND BRANDS’S PROMO NEWS</span><img src="<?php echo get_template_directory_uri(); ?>/images/img/banner1.png" /></div>
        <div class="banner"><img src="<?php echo get_template_directory_uri(); ?>/images/img/banner1.png" /></div>
        <div class="banner"><img src="<?php echo get_template_directory_uri(); ?>/images/img/banner1.png" /></div>
    </div>
</div>

<div class="content">
    <section class="section section1">
        <div class="box shadow">
            <div class="row">
                <div class="col-md-3 list-home-btn">
                    <div class="home-btn">New arrivals</div>
                    <div class="home-btn">Last seen products</div>
                    <div class="home-btn">Watchsignal’s Deal</div>
                    <div class="home-btn">Marketplace</div>
                    <div class="home-btn">Trending now</div>
                    <div class="home-btn">Free shipping</div>
                </div>
                <div class="col-md-9">
                    <div class="list-product">
                        <h6 class="text-center">NEW ARRIVALS</h6>
                        <div class="list-brand">
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                            <div class="brand-btn">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/bell.svg" />
                                <br/>
                                <span>ROLEX BRAND</span>
                            </div>
                        </div>

                        <div class="content-banner">
                            <div class="banner">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/img/banner1.png" />
                                <span class="banner-label">BANNER FOR ROLEX</span>
                            </div>
                        </div>

                        <div class="product-items row">
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="product-item">
                                    <div class="bag">new</div>
                                    <div class="product-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch.svg" />
                                        <div class="comming-soon">
                                            <label>comming soon</label>
                                        </div>
                                    </div>
                                    <div class="product-info text-center">
                                        <span>exclusive</span>
                                        <h2 class="product-name">
                                            DATEJUST VER3.0 BY ROLEX
                                        </h2>
                                        <h3 class="des">
                                            Pre-owned, sponsored by <br/>
                                            [Dealer name], Germany
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="long-btn">
                        View & Compare all deals
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section popular-search">
        <div class="label text-center">
            POPULAR SEARCH TAGS
        </div>
        <div class="popular-search-slick">
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
            <div class="search-btn"><span>Rolex</span></div>
        </div>
        <div class="row">
            <div class="col-md-6 haft-banner">
                <img src="<?php echo get_template_directory_uri(); ?>/images/img/small-banner.png" />
                <div class="banner-des">
                    PROMO BANNER OF DEALERS WILL APPEAR HERE <br/>
                    SUCH AS: PROMO CODE <br/>
                    TIME: 5/4 - 12/6 <br/>
                    <a href="#">READ MORE</a>
                </div>
            </div>
            <div class="col-md-6 haft-banner">
                <img src="<?php echo get_template_directory_uri(); ?>/images/img/small-banner.png" />
                <div class="banner-des">
                    PROMO BANNER OF DEALERS WILL APPEAR HERE <br/>
                    SUCH AS: PROMO CODE <br/>
                    TIME: 5/4 - 12/6 <br/>
                    <a href="#">READ MORE</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section picks">
        <div class="label text-center">
            WATCHSIGNAL PICKS
        </div>
        <div class="box shadow">
            <div class="product-items slick-product-items">
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="slick-product-item">
                    <div class="product-item">
                        <div class="bag">top buy</div>
                        <div class="product-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                            <div class="comming-soon">
                                <label>comming soon</label>
                            </div>
                        </div>
                        <div class="product-info">
                            <span>exclusive</span>
                            <h2 class="product-name">
                                DATEJUST VER3.0 BY ROLEX
                            </h2>
                            <h3 class="des">
                                Pre-owned, sponsored by [Dealer name], Germany
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="long-btn">
                        <label>View & Compare all deals</label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section picks">
        <div class="label text-center">
            HOT PRICE - GOOD DEAL TODAY
        </div>
        <div class="box shadow">
            <div class="product-list-items product-items">
                <div class="slick-product-list-item">
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-product-list-item">
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-product-list-item">
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list-item product-item">
                        <div class="row">
                            <div class="product-img col-md-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon/watch2.svg" />
                                <div class="comming-soon">
                                    <label>comming soon</label>
                                </div>
                            </div>
                            <div class="product-info col-md-8">
                                <div class="row">
                                    <div class="price col-md-6">
                                        <label class="currency">$</label>
                                        <label class="num-price">234.000</label>
                                    </div>
                                    <div class="pro-status col-md-6">
                                        <div class="arrow-price">
                                            <i class="fas fa-caret-up"></i>
                                            <span class="up-down-price">$350</span>
                                        </div>
                                        <div class="num-bought">
                                            <span>12 people bought</span>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product-name">
                                    DATEJUST VER3.0 BY ROLEX
                                </h2>
                                <div class="dealer">
                                    [Dealer name], Germany
                                </div>
                                <div class="promotion-button">
                                    Click to see the Promo code
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="long-btn">
                        <label>View & Compare all deals</label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section subscribe">
        <div class="row">
            <div class="col-md-6">
                <div class="left"><img width="130" src="<?php echo get_template_directory_uri(); ?>/images/icon/email.svg" /></div>
                <div class="right">
                    <span class="title">Subscribe to get latest Watchsignal promotion, deals, price alert, and news</span><br/>
                    <span class="des">Do not miss our hot deals everyday. Signup with us now</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subscribe-box">
                    <input type="text" placeholder="Type your email here" />
                    <div class="subscribe-btn">
                        <input class="button" type="button" value="subscribe" />
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="our-partner">
                <h6>Our Partner</h6>
                <div class="partners">
                    <img height="47" src="<?php echo get_template_directory_uri(); ?>/images/home/ic_google_logo.png" />
                    <img height="30" src="<?php echo get_template_directory_uri(); ?>/images/home/ic_segment_logo.png" />
                </div>
                <h6>Payment Accepted</h6>
                <div class="payment-methods">
                    <img height="37" src="<?php echo get_template_directory_uri(); ?>/images/icon/paypal.png" />
                    <img height="25" src="<?php echo get_template_directory_uri(); ?>/images/icon/visa.png" />
                    <img height="37" src="<?php echo get_template_directory_uri(); ?>/images/icon/master.png" />
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    jQuery(document).ready(function() {
        $('.banner-slick').slick({
            dots: false,
            arrows: true,
            autoplay: false
        });

        $('.popular-search-slick').slick({
            dots: false,
            arrows: true,
            autoplay: false,
            slidesToShow: 6, // default desktop values
            slidesToScroll: 1
        });

        $('.slick-product-items').slick({
            dots: false,
            arrows: true,
            autoplay: false,
            slidesToShow: 7, // default desktop values
            slidesToScroll: 1
        });

        $('.product-list-items').slick({
            dots: false,
            arrows: true,
            autoplay: false,
            slidesToShow: 2, // default desktop values
            slidesToScroll: 2
        });
    });
</script>