<?php
foreach($query_result["Response"] as $item){
	$link=home_url().'/'.$WS_CLASS->base58_encode_url($item["id"], $item["watchname"]);
	$imgSource=WATCHSIGNALS_NOPHOTO_URL;
	$priceRange="From $".number_format($item["minprice"], 0, '.', ',');
?>
<div class="col-md-12 no-padding-leftright product-result">
	<div class="row itemgrid">
		<div class="col-md-3">
            <div class="button_action">
                <div class="ws_wishlist ws-wish-<?php echo $item['id']; ?> wishlist-btn" data-wishlist="no-wishlist" wish-id="<?php echo $item['id']; ?>">
                    <i class="far fa-heart"></i>
                </div>
            </div>
			<figure class="position-relative">
				<a href="<?php echo $link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/no-photo.png"></a>
			</figure>
		</div>
		<div class="col-md-9">
			<div class="cat_for_grid">
				<h3><a href="<?php echo $link; ?>" title="<?php echo $item["watchname"]; ?>"><?php echo $item["watchname"]; ?></a></h3>
				<div class="row">
					<div class="col-md-6 no-padding-leftright">
					<span class="text" title="<?php echo $item["brandname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Brand:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["brandname"]!=""?$item["brandname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["watchmodel"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Model:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["watchmodel"]?$item["watchmodel"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["referencenumber"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Reference Number:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["referencenumber"]?$item["referencenumber"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["glassname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Glass:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["glassname"]?$item["glassname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["bucklename"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Buckle:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["bucklename"]?$item["bucklename"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["powerreserve"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Power Reserve:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["powerreserve"]?$item["powerreserve"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["waterresistanceatm"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Water Resistance:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["waterresistanceatm"]?$item["waterresistanceatm"]:"-"; ?></div></div></span>
					</div>
					<div class="col-md-6 no-padding-leftright">
					<span class="text" title="<?php echo $item["gendername"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Gender:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["gendername"]?$item["gendername"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["casematerialname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Case Material:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["casematerialname"]?$item["casematerialname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["casediameter"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Case Diameter:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["casediameter"]?$item["casediameter"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["braceletmaterialname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Bracelet Material:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["braceletmaterialname"]?$item["braceletmaterialname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["braceletcolorname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Bracelet Color:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["braceletcolorname"]?$item["braceletcolorname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["numeralname"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Numeral:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["numeralname"]?$item["numeralname"]:"-"; ?></div></div></span>
					<span class="text" title="<?php echo $item["numberofjewels"]; ?>"><div class="row"><div class="col-md-6 item-list-mobile no-padding-leftright">Number Of Jewels:</div><div class="col-md-6 item-list-mobile text-ellipsis"><?php echo $item["numberofjewels"]?$item["numberofjewels"]:"-"; ?></div></div></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 no-padding-leftright"><h4><?php echo $priceRange; ?></h4></div>
					<div class="col-md-4 col-md-offset-4">
						<!--p class="text-muted">Dealer</p-->
						<!--p class="text-info"><?php //echo $item["WebSource"]; ?></p-->
						<span class="article-rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
