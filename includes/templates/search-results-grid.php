<?php
foreach($query_result["Response"] as $item){
	$link=home_url().'/'.$WS_CLASS->base58_encode_url($item["id"], $item["watchname"]);
	$imgSource=WATCHSIGNALS_NOPHOTO_URL;
	$priceRange="From $".number_format($item["minprice"], 0, '.', ',');
?>
<div class="col-md-3 no-padding-leftright product-result">
	<div class="item">
        <div class="button_action">
            <div class="ws_wishlist ws-wish-<?php echo $item['id']; ?> wishlist-btn" data-wishlist="no-wishlist" wish-id="<?php echo $item['id']; ?>">
                <i class="far fa-heart"></i>
            </div>
        </div>
		<figure class="position-relative">
			<a href="<?php echo $link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/no-photo.png"></a>
		</figure>
		<div class="cat_for_grid">
			<h5><a href="<?php echo $link; ?>" title="<?php echo $item["watchname"]; ?>"><?php echo $item["watchname"]; ?></a></h5>
			<h4><?php echo $priceRange; ?></h4>
		</div>
		<div class="article-dealer">
			<!--p class="text-muted">Dealer</p-->
			<!--p class="text-info"><?php //echo $item["WebSource"]; ?></p-->
			<span class="article-rating">
				<i class="fas fa-star"></i>
				<i class="fas fa-star"></i>
				<i class="fas fa-star"></i>
				<i class="fas fa-star"></i>
				<i class="fas fa-star"></i>
			</span>
		</div>
	</div>
</div>
<?php
}
?>
