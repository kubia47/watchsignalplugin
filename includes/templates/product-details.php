<?php
    global $WS_CLASS;

	$product_slug = get_query_var('product_slug');
	$product_flag = false;
	//echo $product_slug;
	if($product_slug){
		$explode_arr = explode('_', $product_slug);
		if(count($explode_arr) > 0){
			$product_id = $WS_CLASS->base58_decode($explode_arr[count($explode_arr) - 1]);
		}
	}

	//echo $encode_product = base58_encode_url(113403, "Omega Seamaster Planet Ocean Co-Axial White Dial 232.15 Men's SS Diamond Bezel Automatic Volume Watch Unused beauty item OMEGA box Empty Gala Used Ginza");
	//echo $decode_product = base64_decode($encode_product);
	//echo 'Product id: '.$product_id; echo $_GET['product_id'];
	//RewriteRule ^product/(.*)$ /product-details/?ref=$1 [L]
	function showProductPrice($price, $currency){
		if($price){
        	if($currency == 'USD'){
        		setlocale(LC_MONETARY,"en_US");
        		return money_format('%.2n',$price);
        	}else if($currency == 'EUR'){
        		setlocale(LC_MONETARY,"de_DE");
        		return money_format('€ %!n',$price);
        	}else{
        		return $price;
        	}
    	}else{
    		return 'Expired';
    	}
	}

	if($product_id){
		$watchId = $product_id;

		?>
		<script type="text/javascript">
			var GlobalWatchId = <?php echo $watchId; ?>;
		</script>
		<?php
		//get data from GCP via reference number
		$watchItems=$WS_CLASS->getWatchById($watchId);

		if(count($watchItems) > 0){
			$watchInfos = $watchItems[0];
			//print_r($watchInfos);

			$watchProposals = $WS_CLASS->getDynamicProposalById($watchInfos["id"]);

			if(count($watchProposals) > 0){
				$arrProposals = [];
				$arrWebsource = [];
				$arrByDate = [];
				foreach($watchProposals as $key =>$watchProposal){
					//exchange currency
                    if($watchProposal["currency"] != 'USD'){
                        $watchProposal["price"] = $WS_CLASS->currencyExchange($watchProposal["price"], $watchProposal["currency"]);
                        $watchProposal["currency"] = 'USD';
                    }

					//group by websource
					if($watchProposal["enddate"] == null){
                        $arrWebsource[$watchProposal['websource']][] = $watchProposal;
                        array_push($arrProposals, $watchProposal);
					}

					//group by date
					$arrByDate[$watchProposal['startdate']][] = $watchProposal;
				}

				//sort date array
				ksort($arrByDate);
				end($arrByDate);         // move the internal pointer to the end of the array
				$lastdate = key($arrByDate);

                // convert date and time to seconds
                $sec = strtotime($lastdate);

                // convert seconds into a specific format
                $lastDateFormat = date("d.m.Y", $sec);

				/*echo '<pre>';
				print_r($watchProposal);
				echo '</pre>';*/

			}


			//start sort by price
            usort($arrProposals, function ($item1, $item2) {
                return $item1["price"] > $item2["price"];
            });

			//get price range
            $lowest_price = $arrProposals[0]["price"];
            $highest_price = $arrProposals[count($arrProposals) - 1]["price"];
            $price_currency = 'USD';
            $price_range = ($lowest_price != $highest_price) ? showProductPrice($lowest_price, $price_currency).' - '.showProductPrice($highest_price, $price_currency) : 'From '.showProductPrice($lowest_price, $price_currency);

            $num_dealers = count($arrWebsource);
            $num_offers = count($arrProposals);
            /*echo '<pre>';
            print_r($arrProposals);
            echo '</pre>';*/


			//save product id to last seen array
			$product_flag = true;

            $reviews = []; //$WS_CLASS->getWatchReviews($watchId);
            $numReview = count($reviews);
		}else{
			echo 'Product Not Found!';
			exit(0);
		}
	}else{
		echo 'Product Not Found!';
		exit(0);
	}

	function lastSeen($product_id){
	    if(!isset($_SESSION['LASTSEEN'])){
	        $_SESSION['LASTSEEN'] = array($product_id);
	    }else{
	        $tmpSession = $_SESSION['LASTSEEN'];
			$tmpSession[] = $product_id;
			$tmpSession = array_unique($tmpSession);

			if(count($tmpSession) > 10){
			   $tmpSession = array_slice($tmpSession,1);
			}
			$_SESSION['LASTSEEN'] = $tmpSession;
	    }
	    return true;
	}
?>
<link rel="stylesheet" type="text/css" href="<?php echo WATCHSIGNALS_URL.'assets/css/productdetails.css'; ?>">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="product-page">
	<div class="row p-breakcrumb">
		<div class="col-md-12">
			<i class="fas fa-arrow-left"></i> <span class="p-sm-lable p-b p-gray">BACK TO STORE</span>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12"><h2 class="pro-name p-orange p-bold"><?php echo $watchInfos['watchname']; ?></h2></div>
    </div>
	<div class="row p-left-top">
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-6 p-l-0">
                    <div class="button_action">
                        <div class="ws_wishlist ws-wish-<?php echo $watchId; ?> wishlist-btn" data-wishlist="no-wishlist" wish-id="<?php echo $watchId; ?>">
                            <i class="far fa-heart"></i>
                        </div>
                    </div>
					<div class="watch-img">
                        <?php if($watchInfos['BrandName'] != 'Rolex'): ?>
                        <a href="<?php echo $lowest_price_watchlink; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/img_watch.png" /></a>
                        <?php else: ?>
                        <div>
                            <iframe height="360" width="340"
                                    src="https://sketchfab.com/models/c4bb0e55a227463a901243e63476c7bb/embed?annotations_visible=0&autospin=0&autostart=1&camera=0&ui_infos=0&ui_stop=1&max_texture_size=8192&transparent=1&ui_controls=0&ui_general_controls=0&ui_help=0&ui_inspector=0&ui_settings=0&ui_stop=0&ui_watermark_link=0&ui_watermark=0">
                            </iframe>
                        </div>
                        <?php endif; ?>
                    </div>
				</div>
				<div class="col-md-6 p-mobile-hide">
					<div><span class="p-nm-lable p-pink">Price range:</span></div>
					<div><span class="p-l-lable p-black p-b"><?php echo $price_range; ?></span></div>
					<div class="m-t-2"><span class="p-nm-lable p-pink">Offers: </span><span class="p-nm-lable p-gray"><?php echo $num_offers == 1 ? $num_offers.' offer' : $num_offers.' offers'; ?></span></div>
					<div class="m-t-2"><span class="p-nm-lable p-pink">Reviews: </span><span class="p-nm-lable p-gray"><?php echo $numReview; ?> review<?php echo ($numReview > 1) ? 's' : ''; ?></span></div>
					<div class="rh_woo_star" style="text-align: left;" title="Rated 5 out of 5">
						<span class="rhwoostar rhwoostar1 active">★</span><span class="rhwoostar rhwoostar2 active">★</span><span class="rhwoostar rhwoostar3 active">★</span><span class="rhwoostar rhwoostar4 active">★</span><span class="rhwoostar rhwoostar5 active">★</span>
						<a href="javascript:void(0);" class="p-nm-lable p-blue">Write a review</a>
					</div>
					<div class="m-t-2"><span class="p-nm-lable p-pink">Product data:</span></div>
					<div><span class="p-nm-lable p-gray">Brand: <?php echo $watchInfos['BrandName']; ?> </span><a href="javascript:void(0);" class="p-nm-lable p-blue">Read more</a></div>

                    <div class="m-t-2"><span class="p-nm-lable p-pink">Last update: </span><span class="p-nm-lable p-gray"><?php echo $lastDateFormat; ?></span></div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="p-price p-border-blue">
				<!--span class="ws-fawn-color p-mobile-hide">Current Price</span-->
				<div class="row">
					<div class="col-md-6 price text-center p-desktop-hide m-t-2"><span><?php if($lowestPriceObj){ echo showProductPrice($lowestPriceObj["price"], $lowestPriceObj["currency"]); }?></span>
						<div>
		       				<a class="price-range" href="javascript:void(0);"><?php echo $num_offers; ?> offers: <?php echo $price_range; ?></a>
		       			</div>
					</div>
					<div class="col-md-6 text-right">
						<div class="row text-center p-desktop-hide">
							<div class="col-mb-half" onclick="showPopupChart();" data-toggle="modal" data-target="#priceHistoryModal">
								<i class="fas fa-chart-line"></i><br/>
								<span>Price History</span>
							</div>
							<div class="col-mb-half" data-toggle="modal" data-target="#notify-modal">
								<i class="far fa-clock"></i><br/>
								<span>Price Alarm</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row price-his-menu p-mobile-hide">
					<div class="left menu-1 col-md-12 text-center">
						<span class="ws-fawn-color p-b p-orange">PRICE HISTORY</span><br/>
						<span class="p-sm-lable p-gray">Compare the price of watch</span>
					</div>
					<!--div class="left rail-select">
						<span class="ws-fawn-color">FROM</span> <input id="his-start-date" value="11-20-2018" />
						<i class="fas fa-angle-down ws-blue-color"></i>
					</div>
					<div class="left rail-select">
						<span class="ws-fawn-color">TO</span> <input id="his-end-date" value="11-30-2018"/>
						<i class="fas fa-angle-down ws-blue-color"></i>
					</div-->
					<div class="col-md-12 text-center m-t-2">
						<a href="javascript:void(0)" class="active">Week</a>
						<a href="javascript:void(0)">Month</a>
						<a href="javascript:void(0)">Year</a>
					</div>
				</div>
				<div class="row historical-chart p-mobile-hide">
                    <div id="curve_chart" style="width: 100%;"></div>
                    <script type="text/javascript">
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChartInit);

                        function drawChart(elementId) {
                            var data = google.visualization.arrayToDataTable([
                                ['Year', "price"],
                                <?php
                                foreach ($arrByDate as $key => $value) {
                                    # code...
                                    echo "['".$key."', ".$value[0]["price"]." ],";
                                }
                                ?>
                            ]);

                            var options = {
                                title: 'Price History',
                                curveType: 'function',
                                legend: { position: 'bottom' }
                            };

                            var chart = new google.visualization.LineChart(document.getElementById(elementId));

                            chart.draw(data, options);
                        }

                        function drawChartInit(){
                            drawChart('curve_chart');
                        }

                        /*$(window).resize(function(){
                            drawChart('curve_chart');
                        });*/
                    </script>
				</div>
			</div>
		</div>
	</div>
	<div class="p-content">
		<div class="panel-body">
            <ul class="nav nav-tabs">
              <li class="col-md-4 text-center active">
                <a href="#tabs-price" data-toggle="tab" class="active show">
                  Compare Price
                </a>
              </li>
              <li class="col-md-4 text-center">
                <a href="#tabs-description" data-toggle="tab">
                  Description
                </a>
              </li>
              <li class="col-md-4 text-center">
                <a href="#tabs-review" data-toggle="tab">
                  Write a review
                </a>
              </li>
            </ul>

            <div class="tab-content tab-content-bordered">
              <div class="tab-pane fade in active show" id="tabs-price">
                <div class="compare-price-list">
	        		<?php
                        $count_item = 0;
	        			if(count($arrProposals) > 0){
	        				foreach ($arrProposals as $key => $value) {
	        					//$value = $arrProposal[0];
	        					//foreach ($arrProposal as $k => $value) {
	        						# code...
                                    $count_item++;
                                    $score_arr = [];

                                    //echo $value['Ratio'];
                                    $score_arr = $WS_CLASS->scoring((double)$value['Ratio']);

                                    // convert date and time to seconds
                                    $sec = strtotime($value['startdate']);

                                    // convert seconds into a specific format
                                    $lastDateUpdate = date("d.m.Y", $sec);
	        					?>
                                <?php if($count_item == 1): ?>
                                    <div class="row col-md-12 compare-block-title p-mobile-hide"><span class="p-pink">Compare with <?php echo $num_dealers; ?> dealers</span></div>
                                <?php endif; ?>
	        					<div class="row compare-block p-mobile-hide" style="margin: 10px 0;" datalink="<?php echo $value["WatchLink"]; ?>">
		        					<div class="col-md-3 watch-name p-black" title="<?php echo $value["WatchName"]; ?>"><?php echo $value["WatchName"]; ?></div>
		        					<div class="col-md-2 watch-price"><b><?php echo showProductPrice($value["price"], $value["currency"]); ?></b><br/><span class="p-blue p-sm-lable">(Not incl. shipping)</span><br/><span class="p-ssm-lable"><?php echo 'Last update: '.$lastDateUpdate; ?></span></div>
		        					<div class="col-md-2 watch-source ws-blue-color"><div><span class="p-sm-lable p-gray" style="line-height: 20px;">Type of payment</span></div><div>
                                            <?php if($value['IsPaypal']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/paypal.png" /><?php endif; ?>
                                            <?php if($value['IsVisa']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/visa.png" /><?php endif; ?>
                                            <?php if($value['IsMastercard']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/mastercard.png" /><?php endif; ?>
                                            <?php if($value['IsAmericanExpress']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/american-express.png" /><?php endif; ?>
                                            <?php if($value['IsJCB']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/jcb.png" /><?php endif; ?>
                                            <?php if($value['IsUnionPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/western-union-pay-card.png" /><?php endif; ?>
                                            <?php if($value['IsAmazonPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/amazon-pay-card-logo.png" /><?php endif; ?>
                                            <?php if($value['IsBankTransfer']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bank-transfer.png" /><?php endif; ?>
                                            <?php if($value['IsBitcoin']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bitcoin.png" /><?php endif; ?>

                                        </div></div>

		        					<div class="col-md-2 watch-review">
		        						<div><?php echo $value["WebSource"]; ?></div>
                                        <?php if($value["WebSourceCountryCode"] != '' && $value["WebSourceCountryCode"] != 'World'): ?>
                                        <div><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $value["WebSourceCountryCode"]; ?>.png" /></div>
                                        <?php endif; ?>

                                        <?php if($value["WebSourceCountryCode"] == 'World'):
                                            //check location
                                            if($value["Location"] && $value['Location'] != ''){
                                                $locationCountry = explode(',', $value['Location']);
                                                if($locationCountry && count($locationCountry) > 0){
                                                    $locationCountry = $locationCountry[0];
                                                    ?>
                                                    <div><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/flag/<?php echo $locationCountry; ?>.png" /></div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php endif; ?>
		        					</div>
                                    <div class="col-md-2 text-center">
                                        <?php if($value['Ratio']): ?>
                                            <div class="signal-bars mt1 sizing-box <?php echo $score_arr['class']; ?>">
                                                <div class="first-bar bar"></div>
                                                <div class="second-bar bar"></div>
                                                <div class="third-bar bar"></div>
                                                <div class="fourth-bar bar"></div>
                                                <div class="fifth-bar bar"></div>
                                            </div>
                                            <div><span class="num-reviews p-sm-lable p-gray"><?php echo $score_arr['label']; ?></span></div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-2 watch-review">
                                        <!--div class="rh_woo_star" style="margin-bottom: 0;" title="Rated 5 out of 5">
                                            <span class="rhwoostar rhwoostar1">★</span><span class="rhwoostar rhwoostar2">★</span><span class="rhwoostar rhwoostar3">★</span><span class="rhwoostar rhwoostar4">★</span><span class="rhwoostar rhwoostar5">★</span>
                                        </div>
                                        <div><a class="p-sm-lable p-blue" href="javascript:void(0)">Rate now</a></div-->
                                        <div class="coupon-block p-mobile-hide">
                                            <?php
                                            //check if has source, brand, model, gender coupon
                                            $websourceCoupons = []; //$WS_CLASS->getSourceCoupons($value['WebSourceID']);
                                            $coupon_temp = '';
                                            $coupon_priority = -1;
                                            foreach($websourceCoupons as $couponkey => $websourceCoupon){
                                                $_p = 0;
                                                if($websourceCoupon['BrandId'] != '0'){
                                                    if($websourceCoupon['BrandId'] != $value['BrandID']){
                                                        break;
                                                    }else{
                                                        $_p++;
                                                    }
                                                }

                                                if($websourceCoupon['Model'] != '0'){
                                                    if($websourceCoupon['Model'] != $value['WatchModel']){
                                                        break;
                                                    }else{
                                                        $_p++;
                                                    }
                                                }

                                                if($websourceCoupon['GenderId'] != '0'){
                                                    if($websourceCoupon['GenderId'] != $value['GenderID'] && $value['GenderID'] != ''){
                                                        break;
                                                    }else{
                                                        $_p++;
                                                    }
                                                }

                                                if($coupon_priority < $_p){
                                                    $coupon_priority = $_p;
                                                    $coupon_temp = $websourceCoupon['Code'];
                                                }
                                            }

                                            if($coupon_temp != ''){$coupon_code = $coupon_temp; $coupon_filter = true;}

                                            //get coupon code
                                            $coupon_temp = ''; //$WS_CLASS->getCouponDynamicSourceId($value['StaticID'], $value['WebSourceID']);
                                            if($coupon_temp != ''){$coupon_code = $coupon_temp; $coupon_filter = false;}

                                            if( current_user_can('editor') || current_user_can('administrator') ) :
                                                //if not has coupon yet

                                                ?>
                                                <div class="action-<?php echo $value['StaticID'].'-'.$value['WebSourceID']; ?>">
                                                    <input type="text" class="coupon-input<?php if($coupon_code){echo ' hidden';} ?>" value="<?php echo $coupon_code; ?>" placeholder="Code Here"/>
                                                    <div class="flip-button flip-main flip-effect tc<?php if(!$coupon_code){echo ' hidden';} ?>">
                                                        <div class="label cursor-pointer">
                                                            <span><?php echo $coupon_code; ?></span>
                                                        </div>
                                                        <span class="coupon-title"> COUPON CODE </span>
                                                        <input type="hidden" value="<?php echo $coupon_code; ?>" class="code" />
                                                        <input type="hidden" value="<?php echo $value['WatchLink']; ?>" class="dealer-link" />
                                                    </div>
                                                    <a class="set-coupon<?php if($coupon_code){echo ' hidden';} ?>" static-id="<?php echo $value['StaticID']; ?>" websource-id="<?php echo $value['WebSourceID']; ?>" href="javascript:void(0)">Save</a>
                                                    <a class="update-coupon hidden" static-id="<?php echo $value['StaticID']; ?>" websource-id="<?php echo $value['WebSourceID']; ?>" href="javascript:void(0)">Update</a>
                                                    <a class="edit-coupon<?php if(!$coupon_code || $coupon_filter){echo ' hidden';} ?>" parent-c="<?php echo $value['StaticID'].'-'.$value['WebSourceID']; ?>" href="javascript:void(0)">Edit</a>
                                                    <a class="remove-coupon<?php if(!$coupon_code || $coupon_filter){echo ' hidden';} ?>" static-id="<?php echo $value['StaticID']; ?>" websource-id="<?php echo $value['WebSourceID']; ?>" href="javascript:void(0)">Remove</a>
                                                </div>

                                            <?php else: ?>
                                                <?php if($coupon_code): ?>
                                                    <div class="flip-button flip-main flip-block flip-effect tc">
                                                        <div class="label cursor-pointer">
                                                            <span><?php echo $coupon_code; ?></span>
                                                        </div>
                                                        <span class="coupon-title"> COUPON CODE </span>
                                                        <input type="hidden" value="<?php echo $coupon_code; ?>" class="code" />
                                                        <input type="hidden" value="<?php echo $value['WatchLink']; ?>" class="dealer-link" />
                                                    </div>
                                                    <div class="coupon-shown">
                                                        <input type="text" value="<?php echo $coupon_code; ?>" />
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
		        					<div class="col-md-2 text-center"><a href="<?php echo $value["WatchLink"]; ?>" target="_blank"><button class="btn p-button" style="padding: 10px 20px;">GO TO SHOP</button></a></div>
	        					</div>
	        					<div class="row p-desktop-hide compare-price-item" datalink="<?php echo $value["WatchLink"]; ?>">
                                    <?php if($count_item == 1): ?>
                                    <div class="row col-md-12"><span class="p-sm-lable p-pink">Compare with <?php echo $num_dealers; ?> dealers</span></div>
                                    <?php endif; ?>
	        						<div class="compare-item row">
                                        <div class="col-md-12 watch-name"><a href="<?php echo $value["WatchLink"]; ?>" target="_blank"><?php echo $value["WatchName"]; ?></a></div>
		        						<div class="compare-item-left">
		        							<div class="col-md-3"></div>
		        							<div class="col-md-3 text-center watch-review">
                                                <?php if($value['Ratio']): ?>
                                                <div class="signal-bars mt1 sizing-box <?php echo $score_arr['class']; ?>">
                                                    <div class="first-bar bar"></div>
                                                    <div class="second-bar bar"></div>
                                                    <div class="third-bar bar"></div>
                                                    <div class="fourth-bar bar"></div>
                                                    <div class="fifth-bar bar"></div>
                                                </div>
                                                <?php endif; ?>
											<div><span class="num-reviews"><?php if($value['Ratio']){echo $score_arr['label'];} ?></span></div>
                                                <!--div class="pro-item-icons"><i class="fas fa-credit-card"></i> | <i class="fas fa-truck"></i></div-->
                                                <div class="pro-item-icons">
                                                    <?php if($value['IsPaypal']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/paypal.png" /><?php endif; ?>
                                                    <?php if($value['IsVisa']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/visa.png" /><?php endif; ?>
                                                    <?php if($value['IsMastercard']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/mastercard.png" /><?php endif; ?>
                                                    <?php if($value['IsAmericanExpress']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/american-express.png" /><?php endif; ?>
                                                    <?php if($value['IsJCB']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/jcb.png" /><?php endif; ?>
                                                    <?php if($value['IsUnionPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/western-union-pay-card.png" /><?php endif; ?>
                                                    <?php if($value['IsAmazonPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/amazon-pay-card-logo.png" /><?php endif; ?>
                                                    <?php if($value['IsBankTransfer']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bank-transfer.png" /><?php endif; ?>
                                                    <?php if($value['IsBitcoin']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bitcoin.png" /><?php endif; ?>
                                                </div>
			        					    </div>

                                            <?php if($coupon_code): ?>
                                            <div class="col-md-3 coupon-block">
                                                    <div class="flip-button flip-main flip-block flip-effect tc">
                                                        <div class="label cursor-pointer">
                                                            <span><?php echo $coupon_code; ?></span>
                                                        </div>
                                                        <span class="coupon-title"> COUPON CODE </span>
                                                        <input type="hidden" value="<?php echo $coupon_code; ?>" class="code" />
                                                        <input type="hidden" value="<?php echo $value['WatchLink']; ?>" class="dealer-link" />
                                                    </div>
                                                    <div class="coupon-shown">
                                                        <input type="text" value="<?php echo $coupon_code; ?>" />
                                                    </div>
                                            </div>
                                            <?php endif; ?>

                                            <div class="col-md-3"><span class="p-ssm-lable"><?php echo 'Last update: '.$lastDateUpdate; ?></span></div>
		        						</div>
		        						<div class="compare-item-right">
		        							<div class="col-md-3 text-center watch-source ws-blue-color"><?php echo $value["WebSource"]; ?></div>
		        							<div class="col-md-3 text-center watch-price"><?php echo showProductPrice($value["price"], $value["currency"]); ?></div>
		        						</div>
                                        <div class="col-md-12">
                                            <a href="<?php echo $value["WatchLink"]; ?>" target="_blank">
                                                <button class="btn p-button">GO TO SHOP</button>
                                            </a>
                                        </div>
		        					</div>
	        					</div>
	        					<?php
	        					//}
	        				}
	        			}
	        		?>

                    <div class="col-md-12" style="font-style: italic;font-size: 13px; clear: both; padding-top: 10px;">
                        <p>*Scores are price\condition ratio compared to similar models and are done with Machine Learning.
                            Our current distribution is finished and looks like:</p>
                        <ul>
                            <li>Awesome Offer:10.1%</li>
                            <li>Good Offer: 12.9%</li>
                            <li>Fair Offer: 29.9%</li>
                            <li>Average (Common) Offer:  20.8%</li>
                            <li>Expensive Offer: 12.3%</li>
                            <li>No Rating - 13.9%</li>
                        </ul>
                        <p><a style="color: #45a6b6;" href="https://beta.watchsignals.com/ai-scoring-luxury-watches-methodology" target="_blank">*Machine Learning approach to evaluate Luxury Watches</a></p>
                    </div>
	        	</div>
              </div>
              <div class="tab-pane fade" id="tabs-description">
                <div class="watch-description">
                	<h6 class="ws-fawn-color"><?php echo $watchInfos['BrandName']; ?></h6>
                	<h3 class="ws-fawn-color">PRODUCT DETAILS</h3>
                	<div class="row">
                		<table class="shop_attributes">
							<tbody>
								<tr>
									<th>Reference number</th>
									<td><p><?php echo $watchInfos['ReferenceNumber']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Brand</th>
									<td><p><?php echo $watchInfos['BrandName']; ?></a></p>
									</td>
								</tr>
									<tr>
									<th>Model</th>
									<td><p><?php echo $watchInfos['WatchModel']; ?></a></p>
									</td>
								</tr>
									<tr>
									<th>Movement</th>
									<td><p><?php echo $watchInfos['MovementName']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Case material</th>
									<td><p><?php echo $watchInfos['CaseMaterialName']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Case Diameter</th>
									<td><p><?php echo $watchInfos['CaseDiameter']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Bracelet material</th>
									<td><p><?php echo $watchInfos['BraceletMaterialName']; ?></p>
									</td>
								</tr>
								</tr>
									<tr>
									<th>Bracelet color</th>
									<td><p><?php echo $watchInfos['BraceletColorName']; ?></p>
									</td>
								</tr>
								</tr>
									<tr>
									<th>Buckle</th>
									<td><p><?php echo $watchInfos['BuckleName']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Gender</th>
									<td><p><?php echo $watchInfos['GenderName']; ?></p>
									</td>
								</tr>
									<tr>
									<th>Number Of Jewels</th>
									<td><p><?php echo $watchInfos['NumberOfJewels']; ?></p>
									</td>
								</tr>
								</tr>
									<tr>
									<th>Power Reserve</th>
									<td><p><?php echo $watchInfos['PowerReserve']; ?></p>
									</td>
								</tr>
								</tr>
									<tr>
									<th>Water Resistance</th>
									<td><p><?php echo $watchInfos['WaterResistanceATM']; ?></p>
									</td>
								</tr>
							</tbody>
						</table>
                	</div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabs-review">
                <?php
			    	$avarageRating = 5; //$WS_CLASS->getAvarageRating($watchId);

			    	//count rating
			    	$ratingValues = [];
			    	$countRating = [];
			    	foreach($reviews as $key => $review){
			    		for($i = 1; $i <= 5; $i++){
			    			if($review['NumRate'] == $i){
			    				if(!$countRating[$i]){
			    					$countRating[$i] = 1;
			    				}else{
			    					$countRating[$i]++;
			    				}
			    			}
			    		}
			    	}
			    	for($i = 1; $i <=5; $i++){
			    		if(!$countRating[$i]){
			    			$ratingValues[$i]['Percent'] = 0;
			    			$ratingValues[$i]['Value'] = 0;
			    		}else{
			    			$percent = ($countRating[$i] / $numReview) * 100;
			    			$ratingValues[$i]['Percent'] = $percent;
			    			$ratingValues[$i]['Value'] =  $countRating[$i];
			    		}
			    	}

			    ?>
			    <div id="reviews" class="woocommerce woocommerce-Reviews row">
					<div class="woo-left-rev-part wpsm-one-half">
						<div class="row">
                            <div class="col-md-4">
                                <div class="woo-avg-rating">
                                    <h5><?php echo number_format($avarageRating, 1, '.', ''); ?><span>/5</span></h5>
                                    <?php if($numReview > 0): ?>
                                        <p class="avg-rating-star">
                                            <span class="star-rated" style="width: <?php echo ($avarageRating / 5) * 100; ?>%"></span>
                                        </p>
                                        <br/><span class="num-rating"><?php echo ($numReview > 1) ? $numReview.' Ratings' : $numReview.' Rating'; ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="woo-rating-bars">
                                    <?php
                                        for($i = 5; $i > 0; $i--){
                                            ?>
                                            <div class="rating-bar">
                                                <div class="star-rating-wrap">
                                                    <div class="rh_woo_star" title="Rated <?php echo $i; ?> out of 5">
                                                        <?php
                                                            for($j = 1; $j <= 5; $j++){
                                                                if($j <= $i ){
                                                                    echo '<span class="rhwoostar rhwoostar'.$j.' active">★</span>';
                                                                }else{
                                                                    echo '<span class="rhwoostar rhwoostar'.$j.'">★</span>';
                                                                }
                                                            }
                                                        ?>

                                                    </div>

                                                </div>
                                                                <div class="rating-percentage-bar-wrap">
                                                    <div class="rating-percentage-bar">
                                                        <span style="width:<?php echo $ratingValues[$i]['Percent']; ?>%" class="rating-percentage"></span>
                                                    </div>
                                                </div>
                                                <div class="rating-count zero"><?php echo $ratingValues[$i]['Value']; ?></div>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" id="review_form_wrapper">
                                <div>
                                    <div id="respond" class="comment-respond">
                                        <div class="row">
                                            <div class="col-md-7 review-quality">
                                                <h5>Quality</h5>
                                                <span>(Are you satisfied with this product?)</span>
                                                <p class="rating-error rating-star">Please rate your stars</p>
                                            </div>
                                            <div class="col-md-5 rating-quality">
                                                <p class="stars"><span><a class="star-1" href="javascript:void(0)">1</a><a class="star-2" href="javascript:void(0)">2</a><a class="star-3" href="javascript:void(0)">3</a><a class="star-4" href="javascript:void(0)">4</a><a class="star-5" href="javascript:void(0)">5</a></span></p>
                                            </div>
                                        </div>
                                        <h6 id="reply-title" class="comment-reply-title">WRITE YOUR OPINIONS</h6>
                                        <form action="" method="post" id="commentform" class="comment-form">
                                            <p class="comment-form-comment"><label for="comment"></label><textarea placeholder="Tell us what your thought about it" id="comment" name="comment" cols="45" rows="8" aria-required="true" required=""></textarea></p>
                                            <p class="rating-error rating-comment">Please type your review</p>
                                            <div class="review-photo">
                                                <input type="file" id="review-photo" class="inputfile" name="review-photo" onchange="readURL(this);"/>
                                                <label for="review-photo">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/photo-camera.png" no-photo="<?php echo get_template_directory_uri(); ?>/images/photo-camera.png"/>
                                                    <div><span>Upload Photo</span></div>
                                                </label>
                                                <p class="rating-error rating-photo">error</p>
                                            </div>
                                            <p class="comment-form-author"><label for="author"></label></p> <div class="input-border"><input id="author" name="author" type="text" value="" size="30" aria-required="true" placeholder="Your name" required=""></div>
                                            <p class="comment-form-email"><label for="email"></label></p> <div class="input-border"><input id="email" name="email" type="email" value="" size="30" aria-required="true" placeholder="Your email" required=""></div>
                                            <p class="rating-error rating-email">Please enter your valid email</p>

                                            <!--p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                            <div class="comment-form-rating row">
                                                <label for="rating">Your Rating</label>
                                                <select name="rating" id="rating" aria-required="true" required="" style="display: none;">
                                                    <option value="">Rate…</option>
                                                    <option value="5">Perfect</option>
                                                    <option value="4">Good</option>
                                                    <option value="3">Average</option>
                                                    <option value="2">Not that bad</option>
                                                    <option value="1">Very Poor</option>
                                                </select></div-->


                                            <p class="text-center"><input name="submit" type="button" id="post-review" class="submit" value="DONE"> <input type="hidden" name="comment_post_ID" value="181" id="comment_post_ID">
                                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
    				<div class="woo-right-rev-part wpsm-one-half wpsm-column-last">
                        <div id="comments">

                            <?php if ( $reviews ) : ?>
                                <h6 id="reply-title" class="comment-reply-title">CUSTOMER OPINIONS</h6>
                                <div class="commentlist">
                                    <?php
                                    foreach($reviews as $key => $review){
                                        ?>
                                        <li class="comment byuser comment-author-recompare bypostauthor even thread-even depth-1" id="li-comment-<?php echo $review['Id']; ?>">

                                            <div id="comment-<?php echo $review['Id']; ?>" class="comment_container">

                                                <!--img alt="" src="http://1.gravatar.com/avatar/dcee5f18ad20764c16917c24a96366e8?s=60&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/dcee5f18ad20764c16917c24a96366e8?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60"-->
                                                <div class="comment-text">
                                                    <div class="comment-customer">
                                                        <div class="rh_woo_star" title="Rated <?php echo $review['NumRate']; ?> out of 5">
                                                            <?php
                                                            for($j = 1; $j <= 5; $j++){
                                                                if($j <= $review['NumRate']){
                                                                    echo '<span class="rhwoostar rhwoostar'.$j.' active">★</span>';
                                                                }else{
                                                                    echo '<span class="rhwoostar rhwoostar'.$j.'">★</span>';
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <p class="meta">
                                                            <span>by <?php echo ($review['UserName'] != '') ? $review['UserName'] : 'Customer'; ?> | Quality: <?php echo $review['NumRate']; ?></span>
                                                        </p>
                                                    </div>
                                                    <div class="comment-date">
                                                        <time class="woocommerce-review__published-date" datetime="<?php echo $review['CreatedTime']; ?>"><?php $reviewdate=date_create($review['CreatedTime']);
                                                            echo date_format($reviewdate,"F d, Y"); ?></time>
                                                    </div>

                                                    <div class="description"><p><?php echo $review['Comment']; ?></p>
                                                    </div>
                                                    <div class="re-photo">
                                                        <?php if($review['Photo'] && $review['Comment'] != ''): ?>
                                                            <img height="50" src="<?php echo get_site_url().'/wp-content/uploads/review/'.$review['Photo']; ?>" />
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </div>
                            <?php else : ?>
                                <div class="ws-noreviews">
                                    <p>
                                        <span><i class="fas fa-caret-left"></i></span>
                                        No reviews for this product. <br/>Be the 1st to write a review.
                                    </p>
                                </div>
                            <?php endif; ?>
                        </div>
					</div>

					<div class="clear"></div>
				</div>
              </div>
            </div>
          </div>
	</div>
	<div class="p-foot">
		<div class="col-md-12"><h4 class="ws-fawn-color">Last Seen</h4></div>
		<div class="col-md-12 position-relative">
	        <a class="owl-navigation prev"><i class="fas fa-arrow-left"></i></a>
	        <div id="owl-demo" class="owl-carousel">
	        	<?php
	        		//last seen
	        		$last_seen = $_SESSION['LASTSEEN'];
	        		if(count($last_seen) > 0):
	        			foreach($last_seen as $k => $pro_id):
	        				$ls_pro = $WS_CLASS->getWatchById($pro_id);
	        				if($ls_pro && count($ls_pro) > 0){
	        					$_ls_pro = $ls_pro[0];
	        					/*echo '<pre>';
	        					print_r($_ls_pro);
	        					echo '</pre>';*/
	        				}?>
	        			<div class="item">
							<figure class="position-relative">
								<a class="" href="<?php echo $WS_CLASS->base58_encode_url($pro_id, $_ls_pro['watchname']); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/img_watch.png"></a>
							</figure>
							<div class="cat_for_grid">
								<div class="text-left" style="padding-left: 10px;font-size: 15px; min-height: 44px;"><a href="<?php echo $WS_CLASS->base58_encode_url($pro_id, $_ls_pro['watchname']); ?>" style="color: #707070; font-weight: 600;">
									<span><?php echo $_ls_pro['watchname']; ?></span>
								</a></div>
								<div class="text-left" style="padding-left: 10px; margin-top: 10px; font-size: 22px;">
									<span class="price ws-fawn-color" style="color: #707070;"><?php echo $WS_CLASS->getPriceRange($pro_id); ?></span>
								</div>
								<div class="text-left" style="padding-left: 10px; margin-top: 20px; ">
									<span style="font-size: 12px; color: #707070;">Dealer</span><br/>
									<span style="font-size: 15px; color: #707070;"><?php echo $_ls_pro['BrandName']; ?></span>
								</div>
								<div class="rh_woo_star" style="text-align: left; padding-left: 10px;" title="Rated 5 out of 5">
									<span class="rhwoostar rhwoostar1 active">★</span><span class="rhwoostar rhwoostar2 active">★</span><span class="rhwoostar rhwoostar3 active">★</span><span class="rhwoostar rhwoostar4 active">★</span><span class="rhwoostar rhwoostar5 active">★</span>
									<span style="font-size: 12px; color: #45A6B6;">(22 reviews)</span>
								</div>
							</div>

						</div>
	        			<?php endforeach;?>
	        		<?php else: ?>
	        			<div class="item">No product was viewed before</div>
	        		<?php endif;
	        		if($product_flag){lastSeen($product_id);}
	        	?>
	        </div>
			<a class="owl-navigation next"><i class="fas fa-arrow-right"></i></a>
	      </div>
	</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade price-history-modal" id="priceHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close ws-blue-color" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times"></i></span>
        </button>
      <div class="modal-body">
       	<div class="row popup-price">
       		<div class="col-md-12 text-center">
	       		<h3 class="ws-fawn-color"><b>PRICE HISTORY</b></h3>
	       		<span class="small-text" style="color: #707070;">Compare the price of watch</span>

	       		<div class="current-price">
	       			<span class="small-text" style="color: #FFC2A6;">Current price</span>
	       			<div class="price">
	       				<span><?php if($lowestPriceObj){ echo showProductPrice($lowestPriceObj["price"], $lowestPriceObj["currency"]); }?></span>
	       			</div>
	       			<div class="price-chart-menu">
	       				<a href="javascript:void(0)" class="active">Week</a>
						<a href="javascript:void(0)">Month</a>
						<a href="javascript:void(0)">Year</a>
	       			</div>
	       			<div id="mb_curve_chart" style="width: 100%;overflow: hidden; margin-left: -15px;">

	       			</div>
	       		</div>
	       	</div>
       	</div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="notify-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered ws-modal" role="document">

    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
      <div class="modal-body notify-form">
        <h2>SET CURRENT BEST PRICE</h2>
		<h3>Rolex Yacht-Master II</h3>

		<div class="row">
			<form id="alert-price-form" class="needs-validation" novalidate>
				<input type="hidden" value="Rolex Yacht-Master II" name="watchname" />
			<div class="col-md-12 no-padding-leftright notify-currentPrice">Current price: <span class="price"><?php echo showProductPrice($lowest_price, $price_currency); ?></span><input type="hidden" value="<?php echo $lowest_price; ?>" name="oprice" /><input type="hidden" value="<?php echo $price_currency; ?>" name="currency"></div>
			<div class="col-md-12 no-padding-leftright">
			  <div class="form-group row">
			  	<div class="col-md-5">
					<label class="desired-price-lbl">Select desired price:</label>
				</div>
				<div class="col-md-7">
					<label class="sr-only" for="alert-price-input"></label>
					<div class="input-group mb-2">

						<input type="text" class="form-control" value="<?php echo round($lowest_price - $lowest_price * 5 / 100); ?>" id="alert-price-input" placeholder="Price" name="dprice" required>
						<div class="input-group-prepend">
							<div class="input-group-text" style="border-radius:0;">$</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
			<hr>
			<div class="col-md-12 no-padding-leftright">
				  <div class="form-group form-group-email">
					<label for="subcriberEmail">Enter your email to get notification of the best current price for <br/><span class="watch-name">Rolex Yacht - Master II</span></label>
					<input type="email" class="form-control" id="subcriberEmail" placeholder="Email" name="email" required>
				  </div>
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="newsletter-input" name="newsletter" value="checked">
					  <label class="custom-control-label" for="newsletter-input">Would you like to receive our newsletter, subscriptions, and all marketing materials?</label>
					</div>
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="agreeTerm" required>
					  <label class="custom-control-label" for="agreeTerm">I have read the privacy policy and agree to the Terms of Use.</label>
						<div class="invalid-feedback">
						You must agree before submitting.
					  </div>
					</div>
					<input type="hidden" name="action" value="alertprice"/>
				  <button type="button" id="subcribe-active" class="btn btn-default">ACTIVE PRICE ALERT</button>
			</div>
			</form>
		</div>
      </div>
    </div>
  </div>
</div>
<div class="notify-area">
    <h6>Notify me when price changes!</h6>
    <button type="button" class="btn btn-default" id="notify-me-btn" data-toggle="modal" data-target="#notify-modal">Alert price</button>
</div>
<div class="notify-area-mobi" data-toggle="modal" data-target="#notify-modal">
    <i class="fas fa-bell"></i>
</div>

<script type="text/javascript">
	var price_chart = false;
	var watchItem=<?php echo json_encode($watchItems); ?>;
</script>
