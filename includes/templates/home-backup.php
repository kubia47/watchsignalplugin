<link rel="stylesheet" type="text/css" href="<?php echo WATCHSIGNALS_URL.'assets/css/productdetails.css'; ?>">
<div class="container-fluid searchhomepage-container">
	<div class="row">		
		<div class="col-md-12 center-vertical-horizontally">
			<div class="main-banner">
				<img alt="WatchSignals" src="<?php echo get_template_directory_uri(); ?>/images/watch_comparison_watchsignals_header.png">
			</div>
			<h1>Find best luxury watches around the world</h1>
			<div class="search-input-container">
				<!--div class="count-result"></div-->
				<div class="form-control text-center">
					<span class="search-icon"><i class="fas fa-search ws-fawn-color"></i></span>
					<input type="text" id="main-search-input" placeholder="Search watch here...">					
				</div>
				<!--div class="row tab-wrapper">					
					<div class="col-md-3 tap-element">Brands</div>
					<div class="col-md-3 tap-element">Dealers</div>
					<div class="col-md-3 tap-element">Models</div>
					<div class="col-md-3 tap-element">Brands</div>
				</div-->
			</div>
			<h3><i>Search <?php echo number_format($totalRecords[0]["Total"], 0, '.', ',') ?> watches from <?php echo count($locations); ?> countries</i><h3>
		</div>
	</div>	
</div>
<div class="container-fluid model3d-container">
	<div class="row">
		<h1 class="widget-title">Virtual Reality store</h1>
		<p class="widget-desc">We help bring luxury experience online with VR. You will be able to see your loved watches to every single details without going to a physical store. More to come...</p>
		<h4 class="widget-title-watch">Rolex GMT-Master II 116719-BLRO</h4>
		<div class="col-md-12 iframe-container">			
			<iframe 
				src="https://sketchfab.com/models/c4bb0e55a227463a901243e63476c7bb/embed?annotations_visible=0&autospin=0&autostart=1&camera=0&ui_infos=0&ui_stop=1&max_texture_size=8192&transparent=1&ui_controls=0&ui_general_controls=0&ui_help=0&ui_inspector=0&ui_settings=0&ui_stop=0&ui_watermark_link=0&ui_watermark=0">
			</iframe>
			<div class="iframe-button-area">
				<div class="iframe-btn-item">
				<a target="_blank" href="<?php echo home_url(); ?>/rolex_gmt-master_ii_116710blnr_5bL3"><i class="fas fa-chart-line"></i> Price History</a>
				</div>
				<div class="iframe-btn-item">
				<a href="#" data-toggle="modal" data-target="#notify-modal"><i class="fas fa-bell"></i> Price Alert</a>			
				</div>
			</div>
		</div>
		<div class="col-md-12">
            <div class="compare-price-list">
                <?php
                $sample_proposals = $WS_CLASS->getDynamicProposalForHomePage();
                $min_price = 1000000;
                $min_price_cur = '';
                //print_r($sample_proposals);
                foreach($sample_proposals as $k => $value):

                    if($min_price > (int)$value['Price']){
                        $min_price = (int)$value['Price'];
                        $min_price_cur = $value['Currency'];
                    }

                    $score_arr = $WS_CLASS->scoring((double)$value['Ratio']);
                    ?>
                    <div class="row compare-block compare-watch-item p-mobile-hide" datalink="<?php echo $value["WatchLink"]; ?>">
                        <div class="col-md-3 watch-name p-black" title="<?php echo $value["WatchName"]; ?>"><?php echo $value["WatchName"]; ?></div>
                        <div class="col-md-2 watch-price"><b><?php echo $WS_CLASS->showProductPrice($value['Price'], $value['Currency']); ?></b><br><span class="p-blue p-sm-lable">(Not incl. shipping)</span></div>
                        <div class="col-md-2 watch-source ws-blue-color"><div><span class="p-sm-lable p-gray" style="line-height: 20px;">Type of payment</span></div><div>
                                <?php if($value['IsPaypal']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/paypal.png" /><?php endif; ?>
                                <?php if($value['IsVisa']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/visa.png" /><?php endif; ?>
                                <?php if($value['IsMastercard']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/mastercard.png" /><?php endif; ?>
                                <?php if($value['IsAmericanExpress']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/american-express.png" /><?php endif; ?>
                                <?php if($value['IsJCB']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/jcb.png" /><?php endif; ?>
                                <?php if($value['IsUnionPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/western-union-pay-card.png" /><?php endif; ?>
                                <?php if($value['IsAmazonPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/amazon-pay-card-logo.png" /><?php endif; ?>
                                <?php if($value['IsBankTransfer']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bank-transfer.png" /><?php endif; ?>
                                <?php if($value['IsBitcoin']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bitcoin.png" /><?php endif; ?>

                            </div></div>
                        <div class="col-md-2 watch-review text-center">
                            <div class="rh_woo_star" style="margin-bottom: 0;" title="Rated 5 out of 5">
                                <span class="rhwoostar rhwoostar1">★</span><span class="rhwoostar rhwoostar2">★</span><span class="rhwoostar rhwoostar3">★</span><span class="rhwoostar rhwoostar4">★</span><span class="rhwoostar rhwoostar5">★</span>
                            </div>
                            <!--div><span class="p-nm-lable p-blue">20 reviews</span></div-->
                            <div><a class="p-sm-lable p-blue" href="javascript:void(0)">Rate now</a></div>
                        </div>
                        <div class="col-md-2 watch-review">
                            <div><?php echo $value["WebSource"]; ?></div>
                            <div><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/germany.png" /></div>
                        </div>
                        <div class="col-md-2 text-center">
                            <?php if($value['Ratio']): ?>
                                <div class="signal-bars mt1 sizing-box <?php echo $score_arr['class']; ?>">
                                    <div class="first-bar bar"></div>
                                    <div class="second-bar bar"></div>
                                    <div class="third-bar bar"></div>
                                    <div class="fourth-bar bar"></div>
                                    <div class="fifth-bar bar"></div>
                                </div>
                                <div><span class="num-reviews p-sm-lable p-gray"><?php echo $score_arr['label']; ?></span></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-2 text-center"><a href="<?php echo $value["WatchLink"]; ?>" target="_blank"><button class="btn p-button" style="padding: 10px 20px;">GO TO SHOP</button></a></div>
                    </div>
                    <div class="row p-desktop-hide compare-price-item" datalink="<?php echo $value["WatchLink"]; ?>">
                        <div class="compare-item row">
                            <div class="col-md-12 watch-name"><a href="<?php echo $value["WatchLink"]; ?>" target="_blank"><?php echo $value["WatchName"]; ?></a></div>
                            <div class="compare-item-left">
                                <div class="col-md-3"></div>
                                <div class="col-md-3 text-center watch-review">
                                    <?php if($value['Ratio']): ?>
                                        <div class="signal-bars mt1 sizing-box <?php echo $score_arr['class']; ?>">
                                            <div class="first-bar bar"></div>
                                            <div class="second-bar bar"></div>
                                            <div class="third-bar bar"></div>
                                            <div class="fourth-bar bar"></div>
                                            <div class="fifth-bar bar"></div>
                                        </div>
                                    <?php endif; ?>
                                    <div><span class="num-reviews"><?php if($value['Ratio']){echo $score_arr['label'];} ?></span></div>
                                    <div class="pro-item-icons">
                                        <?php if($value['IsPaypal']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/paypal.png" /><?php endif; ?>
                                        <?php if($value['IsVisa']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/visa.png" /><?php endif; ?>
                                        <?php if($value['IsMastercard']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/mastercard.png" /><?php endif; ?>
                                        <?php if($value['IsAmericanExpress']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/american-express.png" /><?php endif; ?>
                                        <?php if($value['IsJCB']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/jcb.png" /><?php endif; ?>
                                        <?php if($value['IsUnionPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/western-union-pay-card.png" /><?php endif; ?>
                                        <?php if($value['IsAmazonPay']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/amazon-pay-card-logo.png" /><?php endif; ?>
                                        <?php if($value['IsBankTransfer']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bank-transfer.png" /><?php endif; ?>
                                        <?php if($value['IsBitcoin']): ?><img width="25" src="<?php echo get_template_directory_uri(); ?>/images/bitcoin.png" /><?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="compare-item-right">
                                <div class="col-md-3 text-center watch-source ws-blue-color"><?php echo $value["WebSource"]; ?></div>
                                <div class="col-md-3 text-center watch-price"><?php echo $WS_CLASS->showProductPrice($value['Price'], $value['Currency']); ?></div>
                            </div>
                            <div class="col-md-12">
                                <a href="<?php echo $value["WatchLink"]; ?>" target="_blank">
                                    <button class="btn p-button">GO TO SHOP</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
		</div>
		<div class="col-md-12" style="font-style: italic;font-size: 13px;">
			<p>*Scores are price\condition ratio compared to similar models and are done with Machine Learning. 
Our current distribution is finished and looks like:</p>
			<ul>
			<li>Awesome Offer:10.1%</li>
			<li>Good Offer: 12.9%</li>
			<li>Fair Offer: 29.9%</li>
			<li>Average (Common) Offer:  20.8%</li>
			<li>Expensive Offer: 12.3%</li>
			<li>No Rating - 13.9%</li>			
			</ul>
			<p><a style="color: #45a6b6;" href="https://beta.watchsignals.com/ai-scoring-luxury-watches-methodology" target="_blank">*Machine Learning approach to evaluate Luxury Watches</a></p>
		</div>
	</div>
</div>
<div class="container-fluid featured-prod-container">  
    <div class="row">
	  <h1 class="widget-title">Most Popular Products</h1>
	  <!--h4 class="widget-summary">Automatically show several products per week.</h4-->
      <div class="col-md-12 owl-container position-relative">
        <a class="owl-navigation prev"><i class="fas fa-arrow-left"></i></a>		
        <div id="owl-demo" class="owl-carousel">
			<?php
			foreach($query_result as $item){
				$link=home_url().'/'.$WS_CLASS->base58_encode_url($item["ID"], $item["WatchName"]);
				$imgSource=WATCHSIGNALS_NOPHOTO_URL;
				$priceRange="From $".number_format($item["MinPrice"], 0, '.', ',');			
			?>
			<div class="item">
				<figure class="position-relative">    
					<a class="" href="<?php echo $link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/no-photo.png"></a>  										
				</figure>
				<div class="cat_for_grid">
					<h5><a href="<?php echo $link; ?>" title="<?php echo $item["WatchName"]; ?>"><?php echo $item["WatchName"]; ?></a></h5>			
					<h4><?php echo $priceRange; ?></h4>
				</div>
				<div class="article-dealer">
					<!--p class="text-muted">Dealer</p-->
					<!--p class="text-info"><?php //echo $item["WebSource"]; ?></p-->
					<span class="article-rating">
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
					</span>
				</div>				
			</div>
			<?php
			}
			?>
        </div>
		<a class="owl-navigation next"><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
</div>
<div class="container-fluid shareandvote-container">
	<div class="row">
		<h1 class="widget-title">SHARE & VOTE YOUR WATCHES</h1>
		<p class="widget-desc">Share your great wristshots and get featured and credit on our Instagram channel. We are preparing a playground for watchlovers like you to vote for your beloved timepieces in competition.</p>
		<div class="col-md-12 text-center"><button class="submit-your-watch" onclick="window.location.href='<?php echo home_url().'/luxury-watch-photo-contest' ?>';">SUBMIT YOUR WATCH</button></div>
		<div class="row promo-item-container">
			<div class="photo-contest-header">Photo Contest: Share your best wristshot to win a watch winder!</div>
		</div>
	</div>
</div>
<div class="container-fluid subcriber-container">
	<div class="row">
		<h1 class="widget-title">SUBSCRIBE TO HAVE A BETTER DEAL</h1>
		<h4 class="widget-summary">More updates on trends, price and all watch comparison</h4>      		
		<form class="subcriber-frm form-inline" id="subscribe-form">		  		  
			<div class="form-group">
				<input type="email" id="sub-email" name="sub-email" class="form-control" placeholder="Enter email">	
				<input type="hidden" name="action" value="subscribe" />		
			</div>			
			<button type="button" id="subscribe-button" class="btn btn-default">SUBSCRIBE</button>
			<label class="sub-email-error">Please enter valid email</label>
		</form>		
	</div>
</div>
<div class="modal fade" id="notify-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered ws-modal" role="document">

    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
      <div class="modal-body notify-form">
        <h2>SET CURRENT BEST PRICE</h2>
		<h3>ROLEX GMT-MASTER II 116719-BLRO</h3>

		<div class="row">
			<form id="alert-price-form" class="needs-validation" novalidate>
				<input type="hidden" value="Rolex Yacht-Master II" name="watchname" />
			<div class="col-md-12 no-padding-leftright notify-currentPrice">Current price: <span class="price"><?php echo $WS_CLASS->showProductPrice($min_price, $min_price_cur); ?></span><input type="hidden" value="<?php echo $min_price; ?>" name="oprice" /><input type="hidden" value="<?php echo $min_price_cur; ?>" name="currency"></div>
			<div class="col-md-12 no-padding-leftright">
			  <div class="form-group row">
			  	<div class="col-md-5">
					<label class="desired-price-lbl">Select desired price:</label>
				</div>
				<div class="col-md-7">
					<label class="sr-only" for="alert-price-input"></label>
					<div class="input-group mb-2">
						
						<input type="text" class="form-control" value="<?php echo round($min_price - $min_price * 5 / 100); ?>" id="alert-price-input" placeholder="Price" name="dprice" required>
						<div class="input-group-prepend">
							<div class="input-group-text" style="border-radius:0;">$</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
			<hr>
			<div class="col-md-12 no-padding-leftright">				
				  <div class="form-group form-group-email">
					<label for="subcriberEmail">Enter your email to get notification of the best current price for <br/><span class="watch-name">ROLEX GMT-MASTER II 116719-BLRO</span></label>
					<input type="email" class="form-control" id="subcriberEmail" placeholder="Email" name="email" required>
				  </div>
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="newsletter-input" name="newsletter" value="checked">
					  <label class="custom-control-label" for="newsletter-input">Would you like to receive our newsletter, subscriptions, and all marketing materials?</label>
					</div>
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="agreeTerm" required>
					  <label class="custom-control-label" for="agreeTerm">I have read the privacy policy and agree to the Terms of Use.</label>
						<div class="invalid-feedback">
						You must agree before submitting.
					  </div>
					</div>	
					<input type="hidden" name="action" value="alertprice"/>													  
				  <button type="button" id="subcribe-active" class="btn btn-default">ACTIVATE PRICE ALERT</button>
			</div>
			</form>
		</div>
      </div>      
    </div>
  </div>
</div>
<script>
var featuredProduct=<?php echo json_encode($query_result); ?>;
var locations=<?php echo json_encode($locations); ?>;
setTimeout(function(){ 
	$('.compare-watch-item, .compare-price-item').click(function(){
		var _href = $(this).attr('datalink');
		window.open(_href, '_blank');
	});
	$(".pc-gal-select-last2").remove();
	$(".contest-pagination").remove();
	$($(".contest-for-home-page").detach()).appendTo(".promo-item-container");
	$(".promo-item-container").append("<div class='photo-contest-seemore'><a href='<?php echo home_url().'/luxury-watch-photo-contest' ?>'>See More</a></div>");
	$(".contest-for-home-page").show();
}, 500);
</script>