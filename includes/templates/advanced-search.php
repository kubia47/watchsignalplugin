<div class="row ws-adsearch-container">
	<!--div class="col-md-3 ws-adsearch-topkeywords">
		<h4>TOP KEYWORDS</h4>
		<ol class="numeration">
		   <li><a class="link" href="#">Rolex Submariner</a></li>
		   <li><a class="link" href="#">Breitling Navitimer</a></li>
		   <li><a class="link" href="#">TAG Heuer Carrera</a></li>
		   <li><a class="link" href="#">Tudor Black Bay</a></li>
		   <li><a class="link" href="#">Hublot Big Bang</a></li>
		   <li><a class="link" href="#">Audemars Piguet Royal Oak Offshore</a></li>
		   <li><a class="link" href="#">Omega Seamaster Railmaster</a></li>
		   <li><a class="link" href="#">Breitling Superocean</a></li>
		   <li><a class="link" href="#">TAG Heuer Connected</a></li>
		   <li><a class="link" href="#">TAG Heuer Aquaracer</a></li>
		   <li><a class="link" href="#">Rolex Datejust Oysterquartz</a></li>
		   <li><a class="link" href="#">Omega Seamaster 300</a></li>
		   <li><a class="link" href="#">IWC Portuguese</a></li>
		   <li><a class="link" href="#">Longines HydroConquest</a></li>
		   <li><a class="link" href="#">TAG Heuer Monaco</a></li>
		   <li><a class="link" href="#">Certina DS Action</a></li>
		   <li><a class="link" href="#">TAG Heuer Formula 1</a></li>
		   <li><a class="link" href="#">Omega Genève</a></li>
		   <li><a class="link" href="#">Breitling Colt</a></li>
		   <li><a class="link" href="#">Cartier Santos</a></li>
		</ol>
	</div-->
	<div class="col-md-12 ws-adsearch-adsearch-area">
		<h4>SEARCH FOR A WRISTWATCH</h4>
		<form>
			<input class="ws-search-link" type="hidden" value="<?php echo home_url(); ?>/search/">
		<!-- line 1 -->
		  <div class="row">
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Brand</label>
			<select class="selectpicker" id="ws-brand-dropdown" multiple title="All manufactures" data-showSubtext="true" data-size="7" data-live-search="true">												  
			<?php		
			foreach($brandArray as $brand){
				echo "<option value='".$brand["ID"]."'>".$brand["Name"]."</option>";
			}
			?>
			</select>
		  </div>
		  <div class="form-group col-md-6">
			<label for="ws-additional-search">Additional search terms</label>
			<input type="text" class="form-control" id="ws-additional-search" placeholder="Model, reference number, ...">
		  </div>
		  </div>
		  <!-- line 2 -->
		  <div class="row">
		  <div class="form-group col-md-12">
			<hr>
		  </div>
		  </div>
		  <!-- line 3 -->
		  <div class="row">
		  <div class="form-group col-md-4 ws-movement-container">
			<label>Movement</label>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="movement" value="Automatic">
				Automatic
			  </label>
			</div>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="movement" value="Manual">
				Manual winding
			  </label>
			</div>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="movement" value="Quartz">
				Quartz
			  </label>
			</div>
		  </div>
		  <div class="form-group col-md-4 ws-scopeofdelivery-container">
			<label>Scope of delivery</label>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="Scopeofdelivery" value="box">
				With box
			  </label>
			</div>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="Scopeofdelivery" value="papers">
				With papers
			  </label>
			</div>			
		  </div>
		  <div class="form-group col-md-4 ws-condition-container">
			<label>Condition</label>			
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="condition" value="New/unworn">
				New/unworn
			  </label>
			</div>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="condition" value="worn/Pre-owned">
				Pre-owned
			  </label>
			</div>			
		  </div>
		  </div>
		  <!-- line 4 -->
		  <div class="row">
		  <div class="form-group col-md-12">
			<hr>
		  </div>
		  </div>
		  <!-- line 5 -->
		  <div class="row">
		  <!--div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Type of Watch</label>
			<select class="form-control" id="ws-typeofwatch-dropdown">
			  <option>---</option>
			  <option>Wristwatch</option>
			  <option>Pocket watch</option>
			  <option>Other watch</option>			  
			</select>			
		  </div-->
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Gender</label>
			<select class="form-control" id="ws-gender-dropdown">
			  <option>---</option>
			  <option value="men">Men's watch/Unisex</option>
			  <option value="women">Women's watch</option>			  
			</select>
			</div>
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Case material</label>
			<select class="form-control" id="ws-casematerial-dropdown">
			  <option>---</option>			  
			  <option>Aluminum</option>			  
			  <option>Bronze</option>			  
			  <option>Carbon</option>			  
			  <option>Ceramic</option>			  
			  <option>Gold/Steel</option>			  
			  <option>Palladium</option>			  
			  <option>Plastic</option>			  
			  <option>Platinum</option>			  
			  <option>Red gold</option>			  
			  <option>Rose gold</option>			  
			  <option>Silver</option>			  
			  <option>Steel</option>			  
			  <option>Tantalum</option>			  
			  <option>Titanium</option>			  
			  <option>White gold</option>			  
			  <option>Wolfram</option>			  
			  <option>Yellow gold</option>			  			  
			</select>
			</div>
		  </div>
		  <!-- line 6 >
		  <div class="row">		  		  
		  </div-->
		  <!-- line 6 -->
		  <div class="row">
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Bracelet material</label>
			<select class="form-control" id="ws-braceletmaterial-dropdown">
			  <option>---</option>
			  <option>Aluminium</option>
			  <option>Calf skin</option>
			  <option>Ceramic</option>
			  <option>Crocodile skin</option>
			  <option>Gold/Steel</option>
			  <option>Leather</option>
			  <option>Lizard skin</option>
			  <option>Ostrich skin</option>
			  <option>Plastic</option>
			  <option>Platinum</option>
			  <option>Red Gold</option>
			  <option>Rose Gold</option>
			  <option>Rubber</option>
			  <option>Satin</option>
			  <option>Shark skin</option>
			  <option>Silicon</option>
			  <option>Silver</option>
			  <option>Snake skin</option>
			  <option>Steel</option>
			  <option>Textile</option>
			  <option>Titanium</option>
			  <option>White Gold</option>
			  <option>Yellow gold</option>			  
			</select>			
		  </div>
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Location</label>
			<select class="form-control" id="ws-location-dropdown">
			  <option value>Please select</option>
			  <?php			
				foreach($WS_COUNTRIES as $country){
				?>
				<option value="<?php echo $country ?>"><?php echo $country ?></option>			
				<?php
				}
				?>	  
			</select>			
		  </div>
		  <!--div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Date Added</label>
			<select class="form-control" id="ws-dateadded-dropdown">
			  <option>---</option>
			  <option>... since yesterday</option>
			  <option>... since three days</option>			  
			  <option>... since one week</option>			  
			  <option>... since three weeks</option>			  
			</select>
			</div-->
		  </div>
		  <!-- line 7 -->
		  <div class="row" class="scrollhere">
		  <div class="form-group col-md-12">
			<hr>
		  </div>
		  </div>
		  <!-- line 8 -->
		  <div class="row">
		  <div class="form-group col-md-6">
			<div class="col-md-12" style="padding:0;"><label for="ws-brand-dropdown">Price ($)</label></div>
			<div class="col-md-5" style="padding:0;"><input type="text" class="form-control" id="ws-pricefrom-search" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></div>
			<div class="col-md-2" style="text-align:center;line-height: 30px;">-</div>
			<div class="col-md-5" style="padding:0;"><input type="text" class="form-control" id="ws-priceto-search" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></div>
		  </div>
		  <div class="form-group col-md-6">
			<label for="ws-brand-dropdown">Sorted by</label>
			<select class="form-control" id="ws-sortby-dropdown">			  
			  <option value="Relevance" selected>Relevance</option>
			  <option value="Newest">Newest</option>			  
			  <option value="PriceLowToHigh">Price: low to high</option>			  			 
			  <option value="PriceHighToLow">Price: high to low</option>			  			 
			</select>
			</div>
		  </div>
		  <!-- line  -->
		  <div class="row">
		  <div class="form-group col-md-12" style="text-align: right;">
			<button type="button" class="wpsm-button red ws-adsearch-btn" style="padding: 20px 40px;font-size: 20px;border-radius: 0 !important;margin-top: 20px;">Search</button>
		  </div>
		  </div>
		</form>
	</div>
</div>