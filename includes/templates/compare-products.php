<style type="text/css">
	.table_view_charts .sticky-wrapper.is-sticky .row_chart_0.image_row_chart.row-is-different{height: 280px !important;}
</style>
<?php
	$compareids = (get_query_var('compareids')) ? explode(',', get_query_var('compareids')) : '';
	if(!empty($compareids)){
		global $WS_CLASS;

		$watchArr = [];
		$overview_arr = ["ID", "ReferenceNumber", "BrandID", "BrandName", "Model", "Image", "WatchName"];

		//get watch static models form dbfore
		foreach($compareids as $key => $watchId){
			$watchInfos = $WS_CLASS->getWatchById($watchId);

			if(count($watchInfos) > 0){
				$getPrice = $WS_CLASS->getCurrentLowestPrice($watchInfos[0]["ID"]);
				if($getPrice){
					$watchInfos[0]["Price"] = $getPrice['Price'];
					$watchInfos[0]["Currency"] = $getPrice['Currency'];
				}
				array_push($watchArr, $watchInfos[0]);
			}
		}

		/*echo '<pre>';
	    print_r($watchArr); 
	    echo '</pre>';*/

	    function showProductPrice($price, $currency){
			if($price){
	        	if($currency == 'USD'){
	        		setlocale(LC_MONETARY,"en_US");
	        		echo money_format('%.2n',$price);
	        	}else if($currency == 'EUR'){
	        		setlocale(LC_MONETARY,"de_DE");
	        		echo money_format('€ %!n',$price);
	        	}else{
	        		echo $price.' '.$currency;
	        	}
	    	}else{
	    		echo 'Expired';
	    	}
		}

	    if(!empty($watchArr)){
	    ?>
	    <div class="top_chart table_view_charts loading">
	    	<div class="chart_helper floatleft mr10 ml10 rhhidden"><i class="fal fa-arrows-h font150"></i></div>
	    	<div class="top_chart_controls">
	            <a href="/" class="controls prev"></a>
	            <div class="top_chart_pagination"></div>
	            <a href="/" class="controls next"></a>
	        </div>
	        <div class="top_chart_first">
                <ul>
                    <li class="row_chart_0 image_row_chart">
                        <div class="sticky-cell"><br /><label class="diff-label"><input class="re-compare-show-diff" name="re-compare-show-diff" type="checkbox" /><?php _e('Show only differences', 'rehub_framework');?></label></div>
                    </li>
                    <li class="row_chart_1 heading_row_chart">
                        <?php _e('Overview', 'rehub_framework');?>
                    </li>                        
                    <li class="row_chart_2 meta_value_row_chart">
                        <?php _e('ReferenceNumber', 'rehub_framework');?>
                    </li> 
                    <li class="row_chart_3 meta_value_row_chart">
                        <?php _e('Brand', 'rehub_framework');?>
                    </li>                          
                    <li class="row_chart_4 meta_value_row_chart">
                        <?php _e('Model', 'rehub_framework');?>
                    </li>   
                    <?php if(!empty($watchArr)):?>
                        <li class="row_chart_5 heading_row_chart">
                            <?php _e('Attributes', 'rehub_framework');?>
                        </li>                        
                        <?php 
                        	$i = 6;
                        	foreach($watchArr[0] as $title => $value){
                        		if(!in_array($title, $overview_arr)){
                        			?>
                    				<li class="row_chart_<?php echo $i;?> meta_value_row_chart">
		                                <?php echo $title; ?>
		                            </li>
                        			<?php
                        			$i++;
                        		}
                        	}
                        ?>
                	<?php endif;?>
                </ul>
            </div>
            <?php wp_enqueue_script('carouFredSel'); wp_enqueue_script('touchswipe'); ?>
            <div class="top_chart_wrap woocommerce"><div class="top_chart_carousel">
            	<?php 
            		if(!empty($watchArr)){
            			foreach ($watchArr as $key => $watch) {
            				$watchlink = 'https://ws.euxira.net/product/?id='.$watch['ID'];
            				?>
            				<div class="top_rating_item top_chart_item compare-item-<?php echo $watch['ID'];?>" id="rank_<?php echo $watch['ID']?>" data-compareid="<?php echo $watch['ID'];?>">
            					<ul>
	                                <li class="row_chart_0 image_row_chart">
	                                	<div class="product_image_col sticky-cell">
	                                		<i class="fas fa-times-circle re-compare-close-in-chart"></i>
	                                		<figure>                                       
	                                            <a href="<?php echo $watchlink; ?>">
	                								<img src="<?php echo $watch['Image'];?>" height="150" alt="<?php echo $watch['WatchName'];?>">
	                                            </a>
	                                        </figure>
	                                        <h2>
	                                            <a href="<?php echo $watchlink; ?>">
	                                                <?php echo $watch['WatchName'];?>                     
	                                            </a>
	                                        </h2>
	                                        <div class="price-in-compare-flip mt20">
	                                        	<?php if ($watch["Price"]) : ?>
	                                                <span class="price-woo-compare-chart rehub-main-font rehub-main-color mb15 fontbold"><?php showProductPrice($watch["Price"], $watch["Currency"]);; ?></span>
	                                                <div class="mb10"></div>
	                                            <?php endif;?>
	                                        </div>
	                                	</div>
	                                </li>
	                                <li class="row_chart_1 heading_row_chart">
                                	</li>
                                	<li class="row_chart_2 meta_value_row_chart">
	                                	<?php echo $watch['ReferenceNumber'];?>
	                                </li>
	                                <li class="row_chart_3 meta_value_row_chart">
	                                	<?php echo $watch['BrandName'];?>
	                                </li>
	                                <li class="row_chart_4 meta_value_row_chart">
	                                	<?php echo $watch['Model'];?>
	                                </li>
	                                <?php if(!empty($watchArr)):?>
				                        <li class="row_chart_5 heading_row_chart">
				                        </li>                        
				                        <?php 
				                        	$i = 6;
				                        	foreach($watch as $key => $value){
				                        		if(!in_array($key, $overview_arr)){
				                        			?>
				                    				<li class="row_chart_<?php echo $i;?> meta_value_row_chart">
						                                <?php echo $value; ?>
						                            </li>
				                        			<?php
				                        			$i++;
				                        		}
				                        	}
				                        ?>
				                	<?php endif;?>
	                            </ul>
            				</div>
            				<?php
            			}
            		}
            	?>
            </div></div>
            <span class="top_chart_row_found" data-rowcount="<?php echo ($i + 1);?>"></span>
	    </div>
	    <?php
	    }else{
	    	_e('No compare product available.', 'rehub_framework');
	    }
	}else{
		_e('No compare product available.', 'rehub_framework');
	}
?>