<div class="ws-container woocommerce row">
<?php
$PROTOCOL=(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
if($searchable){
?>
<div class="ws-search-results-list col-md-12">
<div class="ws-search-total col-md-12" style="text-align:center"><h1><?php echo number_format($total_result,0,".",","); ?> result<?php if($total_result>1){echo "s";} ?> for "<?php echo $_GET["query"]; ?>"</h1></div>
<div id="filters-container">
	<div class="filters-blockUI"></div>
	<div class="row filter-header-container">
		<div class="col-md-2 filter-header" data-type="brand"><span>Brand</span><i class="fas fa-caret-down filter-arrow"></i></div>
		<div class="col-md-2 filter-header" data-type="Model"><span>Model</span><i class="fas fa-caret-down filter-arrow"></i></div>
		<div class="col-md-2 filter-header" data-type="Price"><span>Price</span><i class="fas fa-caret-down filter-arrow"></i></div>
		<!--div class="col-md-2 filter-header" data-type="Dealer"><span>Dealer</span><i class="fas fa-caret-down filter-arrow"></i></div-->
		<div class="col-md-2 filter-header" data-type="Bracelet"><span>Bracelet</span><i class="fas fa-caret-down filter-arrow"></i></div>
		<div class="col-md-2 filter-header" data-type="Case"><span>Case</span><i class="fas fa-caret-down filter-arrow"></i></div>
		<div class="col-md-2 filter-header" data-type="Other"><span>Other</span><i class="fas fa-caret-down filter-arrow"></i></div>
	</div>
	<div class="filter-popup">
		<div id="filter-Bracelet" class="row filter-content" style="height:auto;">
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-braceletMaterial">
					<div class="filter-group-name">
						<strong>Bracelet material</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($braceletMaterialArray as $braceletMaterialItem){
							if($braceletMaterialItem["total"]!=0){
								$selected="";
								if($vars["braceletMaterial"]!="all"){
									foreach($vars["braceletMaterial"] as $braceletMaterial){
										if($braceletMaterial == $braceletMaterialItem["id"]){
											$selected=" checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox braceletMaterial-checkbox">
							  <input type="checkbox" value="<?php echo $braceletMaterialItem["id"]; ?>" class="custom-control-input" id="braceletMaterial-<?php echo $braceletMaterialItem["id"]; ?>" <?php echo $selected; ?>>
							  <label class="custom-control-label" for="braceletMaterial-<?php echo $braceletMaterialItem["id"]; ?>"><?php echo $braceletMaterialItem["name"]." (".$braceletMaterialItem["total"].")"; ?></label>
							</div>
							<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-braceletColor">
					<div class="filter-group-name">
						<strong>Bracelet color</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($braceletColorArray as $braceletColorItem){
							if($braceletColorItem["total"]!=0){
								$selected="";
								if($vars["braceletColor"]!="all"){
									foreach($vars["braceletColor"] as $braceletColor){
										if($braceletColor == $braceletColorItem["id"]){
											$selected=" checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox braceletColor-checkbox">
							  <input type="checkbox" value="<?php echo $braceletColorItem["id"]; ?>" class="custom-control-input" id="braceletColor-<?php echo $braceletColorItem["id"]; ?>" <?php echo $selected; ?>>
							  <label class="custom-control-label" for="braceletColor-<?php echo $braceletColorItem["id"]; ?>"><?php echo $braceletColorItem["name"]." (".$braceletColorItem["total"].")"; ?></label>
							</div>
							<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
		</div>
		<div id="filter-Other" class="row filter-content" style="height:auto;">
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-movement">
					<div class="filter-group-name">
						<strong>Movement</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($movementArray as $movement){
							if($movement["total"] != 0){
								$movementChecked="";
								if($vars["movements"] != "all"){
									foreach($vars["movements"] as $movementItem){
										if($movement["id"]==$movementItem){
											$movementChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox movement-checkbox">
								  <input type="checkbox" value="<?php echo $movement["id"]; ?>" class="custom-control-input" id="movement-<?php echo $movement["id"]; ?>" <?php echo $movementChecked; ?>>
								  <label class="custom-control-label" for="movement-<?php echo $movement["id"]; ?>"><?php echo $movement["name"]." (".$movement["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-gender">
					<div class="filter-group-name">
						<strong>Gender</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($genderArray as $gender){
							if($gender["total"] != 0){
								$genderChecked="";
								if($vars["gender"] != "all"){
									foreach($vars["gender"] as $genderItem){
										if($gender["name"]==$genderItem){
											$genderChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox gender-checkbox">
								  <input type="checkbox" value="<?php echo $gender["name"]; ?>" class="custom-control-input" id="gender-<?php echo $gender["id"]; ?>" <?php echo $genderChecked; ?>>
								  <label class="custom-control-label" for="gender-<?php echo $gender["id"]; ?>"><?php echo $gender["name"]." (".$gender["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-dialColor">
					<div class="filter-group-name">
						<strong>Dial color</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($dialColorArray as $dialColor){
							if($dialColor["total"] != 0){
								$dialColorChecked="";
								if($vars["dialColors"] != "all"){
									foreach($vars["dialColors"] as $dialColorItem){
										if($dialColor["id"]==$dialColorItem){
											$dialColorChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox dialColor-checkbox">
								  <input type="checkbox" value="<?php echo $dialColor["id"]; ?>" class="custom-control-input" id="dialColor-<?php echo $dialColor["id"]; ?>" <?php echo $dialColorChecked; ?>>
								  <label class="custom-control-label" for="dialColor-<?php echo $dialColor["id"]; ?>"><?php echo $dialColor["name"]." (".$dialColor["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-glass">
					<div class="filter-group-name">
						<strong>Glass</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($glassArray as $glass){
							if($glass["total"] != 0){
								$glassChecked="";
								if($vars["glass"] != "all"){
									foreach($vars["glass"] as $glassItem){
										if($glass["id"]==$glassItem){
											$glassChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox glass-checkbox">
								  <input type="checkbox" value="<?php echo $glass["id"]; ?>" class="custom-control-input" id="glass-<?php echo $glass["id"]; ?>" <?php echo $glassChecked; ?>>
								  <label class="custom-control-label" for="glass-<?php echo $glass["id"]; ?>"><?php echo $glass["name"]." (".$glass["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-buckle">
					<div class="filter-group-name">
						<strong>Bucket</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($buckleArray as $buckle){
							if($buckle["total"] != 0){
								$buckleChecked="";
								if($vars["buckle"] != "all"){
									foreach($vars["buckle"] as $buckleItem){
										if($buckle["id"]==$buckleItem){
											$buckleChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox buckle-checkbox">
								  <input type="checkbox" value="<?php echo $buckle["id"]; ?>" class="custom-control-input" id="buckle-<?php echo $buckle["id"]; ?>" <?php echo $buckleChecked; ?>>
								  <label class="custom-control-label" for="buckle-<?php echo $buckle["id"]; ?>"><?php echo $buckle["name"]." (".$buckle["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-buckleMaterial">
					<div class="filter-group-name">
						<strong>Bucket material</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($buckleMaterialArray as $buckleMaterial){
							if($buckleMaterial["total"] != 0){
								$buckleMaterialChecked="";
								if($vars["buckleMaterial"] != "all"){
									foreach($vars["buckleMaterial"] as $buckleMaterialItem){
										if($buckleMaterial["id"]==$buckleMaterialItem){
											$buckleMaterialChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox buckleMaterial-checkbox">
								  <input type="checkbox" value="<?php echo $buckleMaterial["id"]; ?>" class="custom-control-input" id="buckleMaterial-<?php echo $buckleMaterial["id"]; ?>" <?php echo $buckleMaterialChecked; ?>>
								  <label class="custom-control-label" for="buckleMaterial-<?php echo $buckleMaterial["id"]; ?>"><?php echo $buckleMaterial["name"]." (".$buckleMaterial["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<div class="filter-group border-bottom filter-closed filter-group-numeral">
					<div class="filter-group-name">
						<strong>Numeral</strong>
						<i class="fas fa-chevron-down float-right"></i>
						<i class="fas fa-chevron-up float-right"></i>
					</div>
					<div class="filter-group-toggle">
						<?php
						foreach($numeralArray as $numeral){
							if($numeral["total"] != 0){
								$numeralChecked="";
								if($vars["numeral"] != "all"){
									foreach($vars["numeral"] as $numeralItem){
										if($numeral["id"]==$numeralItem){
											$numeralChecked="checked";
										}
									}
								}
								?>
								<div class="custom-control custom-checkbox filter-checkbox numeral-checkbox">
								  <input type="checkbox" value="<?php echo $numeral["id"]; ?>" class="custom-control-input" id="numeral-<?php echo $numeral["id"]; ?>" <?php echo $numeralChecked; ?>>
								  <label class="custom-control-label" for="numeral-<?php echo $numeral["id"]; ?>"><?php echo $numeral["name"]." (".$numeral["total"].")"; ?></label>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="col">
			</div>
		</div>
		<div id="filter-Case" class="row filter-content">
			<div class="col">
				<div class="case-chosen filter-item-chosen">
					<h4>YOUR SELECTION</h4>
					<div class="caseMaterial-chosen-items">
						<?php
						if($vars["caseMaterials"]!="all"){
							foreach($vars["caseMaterials"] as $caseMaterial){
								foreach($caseMaterialArray as $caseMaterialItem){
									if($caseMaterial == $caseMaterialItem["id"]){
										?>
										<span class="label-tag"><span class="text"><?php echo $caseMaterialItem["name"]; ?></span><span class="delete" data-type="caseMaterials[]" data-value="<?php echo $caseMaterial; ?>">x</span></span>
										<?php
									}
								}
							}
						}
						?>
					</div>
				</div>
				<!--div class="case-popular">
					<h4>QUICK SELECTION</h4>
					<ul class="popular-case filter-popular-items">
						<li data-value="24">Rose Gold +</i></li>
						<li data-value="40">Steel +</i></li>
						<li data-value="44">Titanium +</i></li>
						<li data-value="47">White Gold +</i></li>
						<li data-value="48">Yellow Gold +</i></li>
					</ul>
				</div-->
			</div>
			<div class="col-7 caseMaterial-no-results right-dropdown-container" style="display:none;">
				<div class="alert alert-warning" role="alert">
				  <i class="fas fa-exclamation-circle"></i> Sorry, your search did not match any items
				</div>
			</div>
			<div class="col-7 case-dropdown-container right-dropdown-container">
				<h4>SEARCH BY CASE MATERIAL</h4>
				<select id="caseMaterial-dropdown" name="basic[]" multiple="multiple" class="3col active">
				<?php
				foreach($caseMaterialArray as $caseMaterialItem){
					if($caseMaterialItem["total"]!=0){
						$selected="";
						if($vars["caseMaterials"]!="all"){
							foreach($vars["caseMaterials"] as $caseMaterial){
								if($caseMaterial == $caseMaterialItem["id"]){
									$selected=" selected";
								}
							}
						}
						?>
						<option value="<?php echo $caseMaterialItem["id"] ?>"<?php echo $selected; ?>><?php echo $caseMaterialItem["name"] ?> (<?php echo $caseMaterialItem["total"]; ?>)</option>
						<?php
					}
				}
				?>
				</select>
			</div>
		</div>
		<div id="filter-Price" class="row filter-content" style="height:auto;">
			<div class="col">
				<div class="form-row">
					<div class="col-6">
						<label>From</label>
						<div class="input-group">
							<div class="input-group-prepend">
							  <span class="input-group-text" style="border-radius: 0;">$</span>
							</div>
							<input type="text" value="<?php
							if($vars["priceFrom"] == 0){
								echo $query_result["MinPrice"];
							}else{
								echo $vars["priceFrom"];
							} ?>" onkeypress="return isNumberKey(event)" class="form-control price-range-from">
						</div>
					</div>
					<div class="col-6">
						<label>To</label>
						<div class="input-group">
							<div class="input-group-prepend">
							  <span class="input-group-text" style="border-radius: 0;">$</span>
							</div>
							<input type="text" value="<?php
							if($vars["priceTo"] == 0){
								echo $query_result["MaxPrice"];
							}else{
								echo $vars["priceTo"];
							} ?>" onkeypress="return isNumberKey(event)" class="form-control price-range-to">
						</div>
					</div>
					<!--div class="col-12" style="margin-top: 20px;">
						<h4>CONDITION</h4>
						<?php
						foreach($conditionArray as $condition){
							$tmp=explode("(",$condition["name"]);
						?>
						<div class="custom-control custom-checkbox">
						  <input type="checkbox" value="<?php echo $condition["id"]; ?>" class="custom-control-input" id="condition-<?php echo $condition["id"]; ?>">
						  <label class="custom-control-label condition-checkbox" for="condition-<?php echo $condition["id"]; ?>" title="<?php echo substr($tmp[1], 0, -1); ?>"><?php echo $tmp[0]; ?></label>
						</div>
						<?php
						}
						?>
					</div-->
				</div>
			</div>
			<div class="col-8">
				<div class="slider-range" style="margin-top: 45px;">
					<input type="hidden" class="price-range-slider" value="<?php echo $query_result["MinPrice"] ?>,<?php echo $query_result["MaxPrice"] ?>" />
				</div>
			</div>
		</div>
		<!--div id="filter-Dealer" class="row filter-content">
			<div class="col">
				<div class="dealer-chosen filter-item-chosen">
					<h4>YOUR SELECTION</h4>
					<div class="dealer-chosen-items">
						<?php
						if($vars["dealers"]!="all"){
							foreach($vars["dealers"] as $dealer){
								foreach($dealersArray as $dealerItem){
									if($dealer == $dealerItem["id"]){
										?>
										<span class="label-tag"><span class="text"><?php echo $dealerItem["name"]; ?></span><span class="delete" data-type="dealers[]" data-value="<?php echo $dealer; ?>">x</span></span>
										<?php
									}
								}
							}
						}
						?>
					</div>
				</div>
				<div class="dealer-popular">
					<h4>TOP RATED DEALERS</h4>
					<ul class="popular-dealer filter-popular-items">
						<li data-value="382">Rolex +</i></li>
						<li data-value="342">Patek Philippe +</i></li>
						<li data-value="226">IWC +</i></li>
						<li data-value="31">Audemars Piguet +</i></li>
						<li data-value="422">TAG Heuer +</i></li>
					</ul>
				</div>
			</div>
			<div class="col-7 dealer-dropdown-container right-dropdown-container">
				<h4>SEARCH BY LIST</h4>
				<select id="dealer-dropdown" name="basic[]" multiple="multiple" class="3col active">
				<?php
				foreach($dealersArray as $dealerItem){
					//if($dealerItem["total"]!=0){
						$selected="";
						if($vars["dealers"]!="all"){
							foreach($vars["dealers"] as $dealer){
								if($dealer == $dealerItem["id"]){
									$selected=" selected";
								}
							}
						}
						?>
						<option value="<?php echo $dealerItem["id"] ?>"<?php echo $selected; ?>><?php echo $dealerItem["name"] ?></option>
						<?php
					//}
				}
				?>
				</select>
			</div>
		</div-->
		<div id="filter-brand" class="row filter-content">
			<div class="col">
				<div class="brand-chosen filter-item-chosen">
					<h4>YOUR SELECTION</h4>
					<div class="brand-chosen-items">
						<?php
						if($vars["manufactures"]!="all"){
							foreach($vars["manufactures"] as $manufacture){
								foreach($brandArray as $brand){
									if($manufacture == $brand["id"]){
										?>
										<span class="label-tag"><span class="text"><?php echo $brand["name"]; ?></span><span class="delete" data-type="manufactures[]" data-value="<?php echo $manufacture; ?>">x</span></span>
										<?php
									}
								}
							}
						}
						?>
					</div>
				</div>
				<div class="brand-popular">
					<h4>POPULAR BRAND</h4>
					<ul class="popular-brand filter-popular-items">
						<li data-value="382">Rolex +</i></li>
						<li data-value="342">Patek Philippe +</i></li>
						<li data-value="226">IWC +</i></li>
						<li data-value="31">Audemars Piguet +</i></li>
						<li data-value="422">TAG Heuer +</i></li>
					</ul>
				</div>
			</div>
			<div class="col-7 brand-no-results right-dropdown-container" style="display:none;">
				<div class="alert alert-warning" role="alert">
				  <i class="fas fa-exclamation-circle"></i> Sorry, your search did not match any items
				</div>
			</div>
			<div class="col-7 brand-dropdown-container right-dropdown-container">
				<h4>SEARCH BY BRAND</h4>
				<select id="brand-dropdown" name="basic[]" multiple="multiple" class="3col active">
				<?php
				foreach($brandArray as $brand){
					if($brand["total"]!=0){
						$selected="";
						if($vars["manufactures"]!="all"){
							foreach($vars["manufactures"] as $manufacture){
								if($manufacture == $brand["id"]){
									$selected=" selected";
								}
							}
						}
						?>
						<option value="<?php echo $brand["id"] ?>"<?php echo $selected; ?>><?php echo $brand["name"] ?> (<?php echo $brand["total"]; ?>)</option>
						<?php
					}
				}
				?>
				</select>
			</div>
		</div>
		<div class="row justify-content-between filter-popup-footer">
			<div class="col-3 filter-footer-left">
			  <span class="save-filters"><i class="fas fa-download"></i> Save this search</span>
			</div>
			<div class="col-8 text-right">
			  <button type="button" class="btn btn-primary btn-lg filter-footer-btn search-filter-btn">Search</button>
			  <button type="button" class="btn btn-outline-secondary btn-lg filter-footer-btn cancel-filter-btn">Cancel</button>
			</div>
		</div>
	</div>
</div>
<div class="filters-tag">
	<?php
	if($vars["manufactures"]!="all"){
		foreach($vars["manufactures"] as $manufacture){
			foreach($brandArray as $brand){
				if($manufacture == $brand["id"]){
					?>
					<span class="label-tag"><span class="text"><?php echo $brand["name"]; ?></span><span class="delete" data-type="manufactures[]" data-value="<?php echo $manufacture; ?>">x</span></span>
					<?php
				}
			}
		}
	}

	if($vars["movements"]!="all"){
		foreach($vars["movements"] as $movement){
			foreach($movementArray as $move){
				if($movement == $move["id"]){
			?>
			<span class="label-tag"><span class="text"><?php echo $move["name"]; ?></span><span class="delete" data-type="movement[]" data-value="<?php echo $movement; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["caseMaterials"]!="all"){
		foreach($vars["caseMaterials"] as $caseMaterial){
			foreach($materialArray as $material){
				if($caseMaterial == $material["id"]){
			?>
			<span class="label-tag"><span class="text">Case <?php echo $material["name"]; ?></span><span class="delete" data-type="caseMaterials[]" data-value="<?php echo $caseMaterial; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["braceletMaterial"]!="all"){
		foreach($vars["braceletMaterial"] as $braceletMaterial){
			foreach($materialArray as $material){
				if($braceletMaterial == $material["id"]){
			?>
			<span class="label-tag"><span class="text">Bracelet <?php echo $material["name"]; ?></span><span class="delete" data-type="braceletMaterial[]" data-value="<?php echo $braceletMaterial; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["glass"]!="all"){
		foreach($vars["glass"] as $glass){
			foreach($glassArray as $glassItem){
				if($glass == $glassItem["id"]){
			?>
			<span class="label-tag"><span class="text">Glass <?php echo $glassItem["name"]; ?></span><span class="delete" data-type="glass[]" data-value="<?php echo $glass; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["dialColors"]!="all"){
		foreach($vars["dialColors"] as $dialColors){
			foreach($dialColorArray as $dialColor){
				if($dialColors == $dialColor["id"]){
			?>
			<span class="label-tag"><span class="text">Dial color <?php echo $dialColor["name"]; ?></span><span class="delete" data-type="dialColors[]" data-value="<?php echo $dialColors; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["buckle"]!="all"){
		foreach($vars["buckle"] as $buckle){
			foreach($buckleArray as $buckleItem){
				if($buckle == $buckleItem["id"]){
			?>
			<span class="label-tag"><span class="text">Buckle <?php echo $numeralItem["name"]; ?></span><span class="delete" data-type="buckle[]" data-value="<?php echo $buckle; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["numeral"]!="all"){
		foreach($vars["numeral"] as $numeral){
			foreach($numeralArray as $numeralItem){
				if($numeral == $numeralItem["id"]){
			?>
			<span class="label-tag"><span class="text">Numeral <?php echo $numeralItem["name"]; ?></span><span class="delete" data-type="numeral[]" data-value="<?php echo $numeral; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["gender"]!="all"){
		if($vars["gender"]=="Men"){
	?>
	<span class="label-tag"><span class="text">Men's watch</span><span class="delete" data-type="gender" data-value="men">x</span></span>
	<?php
		}else if($vars["gender"]=="Women"){
	?>
	<span class="label-tag"><span class="text">Women's watch</span><span class="delete" data-type="gender" data-value="Women">x</span></span>
	<?php
		}else{
	?>
	<span class="label-tag"><span class="text">Unisex</span><span class="delete" data-type="gender" data-value="Unisex">x</span></span>
	<?php
		}
	}

	if($vars["conditions"]!="all"){
		foreach($vars["conditions"] as $conditions){
			foreach($conditionArray as $condition){
				if($conditions == $condition["id"]){
					$tmp=explode("(",$condition["name"]);
			?>
			<span class="label-tag"><span class="text" title="<?php echo substr($tmp[1], 0, -1); ?>"><?php echo $tmp[0]; ?></span><span class="delete" data-type="conditions[]" data-value="<?php echo $conditions; ?>">x</span></span>
			<?php
				}
			}
		}
	}

	if($vars["priceFrom"]!=0 && $vars["priceTo"]!=0){
	?>
	<span class="label-tag"><span class="text"><?php echo number_format($vars["priceFrom"],0,".",",")." - ".number_format($vars["priceTo"],0,".",",")." $"; ?></span><span class="delete" data-type="price-range" data-value="<?php echo $vars["priceFrom"]."-".$vars["priceTo"];  ?>">x</span></span>
	<?php
	}
	?>
</div>
<hr class="col-md-12" style="padding:0;">
<div class="ws-quick-filters">
	<div class="row">
		<div class="col-md-3 no-padding-leftright">
			<form class="form-inline">
			<div class="form-group">
				<label for="search-sort-by">Sort by</label>
				<select class="form-control" id="search-sort-by" style="margin-left: 5px;">
				  <option value="Relevance" <?php if($sortBy=="all" || $sortBy=="Relevance"){echo "selected";} ?>>Relevance</option>
				  <option value="Newest" <?php if($sortBy=="Newest"){echo "selected";} ?>>Newest</option>
				  <option value="PriceLowToHigh" <?php if($sortBy=="PriceLowToHigh"){echo "selected";} ?>>Price: low to high</option>
				  <option value="PriceHighToLow" <?php if($sortBy=="PriceHighToLow"){echo "selected";} ?>>Price: high to low</option>
				</select>
			</div>
			</form>
		</div>
		<div class="col-md-9 no-padding-leftright view-area">
			<label>View: </label>
			<span class="view-item view-grid<?php if($resultview=="grid"){echo " view-selected";} ?>" data-value="grid"><i class="fas fa-th"></i></span>
			<span class="view-item view-list<?php if($resultview=="list"){echo " view-selected";} ?>" data-value="list"><i class="fas fa-th-list"></i></span>
		</div>
	</div>
</div>
<div class="row no-padding-leftright ws-results-list">
<?php
if($resultview == "grid"){
	include_once("search-results-grid.php");
}else{
	include_once("search-results-list.php");
}
?>
</div>
<div class="col-md-12" id="ws-search-pagination"></div>
</div>
<script type="text/javascript" src="<?php echo WATCHSIGNALS_URL ?>assets/js/jquery.simplePagination.js"></script>
<script type="text/javascript" src="<?php echo WATCHSIGNALS_URL ?>assets/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="<?php echo WATCHSIGNALS_URL ?>assets/js/prism.js"></script>
<script type="text/javascript" src="<?php echo WATCHSIGNALS_URL ?>assets/js/jquery.range.js"></script>
<script type="text/javascript">
var ws_search_result_array=<?php echo json_encode($query_result); ?>;
var vars=<?php echo json_encode($vars); ?>;
var ws_brand_array=ws_search_result_array.brandArray;
var ws_dealer_array=<?php echo json_encode($dealersArray); ?>;
var ws_pagination_pages=<?php echo $page_number; ?>;
var ws_pagination_currentPage=<?php echo $current_page; ?>;
var ws_hrefTextPrefix='<?php echo $baseUrl; ?>&page-number=';
</script>
<?php
}else{
?>
<p class="woocommerce-info">No products were found matching your selection.</p>
<?php
}
?>
</div>
<div class="filter-overlay"></div>
