<div class="container-fluid dealernetwork-container">
	<div class="row dealernetwork-warp">
		<div class="col-md-12 center-vertical-horizontally">
			<div class="dealerNetwork-banner">
				<a href="<?php echo home_url(); ?>"><img alt="WatchSignals" alt="Dealer Network Banner" src="<?php echo WATCHSIGNALS_URL; ?>assets/img/watchsignals-logo-white.png"></a>
			</div>
			<h1>Grow your sales with us!</h1>
			<h3>Be a part of our exclusive watch dealer network</h3>
			<div class="joinnow-btn" data-toggle="modal" data-target="#joinnow-modal">JOIN NOW</div>
		</div>
	</div>
</div>
<div class="container-fluid dealer-form-container" style="background:#fff;padding:0;">
	<form class="dealer-form">
		<input type="hidden" name="action" value="partnerNetwork">
		<div class="row">		
			<div class="col-md-4 dealer-frm-group">				
				<input class="form-control form-control-lg" id="partnerName" name="partnerName" type="email" placeholder="EMAIL">
				<div class="invalid-tooltip" style="display:none;">
				  
				</div>
			</div>
			<div class="col-md-4 dealer-frm-group">				
				<input class="form-control form-control-lg" id="partnerWebsite" name="partnerWebsite" type="text" placeholder="WEBSITE">
				<div class="invalid-tooltip" style="display:none;">
				  
				</div>
			</div>
			<div class="col-md-4 dealer-frm-group">				
				<div class="dealer-frm-btn">Partner with us</div>
			</div>		
		</div>
	</form>
</div>
<div class="container-fluid dealer-benefit-cont">
	<h1>Your Benefits with <a href="<?php echo home_url(); ?>">WatchSignals.com</a></h1>	
	<div class="row justify-content-md-center">
		<div class="col benefit-item">
			<div>
				<img style="vertical-align:middle" src="<?php echo WATCHSIGNALS_URL; ?>assets/img/ic_01.svg" class="rounded float-left" alt="Quality Traffic">
				<span class="align-middle">Exclusive Quality Traffic to your online store</span>
			</div>
		</div>
		<div class="col benefit-item">
			<div>
				<img style="vertical-align:middle" src="<?php echo WATCHSIGNALS_URL; ?>assets/img/ic_02.svg" class="rounded float-left" alt="Highly Qualified">
				<span class="align-middle">Highly Qualified leads that convert to sales within a short period</span>
			</div>
		</div>
	</div>
	<div class="row justify-content-md-center">
		<div class="col benefit-item">
			<div>
				<img style="vertical-align:middle" src="<?php echo WATCHSIGNALS_URL; ?>assets/img/ic_03.svg" class="rounded float-left" alt="Market">
				<span class="align-middle">Market analytics and insights with AI</span>
			</div>
		</div>
		<div class="col benefit-item">
			<div>
				<img style="vertical-align:middle" src="<?php echo WATCHSIGNALS_URL; ?>assets/img/ic_04.svg" class="rounded float-left" alt="Easy, Simple intergration">
				<span class="align-middle">Easy, simple intergration</span>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid dealernetwork-bottom">
	<div class="row">
		<div class="col-md-12 center-vertical-horizontally" style="height: fit-content !important;">
			<h1>Complete cost-free in our beta period!</h1>
			<div class="joinnow-btn" data-toggle="modal" data-target="#joinnow-modal">JOIN NOW</div>
		</div>
	</div>
</div>
<div class="modal fade" id="joinnow-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered ws-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body join-email-form">
                <h3 style="text-align: center;">Grow your sales with us!</h3>
                <p style="text-align: center;">Be a part of our exclusive watch dealer network</p>
				<form class="frm-joinnow">
				<div class="form-group partnerName">
					<label><b>Email *</b></label>
					<input type="email" class="form-control" placeholder="Email" name="partnerName" required>                
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerYName">
					<label><b>Your name</b></label>
					<input type="text" class="form-control" placeholder="Your Name" name="partnerYName">                
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerBName">
					<label><b>Your business name</b></label>
					<input type="text" class="form-control" placeholder="Your business name" name="partnerBName">                
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerWebsite">
					<label><b>Website *</b></label>
					<input type="text" class="form-control" placeholder="Website" name="partnerWebsite" required>                
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerIAm">
					<label><b>I am a</b></label>
					<div class="custom-control custom-radio">
					  <input class="custom-control-input" type="radio" id="joinIam1" value="WatchDealer" name="partnerIAm">
					  <label class="custom-control-label" for="joinIam1">
						Watch dealer/Independent watch shop
					  </label>
					</div>
					<div class="custom-control custom-radio">
					  <input class="custom-control-input" type="radio" id="joinIam2" value="Marketplace" name="partnerIAm">
					  <label class="custom-control-label" for="joinIam2">
						Marketplace/Portal
					  </label>
					</div>
					<div class="custom-control custom-radio">
					  <input class="custom-control-input" type="radio" id="joinIam3" value="WatchAuthorizationService" name="partnerIAm">
					  <label class="custom-control-label" for="joinIam3">
						Watch authorization service
					  </label>
					</div>
					<div class="custom-control custom-radio">
					  <input class="custom-control-input" type="radio" id="joinIam4" value="Other" name="partnerIAm">
					  <label class="custom-control-label" for="joinIam4">
						Other
					  </label>					  
					</div>
					<div id="joinSituation"></div>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerIAmOther">
					<input type="text" class="form-control" id="joinOtherValue" placeholder="" name="partnerIAmOther">
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerWin">
					<label><b>How would you like to partner with us for win-win situation?</b></label>
					<div class="custom-control custom-radio">
					  <input value="1" type="radio" class="custom-control-input" id="joinSituation1" name="partnerWin">
					  <label class="custom-control-label" for="joinSituation1">I have inventory of watches and want to be featured on Watchsignals search results</label>
					</div>
					<div class="custom-control custom-radio">
					  <input value="2" type="radio" class="custom-control-input" id="joinSituation2" name="partnerWin">
					  <label class="custom-control-label" for="joinSituation2">I have affiliate program/referral program for Watchsignals to promote my products</label>
					</div>
					<div class="custom-control custom-radio">
					  <input value="3" type="radio" class="custom-control-input" id="joinSituation3" name="partnerWin">
					  <label class="custom-control-label" for="joinSituation3">I want to place my advertisements on Watchsignals</label>
					</div>
					<div class="custom-control custom-radio">
					  <input value="4" type="radio" class="custom-control-input" id="joinSituation4" name="partnerWin">
					  <label class="custom-control-label" for="joinSituation4">I want real-time market reports, market intelligence for luxury watch market</label>
					</div>
					<div class="custom-control custom-radio">
					  <input value="5" type="radio" class="custom-control-input" id="joinSituation5" name="partnerWin">
					  <label class="custom-control-label" for="joinSituation5">Other</label>					  
					</div>					
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group partnerWinOther">					
					<input type="text" class="form-control" placeholder="" name="partnerWinOther">                
					<div class="invalid-feedback"></div>
				</div>		
				<input type="hidden" name="action" value="partnerNetwork">
                <button type="button" id="join-now-btn" class="btn btn-default" style="background:#45A6B6;color:#fff;">JOIN NOW</button>
				</form>
			</div>
        </div>
    </div>
</div>
<script>
setTimeout(function(){
	$('#joinnow-modal').on('shown.bs.modal', function () {
		$(".frm-joinnow").trigger("reset");
	});

	$('.dealer-frm-btn').click(function(){
		$('.dealer-form .invalid-tooltip').hide();

		var partnerForm = $('.dealer-form').serialize();
		$.blockUI({ message: '<h1><img src="'+ws_homeUrl+'/wp-content/themes/watchSignals/assets/css/ui-anim_basic_24x24.gif" /> Just a moment...</h1>' }); 
		//send ajax to ajax admin
		$.ajax({
		  type:"POST",
		  url: ws_plugin_url+"includes/mailchimp-ajax.php", // our PHP handler file
		  data: partnerForm,
		  success:function(res){
			// do something with returned data
			$.unblockUI();
			var results = JSON.parse(res);			
			if(!results.status){
				$('#'+results.el).next().html(results.message);
				$('#'+results.el).next().css({'display':'block'});
			}else{
				$(".dealer-form").trigger("reset");
				$('#alert-price-success-modal').modal('show');
			}
		  }
		});
	});
	
	$("#join-now-btn").click(function(){
		$('.frm-joinnow .invalid-feedback').hide();
		var partnerForm=$('.frm-joinnow').serialize();
		$.blockUI({ message: '<h1><img src="'+ws_homeUrl+'/wp-content/themes/watchSignals/assets/css/ui-anim_basic_24x24.gif" /> Just a moment...</h1>' }); 
		//send ajax to ajax admin
		$.ajax({
		  type:"POST",
		  url: ws_plugin_url+"includes/mailchimp-ajax.php", // our PHP handler file
		  data: partnerForm,
		  success:function(res){
			// do something with returned data
			$.unblockUI();
			var results = JSON.parse(res);			
			if(!results.status){
				$('.'+results.el+' .invalid-feedback').html(results.message);
				$('.'+results.el+' .invalid-feedback').css({'display':'block'});
			}else{
				$('#joinnow-modal').modal('hide');
				$('#alert-price-success-modal').modal('show');				
			}
		  }
		});
	});
},300);
</script>