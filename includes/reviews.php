<?php
include_once('watchsignal.class.php');

// Function to get the client ip address
function get_client_ip_env() {
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if(getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if(getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if(getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if(getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = 'UNKNOWN';

	return $ipaddress;
}
    if(isset($_POST['rate'])){
        $watchId = $_POST['watchId'];
        $rateValue = $_POST['rate'];
        $comment = $_POST['comment'];
        $username = $_POST['username'];
        $useremail = $_POST['useremail'];
        $userip = $_POST['userip'];
        $photo = $_POST['photo'];

        if($useremail=='' || !filter_var($useremail, FILTER_VALIDATE_EMAIL)){
            echo json_encode(array('status'=>false, 'el'=>'email', 'message'=>"Please enter your valid email"));
            exit;
        }

        //echo $watchId;
        $watchSignalObject = new WATCHSIGNALS();
        $review = $watchSignalObject->postReview($watchId, $rateValue, $comment, $username, $useremail, $userip, $photo);
        //$review = $WS_CLASS->getWatchById(14080);

        if($review){
            echo json_encode(array('status'=>true, 'message'=>'save review successfully!', 'data'=>$review));
        }else{
            echo json_encode(array('status'=>false, 'message'=>'error saving review!'));
        }
        exit;
    }

    if(isset($_POST['uploadfile'])){
        if (!empty($_FILES['file'])) {
            foreach($_FILES["file"]["size"] as $k => $value){
                if($value > 2097152){
                    echo json_encode(array('status' => false, 'el'=>'filesize', 'message' => 'Photo upload must be under 2MB'));
                    exit;
                }
            }

            $fileuploaded = array();
            for($i = 0; $i < count($_FILES["file"]["name"]); $i++){
                //print_r($file);
                $file_type = strtolower($_FILES["file"]["type"][$i]);
                if($file_type == "image/jpeg" || $file_type == "image/png"){
                    $target_dir = '../../../uploads/review/';

                    $digits = 3;
                    $random = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
                    $newName = round(microtime(true) * 1000) . $random . substr($_FILES["file"]["name"][$i], -4);

                    if (move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_dir . $newName)) {
                        //if success uploading file, save to database
                        //$this->returnJsonAjax(array('status' => true, 'filename'=> $newName, 'message' => 'Upload images successfully!'));
                        array_push($fileuploaded, $newName);
                    } else { // if not success
                        echo json_encode(array('status' => false, 'el'=>'uploadfailed', 'message' => 'Upload failed!'));
                        exit;
                    }
                }else{
                    echo json_encode(array('status'=>false, 'el'=>'wrongformat', 'message'=> 'Just JPG, PNG images are allowed!'));
                    exit;
                }
            }
            echo json_encode(array('status' => true, 'el'=>'success', 'fileuploaded'=> $fileuploaded, 'message' => 'Upload images successfully!'));
            exit;
        }else{
            echo json_encode(array('status' => true, 'el'=>'nophoto', 'message' => 'No photo'));
            exit;
        }
    }
?>